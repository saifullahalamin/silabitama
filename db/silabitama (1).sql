-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 07 Apr 2014 pada 18.57
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `silabitama`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pendd`
--

CREATE TABLE IF NOT EXISTS `detail_pendd` (
  `id_pendd` int(11) NOT NULL DEFAULT '0',
  `dsn_penguji` varchar(10) NOT NULL DEFAULT '',
  `cttn_pendd` varchar(200) DEFAULT '-',
  `bts_wkt` datetime DEFAULT NULL,
  `cttn_rev` varchar(200) DEFAULT '-',
  `stts_rev` enum('X','Tidak ada revisi','Belum merevisi','Sudah merevisi','Sudah mendapat penilaian terahir') DEFAULT 'X',
  `tt_bhs` int(3) DEFAULT NULL,
  `tt_fp` int(3) DEFAULT NULL,
  `cp_tertulis` int(3) DEFAULT NULL,
  `cp_lisan` int(3) DEFAULT NULL,
  `b_permasalahan` int(3) DEFAULT NULL,
  `pm_dp` int(3) DEFAULT NULL,
  `pm_prog` int(3) DEFAULT NULL,
  `pm_uf` int(3) DEFAULT NULL,
  PRIMARY KEY (`id_pendd`,`dsn_penguji`),
  KEY `dsn_penguji` (`dsn_penguji`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pendd`
--

INSERT INTO `detail_pendd` (`id_pendd`, `dsn_penguji`, `cttn_pendd`, `bts_wkt`, `cttn_rev`, `stts_rev`, `tt_bhs`, `tt_fp`, `cp_tertulis`, `cp_lisan`, `b_permasalahan`, `pm_dp`, `pm_prog`, `pm_uf`) VALUES
(12, '0501096302', 'Tidak ada revisi', '2013-01-01 00:00:00', 'Tidak ada revisi', 'Sudah mendapat penilaian terahir', 12, 12, 12, 21, 12, 12, 21, 21),
(12, '0517127402', '-', NULL, '-', 'Sudah mendapat penilaian terahir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '0524047801', '-', NULL, '-', 'Sudah mendapat penilaian terahir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '0510127501', '-', NULL, '-', 'X', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dsn`
--

CREATE TABLE IF NOT EXISTS `dsn` (
  `id_dsn` varchar(10) NOT NULL DEFAULT '',
  `nm_dsn` varchar(70) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `fb` varchar(500) DEFAULT NULL,
  `kab` varchar(30) DEFAULT NULL,
  `prov` varchar(30) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  PRIMARY KEY (`id_dsn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dsn`
--

INSERT INTO `dsn` (`id_dsn`, `nm_dsn`, `no_telp`, `fb`, `kab`, `prov`, `user_id`, `password`) VALUES
('0004015002', 'Kartini Pramono, M.Hum.', NULL, NULL, NULL, NULL, NULL, NULL),
('0008045303', 'Arry Mth Soekowathy, M.Hum.', NULL, NULL, NULL, NULL, NULL, NULL),
('0008107301', 'Agung Priyanto, S.T.', NULL, NULL, NULL, NULL, '132312089', '900150983cd24fb0d6963f7d28e17f72'),
('0011077702', 'Choerun Asnawi, S.Kom.', '43432', 'http://localhost/ci/user_guide/database/active_record.html', 'dfsdfsd', 'wdwd', 'demo_kaprodi', 'fe01ce2a7fbac8fafaed7c982a04e229'),
('0019047401', 'Titik Rahmawati, S.T.', NULL, NULL, NULL, NULL, NULL, NULL),
('0026026805', 'Rahmat Hidayat, S.Kom., M.Cs.', NULL, NULL, NULL, NULL, NULL, NULL),
('0501096302', 'Abdul Kadir, Dr.', NULL, NULL, NULL, NULL, 'abd', 'e10adc3949ba59abbe56e057f20f883e'),
('0502026802', 'Edhy Tri Cahyono, S.Si., M.M.', NULL, NULL, NULL, NULL, NULL, NULL),
('0506037101', 'Igoon, S.Kom.', NULL, NULL, NULL, NULL, 'igo', 'd225adb4e7bd47bf65846f83634e4420'),
('0506056801', 'Anna Partina, Dra., M.Si.', NULL, NULL, NULL, NULL, NULL, NULL),
('0507037401', 'Dayat Subekti, S.Si.', NULL, NULL, NULL, NULL, NULL, NULL),
('0509017002', 'Ari Cahyono, S.Si., M.T.', NULL, NULL, NULL, NULL, 'kaprodis1', 'e10adc3949ba59abbe56e057f20f883e'),
('0509038001', 'Muhammad Abdus Shomad, S.Sos.I.', '0876676977', 'http://localhost/ci/user_guide/database/active_record.html', 'sleman', 'DIY', NULL, NULL),
('0509057201', 'Damar Widodo, S.Si.', NULL, NULL, NULL, NULL, NULL, NULL),
('0510127501', 'Adkhan Sholeh, S.Si.', NULL, NULL, NULL, NULL, 'demo_pembimbing', 'fe01ce2a7fbac8fafaed7c982a04e229'),
('0513126801', 'Atmaji Istiarno, S.Sos.', NULL, NULL, NULL, NULL, NULL, NULL),
('0514026902', 'V. Sri Sumardiyanti, S.E., M.Si.', NULL, NULL, NULL, NULL, NULL, NULL),
('0514068101', 'Chanief Budi Setiawan, S.T.', NULL, NULL, NULL, NULL, NULL, NULL),
('0514117301', 'Dodi Heriadi, S.T.', NULL, NULL, NULL, NULL, NULL, NULL),
('0516056902', 'Ridwan Arif Nugroho, S.Pd., Dipl. TEFL', NULL, NULL, NULL, NULL, NULL, NULL),
('0517127402', 'Arif Himawan, S.Kom., M.M.', NULL, NULL, NULL, NULL, NULL, NULL),
('0520117801', 'Muhammad Aditya, S.T., M.M.', NULL, NULL, NULL, NULL, NULL, NULL),
('0524047801', 'Adityo Hidayat, S.Kom., M.M.', NULL, NULL, NULL, NULL, 'demo_penguji', 'fe01ce2a7fbac8fafaed7c982a04e229'),
('0525066902', 'Syamsul A. Syahdan, Ir., M.Kom.', NULL, NULL, NULL, NULL, NULL, NULL),
('0525086801', 'Tetty Berliana Damanik, S.Th.', NULL, NULL, NULL, NULL, NULL, NULL),
('0526047001', 'Wahyu Tri Widadyo, S.S., M.Sn.', NULL, NULL, NULL, NULL, NULL, NULL),
('0527017301', 'Rianto, S.Kom., M.Eng.', NULL, NULL, NULL, NULL, NULL, NULL),
('0527027001', 'Landung Sudarmana, S.T., M.Kom.', NULL, NULL, NULL, NULL, NULL, NULL),
('0527066802', 'Agung Widodo, S.S.', NULL, NULL, NULL, NULL, NULL, NULL),
('0528088301', 'Ahmad Hanafi, S.T., M.Eng.', NULL, NULL, NULL, NULL, NULL, NULL),
('0530077201', 'Siti Fiatul Hasanah, S.Pd.', NULL, NULL, NULL, NULL, NULL, NULL),
('0530078101', 'Yosef Endi Sonatha, S.Si.', NULL, NULL, NULL, NULL, NULL, NULL),
('dcs', 'Fgrghh', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mhs`
--

CREATE TABLE IF NOT EXISTS `mhs` (
  `id_prodi` char(2) NOT NULL,
  `nim` varchar(18) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL,
  `fb` varchar(500) DEFAULT NULL,
  `jk` char(1) DEFAULT NULL,
  `kab` varchar(30) DEFAULT NULL,
  `prov` varchar(30) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mhs`
--

INSERT INTO `mhs` (`id_prodi`, `nim`, `nama`, `no_telp`, `fb`, `jk`, `kab`, `prov`, `user_id`, `password`) VALUES
('TI', '2009.01424.11.0003', 'Nurdin Isnanto', NULL, NULL, NULL, NULL, NULL, '31.0003', 'e10adc3949ba59abbe56e057f20f883e'),
('TI', '2009.01425.11.0004', 'Henny Kusumaningrum', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01426.11.0005', 'Nur Risnawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01427.11.0006', 'Isna Tri Puspita Wardani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01428.11.0007', 'Anang Tri Utomo', NULL, NULL, NULL, NULL, NULL, '11.0007', 'e10adc3949ba59abbe56e057f20f883e'),
('TI', '2009.01429.11.0008', 'Muhamad Abdul Azis', NULL, NULL, NULL, NULL, NULL, '11.0008', '900150983cd24fb0d6963f7d28e17f72'),
('TI', '2009.01430.11.0009', 'Beni Ike Hendra Kuswara', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01431.11.0010', 'San Gus Nugroho Saputro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01432.11.0011', 'Yayuk Septi Eka Yudhyawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01434.11.0013', 'Dessy Risna Munziana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01435.11.0014', 'Wenny Tri Manda Sari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01436.11.0015', 'Muh Arif Fatoni', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01438.11.0017', 'Daniel Adi Nugroho', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01439.11.0018', 'Yovi Septian Nugroho', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01440.11.0019', 'Ahmad Herijati Waluyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01441.11.0020', 'Agus Ditya Rua Jala Senatri', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01444.11.0022', 'Harwinda Purnawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01445.11.0023', 'Sandy Hendrawan Decakra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01446.11.0024', 'Sandy Suhendra A. Gafar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01447.11.0025', 'Ajeng Murtiana', '', 'http://localhost/project1/index.php/user_class/mahasiswa/show_profil_mhs', 'P', '', '', NULL, NULL),
('TI', '2009.01448.11.0026', 'Vina Widiyanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01452.11.0028', 'Iip Syarip Hidayatulloh Paozi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01459.11.0029', 'Martiko Suhardiyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01461.11.0030', 'Joko Iswanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01480.11.0031', 'Ekarada Akhniso', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01493.11.0033', 'Reza Bayu Permana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01500.11.0034', 'Dwi Saputranadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01510.11.0037', 'Ade Jandiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01527.11.0038', 'Aji Beni Setyawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01533.11.0039', 'Herry Suratman', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01535.11.0040', 'Rubiyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01543.11.0041', 'Danang Kastowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01544.11.0042', 'Anna Marcelia Bulawaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01551.11.0045', 'Suedi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01553.11.0046', 'Heriyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01554.11.0047', 'Diana Sukmawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01555.11.0048', 'Wilis Al Fiar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01564.11.0049', 'Muhammad Jamil Handinoto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01565.11.0050', 'Dana Wahyudi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01571.11.0051', 'Prudencio Ribeiro Serpa', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01574.11.0052', 'Endah Fachridatus Zakia', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01577.11.0053', 'Maulana Trisna Yulianto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01578.11.0054', 'Alvindra Khunafa Mujono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01579.11.0055', 'Novi Agus Setiawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01580.11.0056', 'Alfirna Rizqi Lahitani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01581.11.0057', 'Erwianta Gustial Radjah', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01582.11.0058', 'Sethepanus Cahya Maendra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01583.11.0059', 'Rahmat Hidayanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01584.11.0060', 'Devita Aditya Mahardika', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01585.11.0061', 'Dwi Wahyu Bintoro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01587.11.0063', 'Marter Rina', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01589.11.0065', 'Rendy Indrasukma', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01590.11.0066', 'Retno Wulandari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01591.11.0067', 'Lifarda Triyuni Kefi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01592.11.0068', 'Suciyati Elviona Alexandre', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01593.11.0069', 'Marcelino Da Silva', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01594.11.0070', 'Nia Nuro Rachniyati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01595.11.0071', 'Satriyo Dwi Handoko', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01596.11.0072', 'Florindo Ruas Guterres', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01597.11.0073', 'Rui Roberto Ximenes', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01598.11.0074', 'Fitri Sri Muliati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01599.11.0075', 'Adrianto Lebre', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01614.11.0076', 'Triana Widiastuti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01617.11.0077', 'Astry Grstanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01619.11.0078', 'Eka Metri Ayu Puspita Dewi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01620.11.0079', 'Edegar De Deus Da Costa Freitas', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01621.11.0080', 'Tofik Wijayanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2009.01622.11.0081', 'Pramuryanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01624.31.1240', 'Siwi Irawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01625.31.1241', 'Irma Devi Megandari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01626.31.1242', 'Indrarta Fajar Nuzuli', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01627.31.1243', 'Dwi Ratna Cahyani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01628.31.1244', 'Tri Septiyana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01629.31.1245', 'Arif Budiman', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01630.31.1246', 'Arif Darmawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01631.31.1247', 'Wahyu Wulansari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01632.31.1248', 'Yulianto', '085878725646', 'http://localhost/ci/user_guide/database/active_record.html', 'L', '', '', '31.1248', 'e10adc3949ba59abbe56e057f20f883e'),
('MI', '2010.01633.31.1249', 'Mochamad Wahyu Triyadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01634.11.0082', 'Wahyu Pangestika', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01635.11.0083', 'Armita Arif Irmawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01636.11.0084', 'Bayu Arfian Sampurna', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01637.11.0085', 'Alan Fajar Jatmika', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01638.11.0086', 'Fajar Prastawa', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01639.11.0087', 'Ervina Diyah Indriarti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01640.11.0088', 'Yohanes Barhekman Anggit Pratama', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01641.11.0089', 'Febri Satriyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01642.11.0090', 'Pandu Pangesti Aji', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01643.11.0091', 'Ratih Puspita Eka Sari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01644.31.1250', 'Yuli Ambarwati', '085878725646', 'yuli ambarwati', 'P', 'lampung tengah', 'lampung', NULL, NULL),
('MI', '2010.01645.31.1251', 'Ade Arifin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01646.31.1252', 'Arnoldus Steven Seri', '085878725646', 'fb', 'L', 'demos', 'demos', '31.1252', 'e10adc3949ba59abbe56e057f20f883e'),
('MI', '2010.01647.31.1253', 'Buana Puguh Ampril Wibowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01648.31.1254', 'Eko Fajar Budisetyawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01649.31.1255', 'Fajar Eka Bintara', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01650.31.1256', 'Amandio Florindo Pires', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01651.31.1257', 'Gardiga Wira Prasetya', '085878725646', 'http://localhost/ci/user_guide/database/active_record.html', 'L', '', '', '31.1257', 'e10adc3949ba59abbe56e057f20f883e'),
('MI', '2010.01652.31.1258', 'Herkulanus Anggi Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01653.31.1259', 'Roy Kelitus Maya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01654.11.0092', 'Eka Kurnia Damayanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01655.11.0093', 'Diana Sari Pratiwi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01656.11.0094', 'Dyah Sari Prastyawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01657.11.0095', 'Ganang Nurdiana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01658.11.0096', 'Jeffry Vendy Hadianto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01659.11.0097', 'Deni Riki Dwi Cahyono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01660.11.0098', 'Aji Tri Pamungkas', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01661.11.0099', 'Febdia Bostarandi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01662.11.0100', 'Rikyan Manual Supriyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01663.11.0101', 'Dwi Wahyuningrum', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01664.31.1260', 'Yudhi Heriyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01665.31.1261', 'Ririn Damayanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01666.31.1262', 'Saifullah Al Amin', '085815995299', 'http://localhost/ci/user_guide/database/active_record.html', 'L', 'Tuban', 'Jawa Timur', 'demo_mhs', 'fe01ce2a7fbac8fafaed7c982a04e229'),
('MI', '2010.01667.31.1263', 'Sigit Iskandar', '08272772', 'http://localhost/project1/index.php/user_class/mahasiswa/edit_profil', 'L', 'siantar top', 'sumut', NULL, NULL),
('MI', '2010.01668.31.1264', 'Aurelius', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01669.31.1265', 'Dionico Sesotya Adi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01670.31.1266', 'Sutrianingsih', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01671.31.1267', 'Yudha Pratama Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01672.31.1268', 'Ermot Kogoya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01673.31.1269', 'Imron Mahmud Saptadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01674.11.0102', 'Cahya Manggarani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01675.11.0103', 'Galih Wahyu Nugroho', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01676.11.0104', 'Okta Wulandari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01677.11.0105', 'Bagus Putro Safarrudin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01678.11.0106', 'Danar Dwi Kiswanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01679.11.0107', 'Putri Sekar Tanjung', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01680.11.0108', 'Dias Aditya Herawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01681.11.0109', 'Andi Wibowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01682.11.0110', 'Sofia Hasmidar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01683.11.0111', 'Gama Darma Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01684.31.1270', 'Joanito Dos Reis', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01685.31.1271', 'Eko Tri Sanjaya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01686.31.1272', 'Nanang Prasetiyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01687.31.1273', 'Suci Gista Erawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01688.31.1274', 'Hadi Prawoto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01689.31.1275', 'Ratna Wulandari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01690.31.1276', 'Bayu Widyantoro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01691.31.1277', 'Erik Nur Badrudin Patria Perdana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01692.31.1278', 'Jusmelia Dos Santos Trindade', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01693.31.1279', 'Nofitha Susana Ulu', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01694.11.0112', 'Ricky Suswandono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01695.11.0113', 'Yeni Widaniyati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01696.11.0114', 'Danny Trihartiwandi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01697.11.0115', 'Abdul Aziiz', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01698.11.0116', 'Neny Afriany', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01699.11.0117', 'Alan Jalu Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01700.11.0118', 'Sixto Da Cruz Henriques', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01701.11.0119', 'Murwanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01702.11.0120', 'Ridwan Efendi Renfaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01703.11.0121', 'Nurmalita Sari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01704.31.1280', 'Feter Agus Setiawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01705.31.1281', 'Siti Nurjanah', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01706.31.1282', 'Didik Suharta', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01707.31.1283', 'Hery Kusmayadi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01708.31.1284', 'Subandi Rumalean', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01709.31.1285', 'Eriawan Sujatmiko', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01710.31.1286', 'Pamungkas Putra Mulya W.', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01711.31.1287', 'Leonito Pereira De Yesus', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01712.31.1288', 'Nuki Ratul Wana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01713.31.1289', 'Danu Aji Pramono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01714.11.0122', 'Arrairi Ansari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01715.11.0123', 'Lelsuri Wewra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01716.11.0124', 'Pingga Oktofinanda Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01717.11.0125', 'Avriana Luvitasari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01718.11.0126', 'Fery Fre Susilowati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01719.11.0127', 'Wahyu Elen Nugraha', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01720.11.0128', 'Eky Arianto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01721.11.0129', 'Endah Lestariningsih', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01722.11.0130', 'Riska Laiva Fristanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01723.11.0131', 'Dwi Octa Widiya Kusumaningrum', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01724.31.1290', 'Eko Rini Kurniawati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01725.31.1291', 'Desy Misdiasari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01726.31.1292', 'Tri Kurniasih', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01727.31.1293', 'Betsalia Wijaya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01728.31.1294', 'Jefri Kurnia Nur Ihsan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01729.31.1295', 'Jefri Sona', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01730.31.1296', 'Bima Gilang Pamukti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01731.31.1297', 'Irma Liani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01732.31.1298', 'Teofilio Da Silva Amaral', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01733.31.1299', 'Hendra Adi Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01734.11.0132', 'Maya Handayani Raga Lay', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01735.11.0133', 'Dui Hatita', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01736.11.0134', 'Rahmat Nur Iman', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01737.11.0135', 'Resi Bimo Sukoco', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01738.11.0136', 'Sutriatno', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01739.11.0137', 'Sarwo Wibowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01740.11.0138', 'Wardjito', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01741.11.0139', 'Uwanan Andarwati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01742.11.0140', 'Romsi Hidayat', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01743.11.0141', 'Helga Oliviana Raunsai', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01744.31.1300', 'Pascoela Pinto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01745.31.1301', 'Leonardus Bonipasius', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01746.31.1302', 'Maria Magdalena Hembring', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01747.31.1303', 'Deby Saputra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01748.31.1304', 'Maria Merry Astriyani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01749.31.1305', 'Fajar Suryono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01750.31.1306', 'Dwi Yoga Satria', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01751.31.1307', 'Julia Da Silva Freitas Go', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01752.31.1308', 'Asri Hemas Widiastuti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01753.31.1309', 'Wahyu Kurniawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01754.11.0142', 'Ossy Novantoro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01755.11.0143', 'Dedy Setyonugroho', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01756.11.0144', 'Nining Pujiyanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01757.11.0145', 'Annisa Dian Ekowati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01758.11.0146', 'Fahria Dahlia Talaohu', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01759.11.0147', 'Ansori Riza Yogi Saputro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01760.11.0148', 'Sugandar Priyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01761.11.0149', 'Ricky Valery', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01762.11.0150', 'Budi Santoso', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01763.11.0151', 'Mahmud Abdurrokhim', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01764.31.1310', 'Hanafi Husni Mubaroq', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01765.31.1311', 'Febrian Pembayun', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01766.31.1312', 'Guntur Eka Noviadaru', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01767.31.1313', 'Achmad Taufiq Nurcahyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01768.31.1314', 'Hery Juniardi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01769.31.1315', 'Rio Bagus Munandar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01770.31.1316', 'Ad Endri Susian Jaya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01771.31.1317', 'Rachmad Efendi Pradana', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01772.11.0152', 'Abdul Hamid Riady', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01773.11.0153', 'Nurdi Fajar Prabowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01774.11.0154', 'Ganang Aditya Rachmat', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01775.11.0155', 'Beato Selmi Fernatyanan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01776.11.0156', 'Ulil Adib', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01777.11.0157', 'Fina Ikhlasia Putri', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01778.11.0158', 'Arek Kaisar', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01779.11.0159', 'Kemas Rizaldi Noor Ikhsyan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01780.11.0160', 'Pambudi Lono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01781.11.0161', 'Ajat Dwi Sudrajat', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01782.11.0162', 'Muhammad Rasyid Firmansyah', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01783.11.0163', 'Ari Wibowo Saputro', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01784.11.0164', 'Abilio Martins Barros Da Costa', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01785.11.0165', 'Kiki Ani Wijayanti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01786.11.0166', 'Deni Kogoya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01787.11.0167', 'Paulina Ina Sensia Menong', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01788.11.0168', 'Wakhid Mukti Mufit Adnan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01789.11.0169', 'Yanuaria Irma Buti', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01790.11.0170', 'Nurpika Widianingrum', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01791.11.0171', 'Endriana Bayu', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01792.11.0172', 'Custodio Ramos Da Silva', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01793.31.1318', 'Sara Paula Ferreira', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01794.31.1319', 'Alberto De Araujo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01795.31.1320', 'Meilana Widayati', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01796.31.1321', 'Mirawati Kogoya', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('MI', '2010.01797.31.1322', 'Ferari Ulfita Anggun Putri', NULL, NULL, NULL, NULL, NULL, 'ferrari', '62c428533830d84fd8bc77bf402512fc'),
('MI', '2010.01798.31.1323', 'Indra Dwi Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01799.11.0173', 'Devi Lucia Agvianda', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01800.11.0174', 'Mufti Baihaqi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01801.11.0175', 'Ayu Styefani', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01802.11.0176', 'Heri Setiawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01803.11.0177', 'Dicky Renville Patria', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01804.11.0178', 'Jekstenli Rematoby', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01805.11.0179', 'Yessy Januarista', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01806.11.0180', 'Maria Goreti Bupu', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01807.11.0181', 'Agus Hirjan', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01808.11.0182', 'Wahyu Lestari', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01809.11.0183', 'Domingos M. Moniz', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01810.11.0184', 'Agapito Lopes Maia', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01811.11.0185', 'Imam Sugiyono', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01812.11.0186', 'Michael Fransiskus Yunior Lopez', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01813.11.0187', 'Tri Supranta', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('TI', '2010.01814.11.0188', 'Almerindo Lopes Martins ', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_ta`
--

CREATE TABLE IF NOT EXISTS `nilai_ta` (
  `id_ta` int(11) NOT NULL,
  `nilai` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_ta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai_ta`
--

INSERT INTO `nilai_ta` (`id_ta`, `nilai`) VALUES
(45, 'C');

-- --------------------------------------------------------

--
-- Struktur dari tabel `opsdik`
--

CREATE TABLE IF NOT EXISTS `opsdik` (
  `npp` varchar(17) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  PRIMARY KEY (`npp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `opsdik`
--

INSERT INTO `opsdik` (`npp`, `nama`, `user_id`, `password`) VALUES
('1994.100372.12.02', 'Indawati, S.E.', NULL, NULL),
('2001.230177.12.17', 'Surani Priyadi', NULL, NULL),
('2002.111178.12.21', 'Muh. Dalfi Husaen', '1234', '900150983cd24fb0d6963f7d28e17f72'),
('2003.020466.12.29', 'Muh. Dimyati', 'demo_opsdik', 'fe01ce2a7fbac8fafaed7c982a04e229');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan_jdl`
--

CREATE TABLE IF NOT EXISTS `pengajuan_jdl` (
  `id_jdl` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(18) DEFAULT NULL,
  `jdl` varchar(500) DEFAULT NULL,
  `abstraksi` text,
  `jenis` varchar(30) DEFAULT NULL,
  `tgl_pengajuan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_ds7i` datetime DEFAULT NULL,
  `stts` enum('Disetujui','Belum Disetujui','Tidak Disetujui','Menghadap Kaprodi','Mengangkat Tema Baru') NOT NULL DEFAULT 'Belum Disetujui',
  PRIMARY KEY (`id_jdl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data untuk tabel `pengajuan_jdl`
--

INSERT INTO `pengajuan_jdl` (`id_jdl`, `nim`, `jdl`, `abstraksi`, `jenis`, `tgl_pengajuan`, `tgl_ds7i`, `stts`) VALUES
(49, '2010.01632.31.1248', 'abc', 'def', 'Multimedia', '2013-09-01 11:30:15', '2013-09-01 18:32:56', 'Disetujui'),
(50, '2010.01651.31.1257', 'abc', 'abc', 'Multimedia', '2013-09-03 23:58:35', '2013-09-04 07:00:16', 'Disetujui');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE IF NOT EXISTS `prodi` (
  `id_prodi` char(2) NOT NULL DEFAULT '',
  `nm_prodi` varchar(26) DEFAULT NULL,
  `kaprodi` varchar(10) NOT NULL,
  PRIMARY KEY (`id_prodi`),
  KEY `kaprodi` (`kaprodi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `nm_prodi`, `kaprodi`) VALUES
('DD', 'dada', '0514068101'),
('MI', 'Manajemen Informatika', '0011077702'),
('TB', 'tata boga', '0501096302'),
('tg', 'tata gantung', '0506056801'),
('TI', 'Teknik Informatika', '0509017002');

-- --------------------------------------------------------

--
-- Struktur dari tabel `proposal`
--

CREATE TABLE IF NOT EXISTS `proposal` (
  `id_prop` int(11) NOT NULL AUTO_INCREMENT,
  `id_jdl` int(11) NOT NULL,
  `rek_dsn_pembb` varchar(10) DEFAULT NULL,
  `stts_acc` enum('Belum disetujui','Disetujui','Tidak disetujui','Menghadap Kaprodi','Mengangkat Tema Baru') DEFAULT 'Belum disetujui',
  `tgl_pendf` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_acc` datetime DEFAULT NULL,
  PRIMARY KEY (`id_prop`),
  UNIQUE KEY `id_jdl` (`id_jdl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data untuk tabel `proposal`
--

INSERT INTO `proposal` (`id_prop`, `id_jdl`, `rek_dsn_pembb`, `stts_acc`, `tgl_pendf`, `tgl_acc`) VALUES
(30, 49, '0', 'Disetujui', '2013-09-01 11:31:48', '2013-09-01 18:32:56'),
(31, 50, '0', 'Disetujui', '2013-09-03 23:59:18', '2013-09-04 07:00:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `req_pembb`
--

CREATE TABLE IF NOT EXISTS `req_pembb` (
  `id_req` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(18) DEFAULT NULL,
  `id_dsn` varchar(10) DEFAULT NULL,
  `stts_req` enum('Disetujui','Belum Disetujui','Tidak Disetujui') DEFAULT 'Belum Disetujui',
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_req`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_bbingan`
--

CREATE TABLE IF NOT EXISTS `riwayat_bbingan` (
  `id_bimbb` int(11) NOT NULL DEFAULT '0',
  `wkt_bimbb` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bab` int(1) DEFAULT NULL,
  `materi_bimbb` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_bimbb`,`wkt_bimbb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `riwayat_bbingan`
--

INSERT INTO `riwayat_bbingan` (`id_bimbb`, `wkt_bimbb`, `bab`, `materi_bimbb`) VALUES
(52, '2013-09-03 03:53:48', 1, 'laaa'),
(52, '2013-09-03 03:53:52', 2, '333'),
(52, '2013-09-03 03:53:56', 3, '33333333'),
(52, '2013-09-03 03:53:59', 4, '444444'),
(52, '2013-09-03 03:54:02', 5, '55555'),
(53, '2013-09-04 00:03:08', 1, '111'),
(53, '2013-09-04 00:03:11', 2, '222'),
(53, '2013-09-04 00:03:16', 3, '333'),
(53, '2013-09-04 00:03:20', 4, '4444'),
(53, '2013-09-04 00:03:24', 5, '555');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_jdl`
--

CREATE TABLE IF NOT EXISTS `riwayat_jdl` (
  `id_ta` int(11) NOT NULL,
  `wkt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jdl` varchar(300) DEFAULT NULL,
  `stts` enum('Aktif','Tidak Aktif') NOT NULL DEFAULT 'Aktif',
  PRIMARY KEY (`id_ta`,`wkt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `riwayat_jdl`
--

INSERT INTO `riwayat_jdl` (`id_ta`, `wkt`, `jdl`, `stts`) VALUES
(45, '2013-09-01 11:32:56', 'abc', 'Aktif'),
(46, '2013-09-04 00:00:16', 'abc', 'Tidak Aktif'),
(46, '2013-09-04 00:03:46', 'defg', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_pembb`
--

CREATE TABLE IF NOT EXISTS `riwayat_pembb` (
  `id_bimbb` int(11) NOT NULL AUTO_INCREMENT,
  `id_ta` int(11) DEFAULT NULL,
  `id_dsn` varchar(10) DEFAULT NULL,
  `wkt_penetapan_dsn_pembb` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `stts_pembbngan` enum('Aktif','Tidak aktif') NOT NULL DEFAULT 'Aktif',
  PRIMARY KEY (`id_bimbb`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data untuk tabel `riwayat_pembb`
--

INSERT INTO `riwayat_pembb` (`id_bimbb`, `id_ta`, `id_dsn`, `wkt_penetapan_dsn_pembb`, `stts_pembbngan`) VALUES
(52, 45, '0501096302', '2013-09-01 11:32:57', 'Aktif'),
(53, 46, '0510127501', '2013-09-04 00:00:16', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_pendd`
--

CREATE TABLE IF NOT EXISTS `riwayat_pendd` (
  `id_pendd` int(11) NOT NULL AUTO_INCREMENT,
  `id_ta` int(11) NOT NULL,
  `tgl_pendf` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_pendd` datetime DEFAULT NULL,
  `stts_pendd` enum('Belum terjadwal','Sudah terjadwal','Sudah dilaksanakan','Mengulang') DEFAULT 'Belum terjadwal',
  PRIMARY KEY (`id_pendd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `riwayat_pendd`
--

INSERT INTO `riwayat_pendd` (`id_pendd`, `id_ta`, `tgl_pendf`, `tgl_pendd`, `stts_pendd`) VALUES
(12, 45, '2013-09-03 03:59:30', '2013-09-04 01:00:00', 'Sudah dilaksanakan'),
(13, 46, '2013-09-04 00:08:27', '2013-09-12 13:00:00', 'Sudah terjadwal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `seminar`
--

CREATE TABLE IF NOT EXISTS `seminar` (
  `id_ta` int(11) NOT NULL,
  `id_dsn` varchar(10) DEFAULT NULL,
  `tgl_pendf` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_sem` datetime DEFAULT NULL,
  `penilaian` varchar(20) DEFAULT NULL,
  `cttn_sem` varchar(100) DEFAULT NULL,
  `stts_sem` enum('Belum terjadwal','Sudah terjadwal','Sudah dilaksanakan') NOT NULL DEFAULT 'Belum terjadwal',
  `id_sem` int(11) NOT NULL AUTO_INCREMENT,
  `ruang` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_sem`),
  KEY `id_dsn` (`id_dsn`),
  KEY `id_ta` (`id_ta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `seminar`
--

INSERT INTO `seminar` (`id_ta`, `id_dsn`, `tgl_pendf`, `tgl_sem`, `penilaian`, `cttn_sem`, `stts_sem`, `id_sem`, `ruang`) VALUES
(45, '0501096302', '2013-09-03 03:56:04', '2013-09-05 01:00:00', 'Layak untuk ujian', '', 'Sudah dilaksanakan', 8, 'd'),
(46, '0510127501', '2013-09-04 00:04:32', '2013-09-11 01:00:00', 'Layak untuk ujian', 'disempurnakan mas', 'Sudah dilaksanakan', 9, 'd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ta`
--

CREATE TABLE IF NOT EXISTS `ta` (
  `id_ta` int(11) NOT NULL AUTO_INCREMENT,
  `id_prop` int(11) DEFAULT NULL,
  `nim` varchar(18) NOT NULL,
  `wkt_perst_oleh_prodi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `th_akademik` varchar(9) DEFAULT NULL,
  `stts` enum('Aktif','Mengangkat Tema Baru') DEFAULT 'Aktif',
  PRIMARY KEY (`id_ta`),
  UNIQUE KEY `id_prop` (`id_prop`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data untuk tabel `ta`
--

INSERT INTO `ta` (`id_ta`, `id_prop`, `nim`, `wkt_perst_oleh_prodi`, `th_akademik`, `stts`) VALUES
(45, 30, '2010.01632.31.1248', '2013-09-01 11:32:56', '2013/2014', 'Aktif'),
(46, 31, '2010.01651.31.1257', '2013-09-04 00:00:16', '2013/2014', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `password`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_user`
--
CREATE TABLE IF NOT EXISTS `v_user` (
`user_id` varchar(255)
,`password` char(32)
,`id` varchar(18)
,`nama` varchar(70)
,`level` varchar(9)
);
-- --------------------------------------------------------

--
-- Struktur untuk view `v_user`
--
DROP TABLE IF EXISTS `v_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user` AS select `mhs`.`user_id` AS `user_id`,`mhs`.`password` AS `password`,`mhs`.`nim` AS `id`,`mhs`.`nama` AS `nama`,'mahasiswa' AS `level` from `mhs` where (`mhs`.`user_id` is not null) union select `dsn`.`user_id` AS `user_id`,`dsn`.`password` AS `password`,`dsn`.`id_dsn` AS `id_dsn`,`dsn`.`nm_dsn` AS `nm_dsn`,'dosen' AS `dosen` from `dsn` where (`dsn`.`user_id` is not null) union select `opsdik`.`user_id` AS `user_id`,`opsdik`.`password` AS `password`,`opsdik`.`npp` AS `npp`,`opsdik`.`nama` AS `nama`,'opsdik' AS `opsdik` from `opsdik` where (`opsdik`.`user_id` is not null) union select `dsn`.`user_id` AS `user_id`,`dsn`.`password` AS `password`,`prodi`.`kaprodi` AS `kaprodi`,`dsn`.`nm_dsn` AS `nm_dsn`,'kaprodi' AS `kaprodi` from (`dsn` join `prodi` on((`dsn`.`id_dsn` = `prodi`.`kaprodi`))) where (`dsn`.`user_id` is not null);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pendd`
--
ALTER TABLE `detail_pendd`
  ADD CONSTRAINT `detail_pendd_ibfk_1` FOREIGN KEY (`id_pendd`) REFERENCES `riwayat_pendd` (`id_pendd`),
  ADD CONSTRAINT `detail_pendd_ibfk_2` FOREIGN KEY (`dsn_penguji`) REFERENCES `dsn` (`id_dsn`);

--
-- Ketidakleluasaan untuk tabel `nilai_ta`
--
ALTER TABLE `nilai_ta`
  ADD CONSTRAINT `nilai_ta_ibfk_1` FOREIGN KEY (`id_ta`) REFERENCES `ta` (`id_ta`);

--
-- Ketidakleluasaan untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD CONSTRAINT `prodi_ibfk_1` FOREIGN KEY (`kaprodi`) REFERENCES `dsn` (`id_dsn`);

--
-- Ketidakleluasaan untuk tabel `riwayat_bbingan`
--
ALTER TABLE `riwayat_bbingan`
  ADD CONSTRAINT `riwayat_bbingan_ibfk_1` FOREIGN KEY (`id_bimbb`) REFERENCES `riwayat_pembb` (`id_bimbb`);

--
-- Ketidakleluasaan untuk tabel `seminar`
--
ALTER TABLE `seminar`
  ADD CONSTRAINT `seminar_ibfk_1` FOREIGN KEY (`id_dsn`) REFERENCES `dsn` (`id_dsn`),
  ADD CONSTRAINT `seminar_ibfk_2` FOREIGN KEY (`id_ta`) REFERENCES `ta` (`id_ta`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
