<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sipbta_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function login($uname, $pass)
	{
		$this->db->select('id, nama, level');
		$this->db->from('v_user');
		$this->db->where('user_id', $uname);
		$this->db->where('password', md5($pass));
		return $this->db->get();
	}
	
	function login3($uname, $pass)
	{
		$this->db->select('user_id');
		$this->db->from('user');
		$this->db->where('user_id', $uname);
		$this->db->where('password', md5($pass));
		return $this->db->get();
	}
	
	function login2($uname, $pass)
	{
		require_once 'ws_req/request_service.php';

		$ambildata = array(
			$request_params = array(
				'pModule' => 'user',
				'pSub' => 'user',
				'pAct' => 'GetPortalUserData'
			),
			$data_params = array($uname)
		);
		
		$data = request_service($ambildata);
		if(!$data) return "Username yang anda masukkan belum terdaftar";
		if($data["passwd"] != md5($pass)) return "Maaf, password anda salah";
		
		$required[0] = $data["referensi"];
		$required[1] = $data["profil"];
		$required[2] = strtolower($data["hak_nama"]);
		
		return $required;
		// replace into
	}
		
	function list_mhs($a = '', $b = '',$c = '',$d = '',$e = '',$f = '',$g = '',$h = '',$i = '',$j = '',$oth = '')
	{
		$this->db->select('*');
		$this->db->from('mhs');
		$this->db->join('prodi B', 'mhs.id_prodi = B.id_prodi');
		
		// filtering
		if($a != '') 		$this->db->limit($a,$b);					// a limit -- b offset
		if($c != '') 		$this->db->order_by($c); 					// c order by name ASC
		if($d != '') 		$this->db->where('mhs.id_prodi', $d); 		// d id_prodi
		if($e != '') 		$this->db->like('nim', $e);					// e nim
		if($f != '') 		$this->db->like('nama', $f);				// f nama
		if($g != '') 		$this->db->like('no_telp', $g);				// g no_telp
		if($h != '') 		$this->db->like('jk', $h);					// h jk
		if($i != '') 		$this->db->like('kab', $i);					// i kab
		if($j != '') 		$this->db->like('prov', $j);				// j prov
		if($oth != ''){													// oth others
			if($oth == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
	}
	
	function list_dsn($a = '', $b = '',$c = '',$d = '',$e = '',$f = '',$g = '',$oth = '')
	{
		$this->db->select('*');
		$this->db->from('dsn');
		
		// filtering
		if($a != '') 		$this->db->limit($a,$b);					// a limit -- b offset
		if($c != '') 		$this->db->order_by($c); 					// c order by name ASC
		if($d != '') 		$this->db->like('id_dsn', $d); 				// d  id_dsn
		if($e != '') 		$this->db->like('nm_dsn', $e);				// e nm_dsn
		if($f != '') 		$this->db->like('kab', $f);					// f kab
		if($g != '') 		$this->db->like('prov', $g);				// g prov
		if($oth != ''){													// oth others
			if($oth == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
	}
	
	function ta($aksi, $a = '', $b = '', $c = '', $d = '', $e = '', $f = '',$g = '',$h = '', $i = '', $oth = '')
	{
		if($aksi == 'select'){						// fungsi select
			$this->db->select('A.id_ta, C.jdl, C.wkt, A.nim, B.nama, D.id_dsn, E.nm_dsn');
			$this->db->from('ta A');
			$this->db->join('mhs B', 'B.nim = A.nim');
			$this->db->join('riwayat_jdl C', 'C.id_ta = A.id_ta', 'left');
			$this->db->join('riwayat_pembb D', 'D.id_ta = A.id_ta', 'left');
			$this->db->join('dsn E', 'E.id_dsn = D.id_dsn', 'left');
			
			// filtering
			if($a != '') 			$this->db->limit($a,$b);						// a limit -- b offset
			if($c != '') 			$this->db->order_by($c); 						// c order by name ASC
			if($d != '') 			$this->db->like('A.id_ta', $d); 				// d id_ta
			if($e != '') 			$this->db->like('A.nim', $e); 					// e nim
			if($f != '') 			$this->db->like('C.jdl', $f);					// f jdl
			if($g != '') 			$this->db->like('A.id_prop', $g);				// g id_prop
			if($h != '') 			$this->db->where('C.stts', $h);					// h stts jdl
			if($i != '') 			$this->db->where('D.stts_pembbngan', $i);		// i stts dsn
			if($oth != ''){															// last others
				if($oth == 'count') return $this->db->count_all_results();
			}else return $this->db->get();
			
		}elseif($aksi == 'update'){				// fungsi update
				
				
		}
	}
	
	function ubah_jdl($id_ta, $jdl)
	{
		// non aktifkan semua judul
		// kemudian insert judul baru.
		
		$this->db->update('riwayat_jdl', array('stts' => 'Tidak Aktif'), "id_ta = ".$id_ta);
		
		$this->db->insert('riwayat_jdl', array('id_ta' => $id_ta, 'jdl' => $jdl));
	}
	
	function jdl_ta($id_ta = '', $stts = '')
	{
		$this->db->select('*');
		$this->db->from('riwayat_jdl');
		$this->db->order_by('wkt DESC');
		if($id_ta != '') $this->db->where('id_ta', $id_ta);
		if($stts != '') $this->db->where('stts', $stts);
		return $this->db->get();
	}
}