<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kaprodi_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function prodi($id_prodi)
	{
		$this->db->select('a.id_prodi, a.nm_prodi, a.kaprodi, b.nm_dsn');
		$this->db->from('prodi a');
		$this->db->join('dsn b', 'a.kaprodi = b.id_dsn');
		$this->db->where('a.id_prodi', $id_prodi);
		return $this->db->get();
	}
	
	function edit_data_prodi($id_prodi1, $id_prodi, $nm_prodi, $kaprodi)
	{
		$det = array(
               'id_prodi' => $id_prodi,
               'nm_prodi' => $nm_prodi,
               'kaprodi' => $kaprodi
            );

		$this->db->where('id_prodi', $id_prodi1);
		$this->db->update('prodi', $det);
	}
	
	function add_prodi($id_prodi, $nm_prodi, $kaprodi)
	{
		$det = array(
               'id_prodi' => $id_prodi,
               'nm_prodi' => $nm_prodi,
               'kaprodi' => $kaprodi
            );

		$this->db->insert('prodi', $det);
	}
}