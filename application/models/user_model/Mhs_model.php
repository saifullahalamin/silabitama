<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mhs_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function profil_mhs($nim)
	{
		return $this->db->get_where('mhs', array('nim' => $nim));
		
	}
	
	function get_detail($nim)
	{
		$this->db->like('nim', $nim);
		return $this->db->get('mhs');
		
	}
	
	function edit_profil($nim, $jk, $hp, $fb, $kab, $prov)
	{
		$det = array(
               'jk' => $jk,
               'no_telp' => $hp,
               'fb' => $fb,
               'kab' => $kab,
               'prov' => $prov
            );

		$this->db->where('nim', $nim);
		$this->db->update('mhs', $det); 
	}
	
	function add_mhs($nim, $nama)
	{
		$det = array(
               'nim' => $nim,
               'nama' => $nama
            );

		$this->db->insert('mhs', $det);
	}
	
	function edit_data_mhs($nim1, $nim, $nama)
	{
		$det = array(
               'nim' => $nim,
               'nama' => $nama
            );
            
        $this->db->where('nim', $nim1);
		$this->db->update('mhs', $det); 
	}
}