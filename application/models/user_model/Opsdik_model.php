<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opsdik_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function get_detail($npp)
	{
		$this->db->where('npp', $npp);
		return $this->db->get('opsdik');
		
	}
	
	function edit_data_ops($npp1, $npp, $nama)
	{
		$det = array(
               'npp' => $npp,
               'nama' => $nama
            );
            
        $this->db->where('npp', $npp1);
		$this->db->update('opsdik', $det);
	}
	
	function add_ops($npp, $nama)
	{
		$det = array(
               'npp' => $npp,
               'nama' => $nama
            );

		$this->db->insert('opsdik', $det);
	}
}