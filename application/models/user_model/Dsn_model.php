<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dsn_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function list_dosen()
	{
		$this->db->order_by('nm_dsn ASC');
		return $this->db->get('dsn');
	}
	
	function dsn_option($def = '', $name = '')
	{
		$dsn_opt = "<select name=\"rek_pembb\" data-inline=\"true\">";
		if($name != '' ) $dsn_opt = "<select name='".$name."'>";
		$dsn_opt .= "\n<option value=\"k\"> -- Pilih Dosen -- </option>";
		foreach($this->list_dosen()->result() as $row)
		{
			if($def != '' && $def == $row->id_dsn) $selected = ' selected="selected"';
			elseif($def != '' && $def != $row->id_dsn) $selected = '';
			elseif($def == '') $selected = '';
			$dsn_opt .= "\n<option value=\"". $row->id_dsn ."\"".$selected.">". $row->nm_dsn ."</option>";
		}
		$dsn_opt .= "\n</select>";
		
		return $dsn_opt;
	}
	
	function get_detail($iddn)
	{
		$this->db->where('id_dsn', $iddn);
		return $this->db->get('dsn');
		
	}
	function is_kaprodi($iddn)
	{
		$this->db->where('kaprodi', $iddn);
		return $this->db->get('prodi');
	}
	
	function p_dsn($id_dsn)
	{
		$this->db->where('id_dsn', $id_dsn);
		return $this->db->get('dsn');
	}
	
	function edit_profil($id_dsn, $hp, $fb, $kab, $prov){
		$det = array(
               'no_telp' => $hp,
               'fb' => $fb,
               'kab' => $kab,
               'prov' => $prov
            );

		$this->db->where('id_dsn', $id_dsn);
		$this->db->update('dsn', $det); 
	}
	
	function edit_data_dsn($id_dsn1, $id_dsn, $nm_dsn)
	{
		$det = array(
               'id_dsn' => $id_dsn,
               'nm_dsn' => $nm_dsn
            );
            
        $this->db->where('id_dsn', $id_dsn1);
		$this->db->update('dsn', $det);
	}
	
	function add_dsn($id_dsn, $nm_dsn)
	{
		$det = array(
               'id_dsn' => $id_dsn,
               'nm_dsn' => $nm_dsn
            );

		$this->db->insert('dsn', $det);
	}
}