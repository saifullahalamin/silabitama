<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penguji_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function akumulasi_penguji($id_dsn = '')
	{
		if($id_dsn == ''){
			$sql = "SELECT a.id_dsn, a.nm_dsn, COUNT( b.dsn_penguji ) as akum 
					FROM dsn a
					LEFT JOIN detail_pendd b ON b.dsn_penguji = a.id_dsn
					GROUP BY a.id_dsn
					ORDER BY a.nm_dsn ASC ";
			return $this->db->query($sql);
		}
		else {
			$sql = "SELECT a.id_dsn, a.nm_dsn, COUNT( b.dsn_penguji ) as akum
					FROM dsn a
					LEFT JOIN detail_pendd b ON b.dsn_penguji = a.id_dsn 
					where a.id_dsn = ?
					GROUP BY a.id_dsn
					ORDER BY a.nm_dsn ASC";	
			return $this->db->query($sql, array($id_dsn));
		}	
	}
}