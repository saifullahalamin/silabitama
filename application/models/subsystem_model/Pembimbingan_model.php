<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembimbingan_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	function req_pembb()
	{
		$pembb = array(
		   'nim' => $this->session->userdata('id_user'),
		   'id_dsn' => $this->input->post('rek_pembb')
		);
		
		$this->db->insert('req_pembb', $pembb);
	}
	
	function list_req_pembb($limit = '', $offset = '', $ob = '', $nim = '', $id_dsn = '', $stts_req = '', $waktu = '', $nama = '', $id_req = '', $count = '')
	{
		$this->db->select('A.id_req, A.nim, B.nama, A.id_dsn, A.stts_req, A.waktu, C.nm_dsn');
		$this->db->from('req_pembb A');
		$this->db->join('mhs B',  'B.nim = A.nim');
		$this->db->join('dsn C',  'C.id_dsn = A.id_dsn');
		
		// filtering
		if($limit != '') 		$this->db->limit($limit,$offset);					// 1 limit
		if($ob != '') 			$this->db->order_by($ob); 							// 3 order by name ASC
		if($nim != '') 			$this->db->like('A.nim', $nim); 					// 4 nim
		if($id_dsn != '') 		$this->db->like('A.id_dsn', $id_dsn);				// 5 id_dsn
		if($stts_req != '') 	$this->db->like('A.stts_req', $stts_req);			// 6 stts_req
		if($waktu != '') 		$this->db->like('A.waktu', $waktu);					// 7 waktu
		if($nama != '') 		$this->db->like('B.nama', $nama);					// 8 nama
		if($id_req != '') 		$this->db->like('A.id_req', $id_req);				// 9 id_req
		if($count != ''){															// 10 others
			if($count == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
	}
	
	function penyetujuan_req_pembb($id_req)
	{
		// update tabel req pembb
		$req_pembb = array(
		   'stts_req' => $this->input->post('pers7an')
		);
		$this->db->where('id_req', $id_req);
		$this->db->update('req_pembb', $req_pembb);	
	}
		
	function riwayat_pembb($aksi, $a = '', $b = '')
	{
		if($aksi == 'select'){
			// fungsi select
			
		}elseif($aksi == 'insert'){
			// fungsi insert
			$riwayat_pembb = array(
				'id_ta' => $a,
				'id_dsn' => $b
			);
			
			$this->db->insert('riwayat_pembb', $riwayat_pembb);
		}elseif($aksi == 'update'){
			// fungsi update
			
		}
	}
	
	function tentukan_pembb($id_dsn, $nim)
	{	
		// beri idta
		// gunakan idta untuk dist pembb
		$this->db->insert('ta', array('nim' => $nim));
		
		$this->db->select('id_ta');
		$this->db->from('ta');
		$this->db->where('nim', $nim);
		$this->db->order_by('id_ta', 'desc');
		$id_ta = $this->db->get()->row()->id_ta;
		
		$this->db->insert('riwayat_pembb', array('id_ta' => $id_ta, 'id_dsn' => $id_dsn));
	}
	
	function dist_pembb($a = '', $b = '', $c = '', $d = '', $e = '', $f = '', $g = '', $h = '', $i = '', $j = '', $k = '', $oth = '', $l = '')
	{
		$this->db->select('A.nim, A.nama, A.id_prodi, E.nm_prodi, D.id_dsn, D.nm_dsn, C.id_bimbb, C.id_ta');
		$this->db->from('mhs A');
		$this->db->join('ta B','B.nim = A.nim');
		$this->db->join('riwayat_pembb C','C.id_ta = B.id_ta');
		$this->db->join('dsn D','C.id_dsn = D.id_dsn');
		$this->db->join('prodi E','A.id_prodi = E.id_prodi');
		$this->db->where('C.stts_pembbngan','Aktif');
		// filtering
		if($a != '') 		$this->db->limit($a,$b);					// a limit -- b offset
		if($c != '') 		$this->db->order_by($c); 					// c order by name ASC
		if($d != '') 		$this->db->where('C.id_bimbb', $d); 		// d id_bimbb
		if($e != '') 		$this->db->where('A.nim', $e);				// e nim
		if($f != '') 		$this->db->like('A.nama', $f);				// f nama
		if($g != '') 		$this->db->like('D.nm_dsn', $g);			// g nm_dsn
		if($h != '') 		$this->db->where('D.id_dsn', $h);			// h id_dsn
		if($i != '') 		$this->db->like('A.jk', $i);				// i jk
		if($j != '') 		$this->db->like('A.kab', $j);				// j kab
		if($k != '') 		$this->db->like('A.prov', $k);				// k prov
		if($l != '') 		$this->db->like('A.id_prodi', $l);			// l id_prodi
		if($oth != ''){													// oth others
			if($oth == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
	}
	
	function un_dist_pembb($id_prodi)
	{
		$query = "SELECT c.nim, c.nama
				FROM mhs c
				LEFT JOIN (
				
					SELECT b.nim
					FROM riwayat_pembb a
					JOIN ta b
					USING ( id_ta ) 
					WHERE a.stts_pembbngan =  \"Aktif\"
				)d
				USING ( nim ) 
				WHERE c.id_prodi =  \"$id_prodi\"
				AND d.nim IS NULL ";
		return $this->db->query($query);
	}
	
	function riwayat_bimbb($aksi, $a = '', $b = '', $c = '', $oth = '')
	{
		if($aksi == 'select'){
			// fungsi select
			$this->db->select('*');
			$this->db->from('riwayat_bbingan A');
			$this->db->join('riwayat_pembb B','B.id_bimbb = A.id_bimbb');
			$this->db->join('ta C','C.id_ta = B.id_ta');
			// filtering
			if($a != '') 		$this->db->where('A.id_bimbb',$a);			// a id_bimbb
			if($b != '') 		$this->db->where('C.nim',$b);				// b nim
			if($oth != ''){													// oth others
				if($oth == 'count') return $this->db->count_all_results();
			}else return $this->db->get();
			
		}elseif($aksi == 'insert'){
			// fungsi insert
			$riwayat_bbingan = array(
				'id_bimbb' => $a,
				'bab' => $b,
				'materi_bimbb' => $c
			);
			
			$this->db->insert('riwayat_bbingan', $riwayat_bbingan);
		}elseif($aksi == 'update'){
			// fungsi update
			
		}
	}
	
	function riwayat_bimbb_count_kap($id_prodi = '', $bab)
	{
		$sql = "SELECT COUNT( id_bimbb ) AS jml
				FROM (
				
					SELECT id_bimbb, bab, wkt_bimbb
					FROM (
					
						SELECT id_bimbb, bab, wkt_bimbb
						FROM  `riwayat_bbingan` 
						ORDER BY wkt_bimbb DESC
					)abc
					GROUP BY id_bimbb
				)def
				JOIN riwayat_pembb b
				USING ( id_bimbb ) 
				JOIN ta c
				USING ( id_ta ) 
				JOIN mhs d
				USING ( nim ) 
				JOIN prodi e
				USING ( id_prodi ) 
				WHERE bab =  \"$bab\"";
				
		if($id_prodi != '' ) $sql .= " AND id_prodi =  \"$id_prodi\"";
				
		return $this->db->query($sql);
	}
	function riwayat_bimbb_count_pembb($iddn, $bab)
	{
		$sql = "SELECT COUNT( id_bimbb ) AS jml
				FROM (
					SELECT id_bimbb, bab, wkt_bimbb
					FROM (
						SELECT id_bimbb, bab, wkt_bimbb
						FROM  `riwayat_bbingan` 
						ORDER BY wkt_bimbb DESC
					)abc
					GROUP BY id_bimbb
				)def
				JOIN riwayat_pembb b
				USING ( id_bimbb ) 
				JOIN ta c
				USING ( id_ta ) 
				JOIN mhs d
				USING ( nim ) 
				JOIN prodi e
				USING ( id_prodi ) 
				WHERE bab =  \"$bab\"
				and b.id_dsn = \"$iddn\"";
								
		return $this->db->query($sql);
	}
}