<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendadaran_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
		
	function daftar_pendd($id_ta)
	{
		$this->db->insert('riwayat_pendd', array('id_ta' => $id_ta));
	}
	
	function list_pendf_pendd($gb = '', $a = '', $b = '', $c = '', $d = '', $e = '', $f = '',$g = '', $h = '', $oth = '')
	{
		$this->db->select('C.id_prodi, C.nim, C.nama, B.id_ta, A.tgl_pendf,E.tt_bhs, E.tt_fp, E.cp_tertulis, E.cp_lisan, E.b_permasalahan, E.pm_dp, E.pm_prog, E.pm_uf, E.dsn_penguji, E.stts_rev, D.nm_dsn, A.id_pendd, A.stts_pendd, A.tgl_pendd');
		$this->db->from('riwayat_pendd A');
		$this->db->join('detail_pendd E', 'E.id_pendd = A.id_pendd', 'left');
		$this->db->join('ta B', 'B.id_ta = A.id_ta', 'left');
		$this->db->join('mhs C', 'C.nim = B.nim', 'left');
		$this->db->join('dsn D', 'D.id_dsn = E.dsn_penguji', 'left');
		if($oth == 'pembb')	{
			$this->db->select('F.id_dsn as pembb_aktif');
			$this->db->join('riwayat_pembb F', 'F.id_ta = A.id_ta', 'left');		// oth = pembb
			$this->db->where('stts_pembbngan', 'Aktif');
		}
		
		// filtering
		if($a != '') 		$this->db->limit($a,$b);					// a limit -- b offset
		if($c != '') 		$this->db->order_by($c); 					// c order by name ASC
		if($d != '') 		$this->db->where('A.stts_pendd', $d); 		// d stts_pendd
		if($e != '') 		$this->db->where('A.id_pendd', $e); 		// e id_pendd
		if($f != '') 		$this->db->where('C.id_prodi', $f); 		// f id_prodi
		if($gb == '') 		$this->db->group_by('A.id_pendd');			// gb groub by id_pendd
		if($g != '') 		$this->db->where('B.nim', $g);				// g nim
		if($h != '') 		$this->db->where('E.dsn_penguji', $h);		// h dsn_penguji
		
		return $this->db->get();
	}
	
	function detail_pendd($id_pendd, $dsn_penguji = '')
	{
		$this->db->where('id_pendd', $id_pendd);
		$this->db->where('dsn_penguji', $dsn_penguji);
		return $this->db->get('detail_pendd');
	}
	
	function tentukan_pendd($user, $id_pendd, $a = '', $b = '', $c = '', $d = '', $e = '')
	{
		if($user == 'opsdik'){ 					// a -> dsn_penguji || b -> tgl_pendd
			$data = array(
               'tgl_pendd' => $b,
               'stts_pendd' => 'Sudah terjadwal'
            );

			$this->db->where('id_pendd', $id_pendd);
			$this->db->update('riwayat_pendd', $data);
			
			if($c != 'edit') $this->db->insert('detail_pendd', array('id_pendd' => $id_pendd, 'dsn_penguji' => $a ));
				
		}elseif($user == 'kaprodi'){			// a -> penguji_1 || b -> penguji_2
			if($c == 'tentukan_penguji'){
				if($a != '')$this->db->insert('detail_pendd', array('id_pendd' => $id_pendd, 'dsn_penguji' => $a ));
				if($b != '')$this->db->insert('detail_pendd', array('id_pendd' => $id_pendd, 'dsn_penguji' => $b ));
			}
			if($c == 'edit'){
				$data = array(
		               'dsn_penguji' => $a
		            	);
		
				$this->db->where('id_pendd', $id_pendd);
				$this->db->where('dsn_penguji', $d);
				$this->db->update('detail_pendd', $data);
				
				$data = array(
		               'dsn_penguji' => $b
		            	);
		
				$this->db->where('id_pendd', $id_pendd);
				$this->db->where('dsn_penguji', $e);
				$this->db->update('detail_pendd', $data);
			}
		}
	}
	
	function sttk_pendd($id_dsn = '', $stts_pendd = '')
	{
		$sql = "SELECT COUNT( A.id_pendd ) AS jml
				FROM (
				 `riwayat_pendd` A
				)
				JOIN  `riwayat_pembb` D ON  `D`.`id_ta` =  `A`.`id_ta` 
				WHERE D.stts_pembbngan =  \"Aktif\" ";
				
		if($stts_pendd != '') 	$sql .= "AND stts_pendd =  \"$stts_pendd\" ";
		if($id_dsn != '') 		$sql .= "AND D.id_dsn =  \"$id_dsn\" ";
		
		return $this->db->query($sql);
	}
	
	function tentukan_nilai_ta($id_pendd, $id_ta, $nilai)
	{
		$this->db->insert('nilai_ta', array('id_ta' => $id_ta, 'nilai' => $nilai));
		
		$this->db->where('id_pendd', $id_pendd);
		$this->db->update('detail_pendd', array('stts_rev' => 'Sudah mendapat penilaian terahir'));
	}
	
	function nilai_ta_saja($id_ta)
	{
		$this->db->where('id_ta', $id_ta);
		return $this->db->get('nilai_ta');
	}
	
	function nilai_ta($id_prodi = '', $id_dsn = '')
	{
		$this->db->select('a.id_ta, a.nilai');
		$this->db->from('nilai_ta a');
		$this->db->join('ta b', 'b.id_ta = a.id_ta');
		$this->db->join('mhs c', 'c.nim = b.nim');
		$this->db->join('riwayat_pembb d', 'd.id_ta = a.id_ta');
		$this->db->where('d.stts_pembbngan', 'Aktif');
		
		if($id_dsn != '') $this->db->where('d.id_dsn', $id_dsn);
		if($id_prodi != '') $this->db->where('c.id_prodi', $id_prodi);
		return $this->db->get();
	}
	
	function beri_revisi($id_pendd, $dsn_penguji, $stts_rev, $nilai, $cttn_pendd, $cttn_rev, $bts_wkt)
	{
		$data = array(
                'cttn_pendd' 		=> $cttn_pendd,
                'cttn_rev' 			=> $cttn_rev,
                'bts_wkt' 			=> $bts_wkt,
                'stts_rev' 			=> $stts_rev,
                'tt_bhs' 			=> $nilai['tt_bhs'],
				'tt_fp' 			=> $nilai['tt_fp'],
				'cp_tertulis' 		=> $nilai['cp_tertulis'],
				'cp_lisan' 			=> $nilai['cp_lisan'],
				'b_permasalahan' 	=> $nilai['b_permasalahan'],
				'pm_dp' 			=> $nilai['pm_dp'],
				'pm_prog' 			=> $nilai['pm_prog'],
				'pm_uf' 			=> $nilai['pm_uf']
            );

		$this->db->where('id_pendd', $id_pendd);
		$this->db->where('dsn_penguji', $dsn_penguji);
		$this->db->update('detail_pendd', $data);
	}
	function verifikasi_revisi($id_pendd, $dsn_penguji, $nilai)
	{
		$data = array(
               'stts_rev' => 'Sudah merevisi',
               'nilai' => $nilai
            );

		$this->db->where('id_pendd', $id_pendd);
		$this->db->where('dsn_penguji', $dsn_penguji);
		$this->db->update('detail_pendd', $data);
	}
}