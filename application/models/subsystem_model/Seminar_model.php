<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__)."/pembimbingan_model.php");

class Seminar_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->pembimbingan = new Pembimbingan_model();
	}
		
	function daft_sem($usulan_waktu)
	{
		// cari id_ta dan id_dsn berdasarkan nim
		$nim = $this->session->userdata('id_user');
		
		$mhs = $this->pembimbingan->dist_pembb('','','wkt_penetapan_dsn_pembb DESC','',$nim)->row();
		
		// masukkan ke tabel seminar
		$sem = array(
		   'id_ta' => $mhs->id_ta,
		   'id_dsn' => $mhs->id_dsn,
		   'tgl_sem' => $usulan_waktu
		);
		
		$this->db->insert('seminar', $sem); 
	}
	
	function list_pendf_sem($a = '', $b = '', $c = '', $d = '', $e = '', $f = '', $g = '', $h = '', $i = '')
	{
		$this->db->select('C.id_prodi, A.cttn_sem, C.nim, C.nama, B.id_ta, A.tgl_pendf, A.penilaian, A.id_dsn, D.nm_dsn, A.id_sem, A.stts_sem, A.tgl_sem, A.ruang');
		$this->db->from('seminar A');
		$this->db->join('ta B', 'B.id_ta = A.id_ta');
		$this->db->join('mhs C', 'C.nim = B.nim');
		$this->db->join('dsn D', 'D.id_dsn = A.id_dsn');
		
		// filtering
		if($a != '') 		$this->db->limit($a,$b);					// a limit -- b offset
		if($c != '') 		$this->db->order_by($c); 					// c order by name ASC
		if($d != '') 		$this->db->where('A.stts_sem', $d); 		// d stts_sem
		if($e != '') 		$this->db->where('A.id_sem', $e); 			// e id_sem
		if($f != '') 		$this->db->where('B.nim', $f);				// f nim
		if($g != '') 		$this->db->where('A.penilaian', $g);		// g penilaian
		if($h != '') 		$this->db->where('C.id_prodi', $h);			// h id_prodi
		if($i != '') 		$this->db->where('A.id_dsn', $i);			// i id_dsn
		return $this->db->get();
	}
	
	function sttk_sem($id_dsn = '', $stts = '')
	{
		$sql = "SELECT COUNT( A.id_sem ) AS jml
				FROM (
				 `seminar` A
				)
				JOIN  `ta` B ON  `B`.`id_ta` =  `A`.`id_ta` 
				JOIN  `mhs` C ON  `C`.`nim` =  `B`.`nim` 
				JOIN  `riwayat_pembb` D ON  `D`.`id_dsn` =  `A`.`id_dsn` 
				JOIN dsn E ON E.id_dsn = D.id_dsn
				WHERE D.stts_pembbngan =  \"Aktif\" ";
				
		if($stts != '') $sql .= "AND stts_sem = \"$stts\" ";
		if($id_dsn != '') $sql .= "AND D.id_dsn = \"$id_dsn\"";
		
		return $this->db->query($sql);
	}
	function tentukan_seminar($id_sem, $waktu, $ruang, $pembb)
	{
		$data = array(
               'tgl_sem' => $waktu,
               'ruang' => $ruang,
               'id_dsn' => $pembb,
               'stts_sem' => 'Sudah terjadwal'
            );

		$this->db->where('id_sem', $id_sem);
		$this->db->update('seminar', $data); 
	}
	
	function berinilai_seminar($id_sem, $penilaian, $cttn)
	{
		$data = array(
               'penilaian' => $penilaian,
               'cttn_sem' => $cttn,
               'stts_sem' => 'Sudah dilaksanakan'
            );

		$this->db->where('id_sem', $id_sem);
		$this->db->update('seminar', $data);
	}
}