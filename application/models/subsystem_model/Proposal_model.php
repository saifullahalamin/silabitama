<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__)."/../Sipbta_model.php");

class Proposal_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
		$this->sipbta_model = new Sipbta_model();
	}
		
	function jdl_ta($limit = '',$offset = 0, $ob = '', $id_jdl = '', $nim = '', $jdl = '', $jenis = '', $tgl_peng = '', $tgl_ds7i = '', $stts = '', $count = '', $id_prodi = '')
	{
		$this->db->select('A.id_jdl, A.nim, A.jdl, A.abstraksi, A.jenis, A.tgl_pengajuan, A.tgl_ds7i, A.stts, mhs.nama');
		$this->db->from('pengajuan_jdl A');
		$this->db->join('proposal B', 'B.id_jdl = A.id_jdl', 'left');
		$this->db->join('mhs', 'mhs.nim = A.nim', 'left');
		$this->db->where('B.id_jdl IS NULL');
		
		// filtering
		if($limit != '') 	$this->db->limit($limit,$offset);			// 1 limit
		if($ob != '') 		$this->db->order_by($ob); 					// 3 order by name ASC
		if($id_jdl != '') 	$this->db->where('A.id_jdl', $id_jdl); 		// 4 id_jdl
		if($nim != '')	 	$this->db->where('A.nim', $nim);			// 5 nim
		if($jdl != '') 		$this->db->like('jdl', $jdl);				// 6 jdl
		if($jenis != '') 	$this->db->like('jenis', $jenis);			// 7 jenis
		if($tgl_peng != '') $this->db->like('tgl_pengajuan', $tgl_peng);// 8 tgl_peng
		if($tgl_ds7i != '') $this->db->like('tgl_ds7i', $tgl_ds7i); 	// 9 tgl_ds7i
		if($stts != '') 	$this->db->where('A.stts', $stts);			// 10 stts
		if($id_prodi != '')	$this->db->where('mhs.id_prodi', $id_prodi);// 12 id_prodi
		if($count != ''){												// 11 others
			if($count == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
		
						//  $this->db->order_by('title desc, name asc'); 
						//  Produces: ORDER BY title DESC, name ASC
	}
	
	function jdl_saja($id_prodi = '')
	{
		$this->db->select('*');
		$this->db->from('pengajuan_jdl A');
		$this->db->join('proposal B', 'B.id_jdl = A.id_jdl', 'left');
		$this->db->join('mhs C', 'C.nim = A.nim', 'left');
		$this->db->where('tgl_pengajuan !=', 'B.tgl_pendf');
		if($id_prodi != '') $this->db->where('C.id_prodi', $id_prodi);
		return $this->db->get();
	}
	
	function proposal_ta($limit = '',$offset = 0, $ob = '', $id_prop='', $nim = '', $id_jdl = '',$jdl = '', $rek_pembb = '', $stts_acc = '', $tgl_pendf = '', $tgl_acc = '', $nama = '', $count = '', $id_prodi = '')
	{
		$this->db->select('prodi.nm_prodi, mhs.nim, mhs.nama, B.id_prop, B.id_jdl, A.jdl, A.abstraksi, A.jenis, B.rek_dsn_pembb, B.stts_acc, B.tgl_pendf, B.tgl_acc');
		$this->db->from('proposal B');
		$this->db->join('pengajuan_jdl A',  'B.id_jdl = A.id_jdl');
		$this->db->join('mhs', 'mhs.nim = A.nim');
		$this->db->join('prodi', 'prodi.id_prodi = mhs.id_prodi');
		
		// filtering
		if($limit != '') 		$this->db->limit($limit,$offset);				// 1 limit
		if($ob != '') 			$this->db->order_by($ob); 						// 3 order by name ASC
		if($id_prop != '') 		$this->db->where('id_prop', $id_prop); 			// 4 id_prop
		if($nim != '') 			$this->db->where('A.nim', $nim); 				// 5 nim
		if($id_jdl != '') 		$this->db->where('B.id_jdl', $id_jdl);			// 6 id_jdl
		if($jdl != '') 			$this->db->like('jdl', $jdl);					// 7 jdl
		if($rek_pembb != '') 	$this->db->like('rek_dsn_pembb', $rek_pembb);	// 8 rek_pembb
		if($stts_acc != '') 	$this->db->where('stts_acc', $stts_acc);		// 9 stts_acc
		if($tgl_pendf != '') 	$this->db->where('tgl_pendf', $tgl_pendf); 		// 10 tgl_pendf
		if($tgl_acc != '') 		$this->db->where('tgl_acc', $tgl_acc); 			// 11 tgl_acc
		if($tgl_acc != '') 		$this->db->like('A.nama', $nama); 				// 12 nama mhs
		if($id_prodi != '') 	$this->db->where('mhs.id_prodi', $id_prodi);	// 14 id_prodi
		if($count != ''){														// 13 others
			if($count == 'count') return $this->db->count_all_results();
		}else return $this->db->get();
	}
	
	function ajukan_jdl()
	{
		$jdl = array(
		   'nim' => $this->session->userdata('id_user'),
		   'jdl' => $this->input->post('jdl_ta'),
		   'abstraksi' => $this->input->post('abstraksi'),
		   'jenis' => $this->input->post('jenis')
		);
		
		$this->db->insert('pengajuan_jdl', $jdl); 
	}
	
	function penyetujuan_jdl()
	{
		// tentukan waktu sekarang
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		
		// update tabel pengajuan judul
		$jdl = array(
		   'tgl_ds7i' => $now,
		   'stts' => $this->input->post('pers7an')
		);
		$this->db->where('id_jdl', $this->input->post('id_jdl'));
		$this->db->update('pengajuan_jdl', $jdl);		
	}
	
	function ajukan_prop($id_jdl)
	{
		$prop = array(
		   'id_jdl' => $id_jdl,
		   'rek_dsn_pembb' => $this->input->post('rek_pembb'),
		);
		
		$this->db->insert('proposal', $prop); 
	}
	function penyetujuan_prop()
	{
		// tentukan waktu sekarang
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		
		// update tabel pengajuan prop
		$prop = array(
		   'tgl_acc' => $now,
		   'stts_acc' => $this->input->post('pers7an')
		);
		$this->db->where('id_prop', $this->input->post('id_prop'));
		$this->db->update('proposal', $prop);		
	}
	
	function jadikan_ta($id_prop)
	{
		$prop = $this->proposal_ta('','','',$id_prop)->row();
		
		$ta = array(
		   'id_prop' => $id_prop,
		   'nim' => $prop->nim,
		   'th_akademik ' => $this->thn_aka()
		);
		
		$this->db->insert('ta', $ta);
		
		$ta = $this->sipbta_model->ta('select','','','','','','',$id_prop)->row();
		
		$jdl = array(
		   'id_ta' => $ta->id_ta,
		   'jdl' => $prop->jdl
		);
		
		$this->db->insert('riwayat_jdl', $jdl);
	}
	
	function idta_tanpa_prop($nim)
	{
		$sql = 	"SELECT a.id_ta, c.nm_dsn
				FROM ta a
				JOIN riwayat_pembb b
				USING ( id_ta ) 
				JOIN dsn c
				USING ( id_dsn ) 
				WHERE a.id_prop IS NULL  
				AND nim = ?";
					
		return $this->db->query($sql, array($nim));
	}
	
	function idta_link_to_prop($id_ta, $id_prop)
	{
		$det = array(
		   'id_prop' => $id_prop,
		   'th_akademik ' => $this->thn_aka()
		);
		$this->db->where('id_ta', $id_ta);
		$this->db->update('ta', $det);
		
		$ta = $this->sipbta_model->ta('select','','','','','','',$id_prop)->row();
		
		$jdl = array(
		   'id_ta' => $ta->id_ta,
		   'jdl' => $prop->jdl
		);
		
		$this->db->insert('riwayat_jdl', $jdl);
	}
	
	function thn_aka(){
		
		date_default_timezone_set('Asia/Jakarta');
		$bln = (int) date('m');
		$th = (int) date('Y');
		
		if($bln >= 9){
			$th_aka = $th."/".($th+1);
		}else{
			$th_aka = ($th-1)."/".$th;
		}
		return $th_aka;
	}
}