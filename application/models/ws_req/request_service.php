<?php
function request_service($service_params){
	function merge_array($array){
		$key = array('pKey' => 'xxx'); // key anda
		$request = $array[0];
		
		$request_params = array(array_merge((array)$key, (array)$request));
		$data_params = array($array[1]);
		
		return $service_params = array_merge((array)$request_params, (array)array($array[1]));
	}
	
	// me-load library nusoap.
	require_once 'nusoap/nusoap.php';
	// konek ke web service
	$sc = new nusoap_client('http://sijay.stmikayani.ac.id/jangkrik/index.service.php',false);
	// ngecek berhasil konek atau tidak
	if (false !== $error = $sc->getError()) {
		return false;
	}
	
	// menyiapkan argumen utk panggilan ke web service
	$service_params = merge_array($service_params);
	
	// panggil method Dispatch dari web service
	$result = $sc->Call('Dispatch', $service_params);
	// ngecek lagi kesalahan.
	if (false !== $error = $sc->getError()) {
		return false;
	}
	return $result;
}
?>