<?php
// form ------------
function form_input_readonly($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'text', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." readonly/>";
	}

function form_input_date($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'date', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}

function form_input_time($data = '', $value = '', $extra = '')
	{
		$defaults = array('type' => 'time', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);

		return "<input "._parse_form_attributes($data, $defaults).$extra." />";
	}

function complete_form($form_heading,$form_dest,$fields,$buttons,$form_att,$ul_att)
{
	$form = form_open($form_dest,$form_att);
	
	$form .= heading($form_heading, 3);	
	$form .= ul($fields, $ul_att);
	$form .= $buttons;
	
	$form .= form_close();
	
	return $form;
}
?>