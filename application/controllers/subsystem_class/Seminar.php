<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminar extends CI_Controller {

	private $tahap;
	
	function __construct()
	{
		parent::__construct();
		$this->tahap = 'seminar';
				
		$this->load->model('user_model/dsn_model','',true);
		
		$this->load->model('subsystem_model/seminar_model','',true);
		$this->load->model('subsystem_model/pembimbingan_model','',true);
		$this->load->model('sipbta_model','',true);
	}
	
	function index()
	{
		
	}
	
	// mhs
	function show_prasyarat_sem(){}
	
	function pendf_sem_form()
	{	
		$nim = $this->session->userdata('id_user');

		$id_bimbb = $this->pembimbingan_model->dist_pembb('','','','',$nim)->row()->id_bimbb;
		$count = $this->pembimbingan_model->riwayat_bimbb('select',$id_bimbb,'','','count'); 
		if($count < 5) {
			$data['pemberitahuan'] = 'Untuk mendaftar seminar, mahasiswa melaksanakan pembimbingan minimal 5 kali.';
			$data['pemberitahuan'] .= '<br /> saat ini anda telah melaksanakan bimbingan sebanyak '.$count.' kali';
			$data['page_title'] = 'Form pendaftaran seminar';
			$this->template->display('pemberitahuan', $data, $this->tahap);	
		}else{
			$ta = $this->sipbta_model->ta('select','','','','', $nim, '','', 'Aktif','Aktif')->row();
			$sem = $this->seminar_model->list_pendf_sem('','','tgl_pendf DESC','','',$nim);
			
			
			$data['nim'] = ': '.$ta->nim;
			$data['nama'] = ': '.$ta->nama;
			$data['jdl'] = ': '.$ta->jdl.' (ingin merubah judul? klik '.anchor('user_class/mahasiswa/ubah_jdl/1','di sini').').';
			$data['pembb'] = ': '.$ta->nm_dsn;
			$data['tgl_sem'] = '<label for="tgl_sem">Usulan tanggal sem</label><input name="tgl_sem" type="date" />';
			$data['pukul'] = '<label for="pukul">Pukul</label><input name="pukul" type="time" />';
			$data['ket'] = 'Dengan ini saya mendaftar seminar TA sesuai ketentuan yang berlaku.
				<br />(ketentuan dan syarat mendaftar seminar TA lik <a href="google.com">disini</a>).<br />';
			$data['button'] = '<button type="submit" name="jc1" data-inline="true">Daftar</button>';
			$data['button'] .= anchor('user_class/mahasiswa/mendaftar_sem" data-role="button" data-inline="true','Batal');
			if($sem->num_rows() > 0){
				if($sem->row()->stts_sem == 'Belum terjadwal' || $sem->row()->stts_sem == 'Sudah terjadwal' || $sem->row()->stts_sem == 'Sudah dilaksanakan'){
					$tgl_wkt = explode(" ", $sem->row()->tgl_sem);
					
					if($sem->row()->stts_sem == 'Belum terjadwal')
					$data['tgl_sem'] = '<label for="tgl_sem">Usulan tanggal seminar</label> : '.$tgl_wkt[0];
					else $data['tgl_sem'] = '<label for="tgl_sem">Tanggal seminar</label> : '.$tgl_wkt[0];
					$data['pukul'] = '<label for="pukul">Pukul</label>: '.$tgl_wkt[1];
					$data['ket'] = '<label for="tgl_sem">Status seminar</label> : <strong>'.$sem->row()->stts_sem.'</strong><br />';
					$data['button'] = anchor('user_class/mahasiswa/mendaftar_sem" data-role="button" data-inline="true','kembali');
				}
			}
			$data['action'] = site_url('/subsystem_class/seminar/pendf_sem_proses');
			$data['page_title'] = 'Form pendaftaran seminar';
			
			$this->template->display('subsystem_view/seminar/pendf_seminar_form', $data, $this->tahap);
		}
	}
	
	function pendf_sem_proses()
	{
		$tgl = $this->input->post('tgl_sem');
		$jam = $this->input->post('pukul');
		$usulan_waktu = str_replace("/","-",$tgl).' '.$jam;
		
		$this->seminar_model->daft_sem($usulan_waktu);
		redirect('user_class/mahasiswa/mendaftar_sem');
	}
	
	function seminar_det($id_sem)
	{
		$sem = $this->seminar_model->list_pendf_sem('','','','',$id_sem);
		$ta = $this->sipbta_model->ta('select','','','','', $sem->row()->nim, '','', 'Aktif','Aktif')->row();
		$tgl_wkt = explode(" ", $sem->row()->tgl_sem);
		
		$data['tgl_daft'] = ': '.$sem->row()->tgl_pendf;
		$data['nim'] = ': '.$sem->row()->nim;
		$data['nama'] = ': '.$sem->row()->nama;
		$data['jdl'] = ': '.$ta->jdl;
		$data['pembb'] = ': '.$ta->nm_dsn;
		$data['pembb_sem'] = ': '.$sem->row()->nm_dsn;
		
		if($sem->row()->stts_sem == 'Belum terjadwal')	$data['tgl_sem'] = ': '.$tgl_wkt[0].' (Usulan tanggal seminar)';
		else $data['tgl_sem'] = ': '.$tgl_wkt[0].' (Terjadwal)';
		$data['pukul'] = ': '.$tgl_wkt[1];
		$data['ruang'] = ': '.$sem->row()->ruang;
		$data['button'] = '<label for="tgl_sem">Status seminar</label> : <strong>'.$sem->row()->stts_sem.'</strong><br />';
		$data['button'] .= '<br />'.anchor('user_class/mahasiswa/mendaftar_sem" data-role="button" data-inline="true','kembali');
		
		$data['action'] = site_url('/subsystem_class/seminar/pendf_sem_proses');
		$data['page_title'] = 'Form pendaftaran seminar';
		
		$this->template->display('subsystem_view/seminar/pel_sem', $data, $this->tahap);
	}
	
	function show_jdwl_sem_all($offset = 0)
	{
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$sem_paged = $this->seminar_model->list_pendf_sem($limit,$offset,'tgl_sem DESC')->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('user_class/kaprodi/lihat_jdwl_sem/');
		$conf['total_rows'] = $this->seminar_model->list_pendf_sem('','','tgl_sem DESC')->num_rows(); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
				
		// generate list data
		$data['jadwal'] = '';
		foreach ($sem_paged as $jdwl)
		{
			$jdl = $this->sipbta_model->jdl_ta($jdwl->id_ta, 'Aktif')->row();
			
			$data['jadwal'] .= "<div id=\"jdwl_item\">
									<div id=\"".str_replace(" ","","$jdwl->stts_sem")."\" class=\"title\">
										<div id=\"$jdwl->id_prodi\" class=\"prodi\"></div>
										<div class=\"waktu\">
											$jdwl->stts_sem <br />
											$jdwl->tgl_sem <br />
											[ $jdwl->ruang ]
										</div>
									</div>
									<div id=\"item_det\" class=\"det\">
										Nama : <strong>$jdwl->nama</strong> <br />
										Dosen pembimbing TA : $jdwl->nm_dsn <br />
										Judul TA : $jdl->jdl
									</div>
								</div>";
		}
		
		// view
		$data['id_jdwl'] = 'seminar';
		$data['page_title'] = 'Jadwal seminar';
		
		$this->template->display('subsystem_view/seminar/jadwal_sem', $data, $this->tahap);
	}
	
	function show_nilai_sem_pendd(){}
	
	// opsdik
	function jdwl_sem(){}
	
	function prasyarat_sem_form(){}
	
	function list_pendf_sem()
	{
		// load data
		$list_pendf = $this->seminar_model->list_pendf_sem('','','', '');
		$list_pendf_sem = $list_pendf->result();
		$count = $list_pendf->num_rows(); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('');
		if($count > 0) $this->table->set_heading('No', 'Nim', 'Nama','Tanggal daftar', 'Status', 'Tindakan');
		$i=0;
		foreach ($list_pendf_sem as $pendf)
		{
			$def_link = anchor('subsystem_class/seminar/tanggapi_pendf_sem_form/'.$pendf->id_sem,'Tentukan jadwal',array('class'=>'view'));
			if($pendf->stts_sem == 'Sudah terjadwal') {
				$link = str_replace("Tentukan jadwal","Lihat",$def_link);
				$link .= ' / '.anchor('subsystem_class/seminar/tanggapi_pendf_sem_form/'.$pendf->id_sem.'/edit','Ubah',array('class'=>'view'));
			}
			elseif ($pendf->stts_sem == 'Belum terjadwal') $link = $def_link;
			elseif ($pendf->stts_sem == 'Sudah dilaksanakan') $link = str_replace("Tentukan jadwal","Lihat",$def_link);
			$this->table->add_row(++$i, $pendf->nim,$pendf->nama, $pendf->tgl_pendf,$pendf->stts_sem, $link);
		}
		
		if($count > 0) $list = $this->table->generate(); else $list = 'Belum ada mahasiswa yang mendaftar seminar.';
		
		$data['pagination'] = '';
		$data['table'] = $list;
		$data['page_title'] = 'List pendaftar seminar';
		$this->template->display('subsystem_view/seminar/list_pendf_sem', $data, $this->tahap);
	}
	
	function tanggapi_pendf_sem_form($id_sem, $opt = '')
	{
		// load data
		$sem = $this->seminar_model->list_pendf_sem('','','', '', $id_sem)->row();
		$ta = $this->sipbta_model->ta('select', '','','',$sem->id_ta, '','','','Aktif','Aktif')->row();
		$tgl_wkt = explode(" ", $sem->tgl_sem);
		// view
		$data['action'] = site_url('/subsystem_class/seminar/tanggapi_pendf_sem_proses');
		$data['nim'] = ': '.$sem->nim.form_hidden('id_sem', $id_sem);
		$data['nama'] = ': '.$sem->nama;
		$data['jdl'] = ': '.$ta->jdl;
		$data['pembb'] = ': '.$ta->nm_dsn;
		$data['tgl_daft'] = ': '.$sem->tgl_pendf;
		$data['pembb_sem'] = $this->dsn_model->dsn_option($sem->id_dsn);
		$data['tgl_sem'] = '<input name="tgl_sem" type="date" value="'.$tgl_wkt[0].'" placeholder="Usulan tanggal: '.$tgl_wkt[0].'"/>';
		$data['pukul'] = '<input name="pukul" type="time" value="'.$tgl_wkt[1].'" placeholder="Usulan waktu: '.$tgl_wkt[1].'" />';
		$data['ruang'] = '<input name="ruang" type="text" placeholder="Ruang seminar" />';

		$data['button'] = '<button type="submit" name="jc1" data-inline="true">Tentukan</button>';
		$data['button'] .= anchor('user_class/opsdik/lihat_list_pendf_sem" data-role="button" data-inline="true','Kembali');
		
		if(($sem->stts_sem == 'Sudah dilaksanakan' || $sem->stts_sem == 'Sudah terjadwal') && $opt != 'edit'){
			$data['pembb_sem'] = ': '.$sem->nm_dsn; 
			$data['tgl_sem'] = ': '.$tgl_wkt[0];
			$data['pukul'] = ': '.$tgl_wkt[1];
			$data['ruang'] = ': '.$sem->ruang;
			$data['button'] = anchor('user_class/opsdik/lihat_list_pendf_sem" data-role="button" data-inline="true','Kembali');
		}
		
			
		
		$data['page_title'] = 'Penentuan pelaksanaan seminar';
		$this->template->display('subsystem_view/seminar/pel_sem', $data, $this->tahap);
	}
	
	function tanggapi_pendf_sem_proses()
	{
		$id_sem = $this->input->post('id_sem');
		$tgl = $this->input->post('tgl_sem');
		$jam = $this->input->post('pukul');
		$waktu = str_replace("/","-",$tgl).' '.$jam;
		$ruang = $this->input->post('ruang');
		$pembb = $this->input->post('rek_pembb');
		
		$this->seminar_model->tentukan_seminar($id_sem, $waktu, $ruang, $pembb);
		
		redirect('subsystem_class/seminar/tanggapi_pendf_sem_form/'.$id_sem);
	}
	
	function cetak_presensi_seminar(){}
	
	// dsn
	function show_request_jd_pengg_pembb_sem(){}
	
	function request_jd_pembb_sem_proses(){}
	
	// pembb	
	function show_list_req_sem(){}
	
	function tanggapi_req_sem_form(){}
	
	function tanggapi_req_sem_proses(){}
	
	function penilaian_sem_form($id_sem)
	{
		$sem = $this->seminar_model->list_pendf_sem('','','','',$id_sem)->row();
		$jdl = $this->sipbta_model->jdl_ta($sem->id_ta, 'Aktif');
		// view
		$data['action'] = site_url('/subsystem_class/seminar/penilaian_sem_proses');
		$data['nim'] = $sem->nim.form_hidden('id_sem', $sem->id_sem);
		$data['nama'] = $sem->nama;
		$data['jdl'] = $jdl->row()->jdl;
		$data['tgl_sem'] = $sem->tgl_sem;
		$data['ruang'] = $sem->ruang;
		if(is_null($sem->penilaian)){
			$data['penilaian'] = '<select name="penilaian" >
								    <option value="Layak untuk ujian">Layak untuk ujian</option>
								    <option value="Mengulang seminar">Mengulang seminar</option>
								    <option value="Perbaiki">Perbaiki</option>
								    <option value="Penilaian lain">Penilaian lain</option>
								  </select>';
			$data['cttn'] = '<textarea cols="40" rows="8" name="cttn_sem" placeholder="Catatan untuk mahasiswa"></textarea>';
			$data['btn'] = '<button type="submit" data-inline="true">Submit</button>';
			$data['btn'] .= anchor('user_class/pembimbing/menilai_sem" data-role="button" data-inline="true','Kembali');
		}else{
			$data['penilaian'] = $sem->penilaian;
			$data['cttn'] = $sem->cttn_sem;
			$data['btn'] = anchor('user_class/pembimbing/menilai_sem" data-role="button" data-inline="true','Kembali');
		}
		
		
		$data['page_title'] = 'Penilaian seminar';
		$this->template->display('subsystem_view/seminar/penilaian_seminar', $data, $this->tahap);
	}
	
	function penilaian_sem_proses()
	{
		$id_sem = $this->input->post('id_sem');
		$penilaian = $this->input->post('penilaian');
		$cttn = $this->input->post('cttn_sem');
		
		$this->seminar_model->berinilai_seminar($id_sem, $penilaian, $cttn);
		
		redirect('subsystem_class/seminar/penilaian_sem_form/'.$id_sem);
	}
}