<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proposal extends CI_Controller {

	private $tahap;

	function __construct()
	{
		parent::__construct();
		
		$this->tahap = 'proposal';		
		
		// load model
		$this->load->model('sipbta_model', '', TRUE);
		$this->load->model('subsystem_model/Proposal_model','',true);
		$this->load->model('subsystem_model/Pembimbingan_model','',true);
		$this->load->model('user_model/Dsn_model', '', TRUE);
		
		// look and fill table
		$this->table->set_template(array ( 'row_alt_start' => '<tr class="alt">'));
	}
	
	function aadc()
	{
		$aabb = dirname(__FILE__);
		print_r($aabb);
		$a = $this->Proposal_model->ta('select')->row();
		echo $a->nim;
	}
	
	function index(){}
	
	function cek_query()
	{
		echo $this->Proposal_model->proposal_ta('','','tgl_pengajuan DESCs','','2010.01666.31.1262');
	}
	// mhs
	function pengajuan_jdl($offset = 0)
	{
		// pemberitahuan punya jdl atau tdk
		// tabel list jdl yg pernah diajukan + link detail jdl
		// link ke form pengajuan judul baru
		$nim = $this->session->userdata('id_user');
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$jdl_paged = $this->Proposal_model->jdl_ta($limit,$offset,'tgl_pengajuan DESC', '', $nim)->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('subsystem_class/proposal/pengajuan_jdl/');
		$conf['total_rows'] = $this->Proposal_model->jdl_ta('','','','',$nim,'','','','','','count'); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Judul', 'Jenis', 'Tanggal pengajuan', 'Status', 'Action');
		$i = 0 + $offset;
		foreach ($jdl_paged as $jdl)
		{
			$ac = anchor('subsystem_class/proposal/show_pengajuan_jdl_det/'.$jdl->id_jdl,'Lihat',array('class'=>'view'));
			
			if($jdl->stts == 'Disetujui' || $jdl->stts == 'Belum Disetujui')
				$ac .= ' / '.anchor('subsystem_class/proposal/pengajuan_prop','Ajukan proposal',array('class'=>'view'));
			$this->table->add_row(++$i, $jdl->jdl, $jdl->jenis, $jdl->tgl_pengajuan, $jdl->stts, $ac);
		}
		
		
		// view
		if($conf['total_rows'] != 0){ 					// sudah pernah mengajukan judul
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum pernah mengajukan jdl
			$data['pemberitahuan'] = 'Anda belum pernah mengajukan judul, silahkan klik link di bawah ini:';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		$data['link'] = anchor('subsystem_class/proposal/pengajuan_jdl_form/" data-role="button" data-inline="true','Ajukan judul baru');
		
		$data['page_title'] = 'List pengajuan judul';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	function pengajuan_jdl_form()
	{
		$nim = $this->session->userdata('id_user');
		
		/* chek 
		- sudah ada judul yang belum disetujui?
		- sudah ada judul yang disetujui?
		- sudah ada proposal yang belum disetujui?
		- sudah ada proposal yang disetujui?
		*/
		
		$jdl_menghadap = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Menghadap Kaprodi');
		$jdl_belum = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Belum disetujui');
		$jdl_disetujui = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Disetujui');
		$prop_menghadap= $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Menghadap Kaprodi');
		$prop_belum = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Belum disetujui');
		$prop_disetujui = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Disetujui');
		if($jdl_disetujui->num_rows() > 0) {
			redirect('subsystem_class/proposal/pengajuan_prop');
		}
		elseif($prop_disetujui->num_rows() > 0 || $prop_belum->num_rows() > 0 || $prop_menghadap->num_rows() > 0){
			redirect('subsystem_class/proposal/pengajuan_prop_form');
		}else{
			// form kosong pengajuan jdl
			$options = array(
				'k'					=> '-- Pilih Jenis TA --',
				'Multimedia' 		=> 'Multimedia',
				'Sistem Informasi' 	=> 'Sistem Informasi',
				'Aplikasi Desktop' 	=> 'Aplikasi Desktop',
				'Aplikasi Web' 		=> 'Aplikasi Web',
				'Aplikasi Mobile' 	=> 'Aplikasi Mobile',
				'Skripsi' 			=> 'Skripsi',
				'Lainnya' 			=> 'Lainnya'
			);
			
			$jns = form_dropdown('jenis" data-native-menu="false', $options, 'k');
			
			$data['page_title'] = 'Form pengajuan judul TA';
			
			$data['action'] = site_url('/subsystem_class/proposal/pengajuan_jdl_proses');
			$data['jdl_ta'] = '<textarea cols="40" rows="8" name="jdl_ta" placeholder="Apakah judul TA anda?"></textarea>';
			$data['abstraksi'] = '<textarea cols="40" rows="8" name="abstraksi" placeholder="Berikan gambaran dari judul TA anda"></textarea>';
			$data['jenis'] = $jns;
			$data['button'] = '<button type="submit" name="jc1" data-inline="true">Ajukan judul</button>';
			$data['button'] .= anchor('user_class/mahasiswa/ajukan_jdl" data-role="button" data-inline="true','Batal');
			
			if($jdl_belum->num_rows() > 0){
				$data['jdl_ta'] = $jdl_belum->row()->jdl;
				$data['abstraksi'] = $jdl_belum->row()->abstraksi;
				$data['jenis'] = $jdl_belum->row()->jenis.' -- [ Judul belum disetujui ]';
				$data['button'] = anchor('subsystem_class/proposal/pengajuan_prop" data-role="button" data-inline="true','Ajukan proposal');
				$data['button'] .= anchor('user_class/mahasiswa/ajukan_jdl" data-role="button" data-inline="true','Kembali');
				$data['action'] = '#';
			}elseif($jdl_menghadap->num_rows() > 0){
				$data['jdl_ta'] = $jdl_menghadap->row()->jdl;
				$data['abstraksi'] = $jdl_menghadap->row()->abstraksi;
				$data['jenis'] = $jdl_menghadap->row()->jenis.' -- [ silahkan menghadap kaprodi ]';
				$data['button'] = anchor('user_class/mahasiswa/ajukan_jdl" data-role="button" data-inline="true','Kembali');
				$data['action'] = '#';
			}
			
			$this->template->display('subsystem_view/proposal/pengajuan_jdl', $data, $this->tahap);
		}
	}
	function pengajuan_jdl_proses()
	{
		// proses dari form pengajuan jdl
		$this->Proposal_model->ajukan_jdl();
		redirect('user_class/mahasiswa/ajukan_jdl');
	}
	
	function pengajuan_prop($offset = 0)
	{
		// pemberitahuan punya proposal atau tdk
		// tabel list proposal yg pernah diajukan + link detail proposal
		// link ke form pengajuan proposal baru
		
		$nim = $this->session->userdata('id_user');
		
		$rek_pembb_count = $this->Pembimbingan_model->list_req_pembb('','','',$nim,'','','','','','count');
		if($rek_pembb_count > 0){
			$rek_pembb = $this->Pembimbingan_model->list_req_pembb('','','waktu DESC',$nim)->row();
			$dsn_dsn = ": ".$rek_pembb->nm_dsn.' -- Status request '.$rek_pembb->stts_req;
		}else $dsn_dsn = ": Belum request dosen pembimbing TA";
				
		
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$prop_paged = $this->Proposal_model->proposal_ta($limit,$offset,'tgl_pendf DESC', '', $nim)->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('subsystem_class/proposal/pengajuan_prop/');
		$conf['total_rows'] = $this->Proposal_model->proposal_ta('','','','',$nim,'','','','','','','','count'); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data 
		
		$this->table->set_empty("&nbsp;");
		
		$this->table->set_heading('No', 'Judul', 'Jenis', 'Tanggal pengajuan', 'Status', 'Action');
		$i = 0 + $offset;
		foreach ($prop_paged as $prop)
		{
			$this->table->add_row(++$i, $prop->jdl, $prop->jenis, $prop->tgl_pendf, $prop->stts_acc,
				anchor('subsystem_class/proposal/show_pengajuan_prop_det/'.$prop->id_prop,'Lihat',array('class'=>'view')));
		}
		
		// view
		$data['pemberitahuan'] = '';
		$data['pagination'] = '';
		$data['table'] = '';
		$data['link'] = '';
		
		// jika ada judul yang disetujui tampilkan.
		$jdl_menghadap = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Menghadap Kaprodi');
		$jdl_belum = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Belum disetujui');
		$judul = $this->Proposal_model->jdl_ta('','','tgl_ds7i DESC','',$nim,'','','','','Disetujui');
		if($jdl_menghadap->num_rows > 0) redirect('subsystem_class/proposal/pengajuan_jdl_form/');
		if($judul->num_rows() > 0 || $jdl_belum->num_rows() > 0 ) {
			
			$data['pemberitahuan'] .= form_open('/subsystem_class/proposal/pengajuan_jdl_to_prop_proses');
			if($judul->num_rows() > 0) {
				$jdl_dis7i = $judul->row();
				$data['pemberitahuan'] .= '<strong>[ Selamat judul yang anda ajukan telah disetujui oleh prodi, ';
				$data['pemberitahuan'] .= 'silahkan mengajukan proposal ]</strong><br/><br/>';
				$data['pemberitahuan'] .= '<label>Judul anda </label>: '.$jdl_dis7i->jdl.form_hidden('id_jdl', $jdl_dis7i->id_jdl).br(2);
				$data['pemberitahuan'] .= '<label>Abstraksi </label>: '.$jdl_dis7i->abstraksi.br(2);
			}else{
				$data['pemberitahuan'] .= '<strong>[ Judul yang anda ajukan belum disetujui oleh prodi, ';
				$data['pemberitahuan'] .= 'anda bisa langsung mengajukan proposal dengan judul tersebut ]</strong><br/><br/>';
				$data['pemberitahuan'] .= '<label>Judul anda </label>: '.$jdl_belum->row()->jdl.form_hidden('id_jdl', $jdl_belum->row()->id_jdl).br(2);
				$data['pemberitahuan'] .= '<label>Abstraksi </label>: '.$jdl_belum->row()->abstraksi.br(2);
			}
						
			$data['pemberitahuan'] .= '<label>Rekomendasi pembimbing </label> '.$dsn_dsn.br(3);
			$data['pemberitahuan'] .= '<label for="file">file proposal </label>: <input type="file" name="file" >';
			if( $jdl_menghadap->num_rows() == 0)
			$data['pemberitahuan'] .= form_submit('jdl_to_prop" data-inline="true', 'Ajukan proposal');
			$data['pemberitahuan'] .= form_close(br(2));
		}else {
			$data['pemberitahuan'] = '';
		}
		
		//======
		if($conf['total_rows'] != 0){ 					// sudah pernah mengajukan judul
			$data['pagination'] .= 'Riwayat pengajuan proposal: <br/><br/>'.$this->pagination->create_links();
			$data['table'] .= $this->table->generate();
		}else {											// belum pernah mengajukan jdl
			$data['pagination'] .= '[ Anda belum pernah mengajukan proposal ]';
		}
		if($judul->num_rows() == 0 && $jdl_belum->num_rows() == 0)
		$data['link'] = anchor('subsystem_class/proposal/pengajuan_prop_form/" data-role="button" data-inline="true','Ajukan proposal baru');
		if($judul->num_rows() > 0) $data['link'] = '';
		
		$data['page_title'] = 'List pengajuan proposal';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	
	function pengajuan_jdl_to_prop_proses(){
		$id_jdl = $this->input->post('id_jdl');
		
		// masukkan id_jdl ke tabel proposal
		$this->Proposal_model->ajukan_prop($id_jdl);
		redirect('user_class/mahasiswa/ajukan_prop');
	}
	
	function pengajuan_prop_form()
	{
		// cek pembimbing
		// jika sudah ada pembimbing, tampilkan.
		// jika sudah request pembimbing dan di setujui oleh dsn tsb tampilkan.
		
		
		$nim = $this->session->userdata('id_user');
		
		$rek_pembb_count = $this->Pembimbingan_model->list_req_pembb('','','',$nim,'','','','','','count');
		if($rek_pembb_count > 0){
			$rek_pembb = $this->Pembimbingan_model->list_req_pembb('','','waktu DESC',$nim)->row();
			$data['rek_pembb'] = ": ".$rek_pembb->nm_dsn.' -- Status request '.$rek_pembb->stts_req;
		}else $data['rek_pembb'] = ": Belum request dosen pembimbing TA";
		
		
		// form kosong pengajuan proposal
		$options = array(
			'k'					=> '-- Pilih Jenis TA --',
			'Multimedia' 		=> 'Multimedia',
			'Sistem Informasi' 	=> 'Sistem Informasi',
			'Aplikasi Desktop' 	=> 'Aplikasi Desktop',
			'Aplikasi Web' 		=> 'Aplikasi Web',
			'Aplikasi Mobile' 	=> 'Aplikasi Mobile',
			'Skripsi' 			=> 'Skripsi',
			'Lainnya' 			=> 'Lainnya'
		);
		
		$jns = form_dropdown('jenis', $options, 'k');
		
		$prop_menghadap = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Menghadap Kaprodi');
		$prop_belum = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Belum disetujui');
		$prop_disetujui = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Disetujui');
		if($prop_belum->num_rows() > 0){
			$a = $prop_belum->row();
			
			$data['action'] = '#';
		 	$data['jdl_ta'] = ': '.$a->jdl;
			$data['abstraksi'] = ': '.$a->abstraksi;
			$data['jenis'] = ': '.$a->jenis;
			$data['file'] = ': - <br/><br/><strong> [ Proposal anda belum disetujui, silahkan menunggu atau hubungi kaprodi untuk mengangkat tema baru ]</strong>';
			$data['button'] = anchor('user_class/mahasiswa/ajukan_prop" data-role="button" data-inline="true','Kembali');
		}
		
		// chek sudah ada proposal yang disetujui apa belum.
		
		elseif($prop_disetujui->num_rows() > 0){
			$a = $prop_disetujui->row();
			
			$data['action'] = '';
		 	$data['jdl_ta'] = ': '.$a->jdl;
			$data['abstraksi'] = ': '.$a->abstraksi;
			$data['jenis'] = ': '.$a->jenis;
			$data['file'] = ':<strong> [ Proposal anda telah disetujui, silahkan menghubungi Kaprodi untuk mengangkat tema baru ]</strong>';
			$data['button'] = anchor('user_class/mahasiswa/ajukan_prop" data-role="button" data-inline="true','Kembali');
		}elseif($prop_menghadap->num_rows() > 0 ){
			$a = $prop_menghadap->row();
			
			$data['action'] = '';
		 	$data['jdl_ta'] = ': '.$a->jdl;
			$data['abstraksi'] = ': '.$a->abstraksi;
			$data['jenis'] = ': '.$a->jenis;
			$data['file'] = ':<strong> [ Silahkan menghadap kaprodi terkait proposal yang anda ajukan ]</strong>';
			$data['button'] = anchor('user_class/mahasiswa/ajukan_prop" data-role="button" data-inline="true','Kembali');
		}else{
			$data['action'] = site_url('/subsystem_class/proposal/pengajuan_prop_proses');
		 	$data['jdl_ta'] = '<textarea cols="40" rows="8" name="jdl_ta" placeholder="Apakah judul TA anda?"></textarea>';
			$data['abstraksi'] = '<textarea cols="40" rows="8" name="abstraksi" placeholder="Berikan gambaran dari judul TA anda"></textarea>';
			$data['jenis'] = $jns;
			$data['file'] = '<input type="file" name="file">';
			$data['button'] = '<button type="submit" name="jc1" data-inline="true">Ajukan proposal</button>';
			$data['button'] .= anchor('user_class/mahasiswa/ajukan_prop" data-role="button" data-inline="true','Batal');
		}
		
		
		$data['page_title'] = 'Form pengajuan proposal TA';
		$this->template->display('subsystem_view/proposal/pengajuan_prop', $data, $this->tahap);
	}
	
	function pengajuan_prop_proses()
	{
		// proses dari pengajuan proposal form
		// 1 masukkan jdl ke tabel judul 
		// 2 masukka id judul ke tabel proposal
		
		// 1 masukkan ke tabel judul
		$this->Proposal_model->ajukan_jdl();
		
		// ambil id_jdl yang baru di masukkan
		$nim = $this->session->userdata('id_user');
		$id_jdl = $this->Proposal_model->jdl_ta('','','tgl_pengajuan DESC','',$nim)->row()->id_jdl;
		
		// 2 masukkan id_jdl ke tabel proposal
		$this->Proposal_model->ajukan_prop($id_jdl);
		
		redirect('user_class/mahasiswa/ajukan_prop');
	}
	
	function request_pembb_form($offset = 0)
	{
		$nim = $this->session->userdata('id_user');
		$prop_menghadap= $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Menghadap Kaprodi');
		$prop_belum = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Belum disetujui');
		$prop_disetujui = $this->Proposal_model->proposal_ta('','','tgl_pendf DESC', '', $nim, '','','','Disetujui');
		
		$abcd = $this->Pembimbingan_model->dist_pembb('','','','',$nim)->row();
		$count = $this->Pembimbingan_model->riwayat_bimbb('select', $abcd->id_bimbb,'','','count');
		
		if($prop_menghadap->num_rows() > 0 || $prop_belum->num_rows() > 0 || $prop_disetujui->num_rows() > 0 || $count < 5){
			if($count > 0){
				$data['pemberitahuan'] = 'Dosen pembimbing anda adalah '.$abcd->nm_dsn;
			}else{
				$data['pemberitahuan'] = 'Anda tidak dapat merequest pembimbing setelah mengajukan proposal,';
				$data['pemberitahuan'] .= '<br/> silahkan tunggu pengumuman distribusi pembimbing dari Opsdik.';
			}
			$data['pagination'] = '';
			$data['table'] = '';
			$data['action'] = '';
			$data['calon_pembb'] = '';
			$data['button'] = '';
		}else{
			// pemberitahuan punya proposal atau tdk
			// tabel list proposal yg pernah diajukan + link detail proposal
			// link ke form pengajuan proposal baru
			
			$limit = 3;
			
			// offset
			$uri_segment = 4;
			$offset = $this->uri->segment($uri_segment);
			
			// load data
			$req_pembb_paged = $this->Pembimbingan_model->list_req_pembb($limit,$offset,'waktu DESC', $this->session->userdata('id_user'))->result();
			
			// generate pagination
			$conf['base_url'] = site_url('user_class/mahasiswa/request_pembb');
			$conf['total_rows'] = $this->Pembimbingan_model->list_req_pembb('','','','','','','','','','count'); // count all result
			$conf['per_page'] = $limit; 
			$conf['uri_segment'] = $uri_segment;
			$conf['num_links'] = 3;
			$conf['first_link'] = 'Awal';
			$conf['last_link'] = 'Akhir';
			$this->pagination->initialize($conf); 
			
			// generate table data
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Tanggal Request', 'Calon pembimbing', 'Status');
			$i = 0 + $offset;
			foreach ($req_pembb_paged as $req_pembb)
			{
				$this->table->add_row(++$i, $req_pembb->waktu, $req_pembb->nm_dsn, $req_pembb->stts_req);
			}
			
			// view
			if($conf['total_rows'] != 0){ 					// sudah pernah mengajukan judul
				$data['pemberitahuan'] = '';
				$data['pagination'] = $this->pagination->create_links();
				$data['table'] = $this->table->generate();
			}else {											// belum pernah mengajukan jdl
				$data['pemberitahuan'] = 'Anda belum pernah request dosen pembimbing tugas akhir.';
				$data['pagination'] = '';
				$data['table'] = '';
				
			}
			
			
			$data['action'] = site_url('/subsystem_class/proposal/request_pembb_proses');
			$data['calon_pembb'] = '<label for="pembb">Calon pembimbing</label>'.$this->Dsn_model->dsn_option();
			$data['button'] = '<input type="submit" value="Request" data-inline="true" />';
		}
		$data['page_title'] = 'Request pembimbing';
		$this->template->display('subsystem_view/proposal/req pembb form', $data, 'pembimbingan');
	}
	
	function request_pembb_proses()
	{
		$this->Pembimbingan_model->req_pembb();
		redirect('user_class/mahasiswa/request_pembb');
	}
	
	function show_tanggapan_pembb(){}
	
	// kaprodi
	function id_prodi(){
		// kaprodi apa?
		$prodi = $this->Dsn_model->is_kaprodi($this->session->userdata('id_user'));
		if($prodi->num_rows() > 0){
			return $prodi->row()->id_prodi;
		}else return $id_prodi = '';
	}
	
	function show_list_pengajuan_jdl($offset = 0)
	{
		$id_prodi = $this->id_prodi();
		
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$jdl_paged = $this->Proposal_model->jdl_ta($limit,$offset,'tgl_pengajuan DESC','','','','','','','','','',$id_prodi)->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('subsystem_class/proposal/show_list_pengajuan_jdl/');
		$conf['total_rows'] = $this->Proposal_model->jdl_ta('','','','','','','','','','','count',$id_prodi); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Nama', 'Judul', 'Jenis', 'Tanggal pengajuan', 'Tanggal disetujui', 'Status', 'Action');
		$i = 0 + $offset;
		foreach ($jdl_paged as $jdl)
		{
			$this->table->add_row(++$i, $jdl->nama, $jdl->jdl,$jdl->jenis, $jdl->tgl_pengajuan, $jdl->tgl_ds7i, $jdl->stts,
				anchor('subsystem_class/proposal/show_pengajuan_jdl_det/'.$jdl->id_jdl,'Lihat',array('class'=>'view')));
		}
		
		
		// view
		
		if($conf['total_rows'] != 0){ 					// sudah pernah mengajukan judul
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum pernah mengajukan jdl
			$data['pemberitahuan'] = 'Belum ada mahasiswa yang mengajukan judul.';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		
		$data['link'] = '';
		
		$data['page_title'] = 'List pengajuan judul';
		
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	
	function show_pengajuan_jdl_det($id_jdl)
	{
		
		$jdl = $this->Proposal_model->jdl_ta('','','',$id_jdl)->row();
		
		$options = array(
			'k'					=> '-- Penyetujuan --',
			'Disetujui' 		=> 'Disetujui',
			'Belum disetujui' 	=> 'Belum Disetujui',
			'Tidak disetujui' 	=> 'Tidak Disetujui',
			'Menghadap Kaprodi'	=> 'Menghadap Kaprodi',
			'Mengangkat Tema Baru' 	=> 'Mengangkat Tema Baru'
		);
		
		$pers7an = form_dropdown('pers7an', $options, 'k');
		
		//view
		$data['page_title'] = 'Penyetujuan judul TA';
		$data['status_jdl'] = ': '.$jdl->stts;
		$data['action'] = site_url('/subsystem_class/proposal/pengajuan_jdl_prop_proses');
		
	 	$data['id_jdl'] = form_hidden('id_jdl', $jdl->id_jdl);
	 	$data['jdl_ta'] = ': '.$jdl->jdl;
		$data['abstraksi'] = ': '.$jdl->abstraksi;
		$data['jenis'] = ': '.$jdl->jenis;
		$data['pers7an'] = $pers7an;
		$data['button'] = '<button type="submit" data-inline="true">Submit</button>';
		
		$data['action'] = site_url('/subsystem_class/proposal/tanggapi_pengajuan_jdl');
		$this->template->display('subsystem_view/proposal/jdl detail', $data, $this->tahap);
	}
	
	function tanggapi_pengajuan_jdl()
	{
		$id_jdl = $this->input->post('id_jdl');
		$this->Proposal_model->penyetujuan_jdl();
		redirect('subsystem_class/proposal/show_pengajuan_jdl_det/'.$id_jdl);
	}
	
	function show_list_pengajuan_prop($offset = 0)
	{
		$id_prodi = $this->id_prodi();
		
		$limit = 20;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$prop_paged = $this->Proposal_model->proposal_ta($limit,$offset,'tgl_pendf DESC','','','','','','','','','','',$id_prodi)->result();
		
		// generate pagination
		$conf['base_url'] = site_url('subsystem_class/proposal/show_list_pengajuan_prop/');
		$conf['total_rows'] = $this->Proposal_model->proposal_ta('','','','','','','','','','','','','',$id_prodi)->num_rows(); // count all result
		$conf['per_page'] = $limit;
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$conf['anchor_class'] = 'pg" ';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No','Nim', 'Nama','Judul', 'Jenis', 'Tanggal pengajuan', 'Status', 'Action');
		$i = 0 + $offset;
		foreach ($prop_paged as $prop)
		{
			$this->table->add_row(++$i,$prop->nim,$prop->nama, $prop->jdl, $prop->jenis, $prop->tgl_pendf, $prop->stts_acc,
				anchor('subsystem_class/proposal/show_pengajuan_prop_det/'.$prop->id_prop,'Lihat',array('class'=>'view')));
		}
		
		// view
		if($conf['total_rows'] != 0){ 					// sudah pernah mengajukan judul
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum pernah mengajukan jdl
			$data['pemberitahuan'] = 'Belum ada mahasiswa yang mengajukan proposal.';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		$data['link'] = '';
		
		$data['page_title'] = 'List pengajuan proposal';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	
	function show_pengajuan_prop_det($id_prop)
	{
		$prop = $this->Proposal_model->proposal_ta('','','',$id_prop)->row();
		$nim = $prop->nim;
		$a = $this->Proposal_model->idta_tanpa_prop($nim);
		
		$rek_pembb_count = $this->Pembimbingan_model->list_req_pembb('','','',$nim,'','','','','','count');
		if($rek_pembb_count > 0){
			$rek_pembb = $this->Pembimbingan_model->list_req_pembb('','','waktu DESC',$nim)->row();
			$pembb = ": ".$rek_pembb->nm_dsn.' -- Status request '.$rek_pembb->stts_req;
		}else $pembb = ": Belum request dosen pembimbing TA";
		
		$options = array(
			'k'						=> '-- Penyetujuan Proposal --',
			'Disetujui' 			=> 'Disetujui',
			'Belum Disetujui' 		=> 'Belum Disetujui',
			'Tidak Disetujui' 		=> 'Tidak Disetujui',
			'Menghadap Kaprodi'		=> 'Menghadap Kaprodi',
			'Mengangkat Tema Baru' 	=> 'Mengangkat Tema Baru'
		);
		
		$pers7an 				= ': '.$prop->stts_acc;
		$data['button'] 		= '<br />'.anchor('user_class/kaprodi/lihat_list_pengajuan_prop" data-role="button" data-inline="true','Kembali');
		$data['tentukan_pembb'] = '';
		if($prop->stts_acc == 'Belum disetujui' || $prop->stts_acc == 'Menghadap Kaprodi') {
			$ded = "<label for=\"tentukan_pembb\">Persetujuan dosen pembimbing</label>";
			$pers7an = form_dropdown('pers7an" data-native-menu="false', $options, 'k');
			$data['tentukan_pembb'] = $ded.$this->Dsn_model->dsn_option();
			if($a->num_rows() > 0) $data['tentukan_pembb'] = $ded.$a->row()->nm_dsn." (Ditentukan saat pembagian pembimbing)";
			$data['button'] 		= '<button type="submit" data-inline="true">Submit</button>';
			$data['button'] 		.= anchor('user_class/kaprodi/lihat_list_pengajuan_prop" data-role="button" data-inline="true','Kembali');
		}
		
		//view
		$data['page_title'] = 'Penyetujuan proposal TA';
		
		$data['nim'] 			= ': '.$prop->nim.form_hidden('nim', $nim);
		$data['nama'] 			= ': '.$prop->nama;
		$data['prodi'] 			= ': '.$prop->nm_prodi;
		$data['id_jdl'] 		= form_hidden('id_jdl', $prop->id_jdl);
	 	$data['id_prop'] 		= form_hidden('id_prop', $prop->id_prop);
	 	$data['jdl_ta'] 		= ': '.$prop->jdl;
		$data['abstraksi'] 		= ': '.$prop->abstraksi;
		$data['jenis'] 			= ': '.$prop->jenis;
		$data['status_prop'] 	= ': '.$prop->stts_acc;
		$data['pers7an'] 		= $pers7an;
		$data['rek_pembb'] 		= $pembb;
		
		
		$data['action'] = site_url('/subsystem_class/proposal/tanggapi_proposal');
		$this->template->display('subsystem_view/proposal/prop detail', $data, $this->tahap);
	}
	
	function tanggapi_proposal()
	{
		$id_prop = $this->input->post('id_prop');
		$nim = $this->input->post('nim');
		
		$this->Proposal_model->penyetujuan_jdl();
		$this->Proposal_model->penyetujuan_prop();
		
		if($this->input->post('pers7an') == 'Disetujui'){
			
			// cek apakah dsn sudah ditentukan ketika pembagian distribusi pembimbing?
			$a = $this->Proposal_model->idta_tanpa_prop($nim);
			
			if($a->num_rows() != 0){
				// jika dosen pembimbing sudah ditentukan sebelumnya oleh prodi
				$id_ta = $a->row()->id_ta;
				$this->Proposal_model->idta_link_to_prop($id_ta, $id_prop);
			}else{
				// jika dosen pembimbing belum ditentukan sebelumnya
				$this->Proposal_model->jadikan_ta($id_prop);
				$rek_p = $this->input->post('rek_pembb');
				$id_ta = $this->sipbta_model->ta('select', '','','','','','',$id_prop)->row()->id_ta;
				if( $rek_p != 'k'){
					$this->Pembimbingan_model->riwayat_pembb('insert', $id_ta, $rek_p);
				}
			}
			
		}
		
		
		redirect('subsystem_class/proposal/show_pengajuan_prop_det/'.$id_prop);
	}
	
	function show_list_jdl_ta(){
		echo "belum ada";
	}
	
	// dsn
	function show_list_request_pembb($offset = 0)
	{
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$req_pembb_paged = $this->Pembimbingan_model->list_req_pembb($limit,$offset,'waktu DESC', '', $this->session->userdata('id_user'))->result();
		
		// generate pagination
		$conf['base_url'] = site_url('user_class/mahasiswa/request_pembb');
		$conf['total_rows'] = $this->Pembimbingan_model->list_req_pembb('','','','','','','','','','count'); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Tanggal Request', 'Nama mahasiswa', 'Status', 'Action');
		$i = 0 + $offset;
		foreach ($req_pembb_paged as $req_pembb)
		{
			$this->table->add_row(++$i, $req_pembb->waktu, $req_pembb->nama, $req_pembb->stts_req, 
				anchor('subsystem_class/proposal/tanggapi_request_form/'.$req_pembb->id_req,'Beri persetujuan',array('class'=>'view')));
		}
		
		// view
		if($conf['total_rows'] != 0){ 					// sudah ada yang merequest
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum belum ada yang melakukan request
			$data['pemberitahuan'] = 'Anda tidak memiliki request sebagai pembimbing';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		
		
		$data['action'] = '';
		$data['calon_pembb'] = '';
		$data['button'] = '';
		
		$data['page_title'] = 'Request pembimbing';
		$this->template->display('subsystem_view/proposal/req pembb form', $data, $this->tahap);
	}
	
	function tanggapi_request_form($id_req)
	{
		$req = $this->Pembimbingan_model->list_req_pembb('','','','','','','','',$id_req)->row();
		
		$options = array(
			'k'					=> '-- Penyetujuan --',
			'Disetujui' 		=> 'Disetujui',
			'Belum Disetujui' 	=> 'Belum Disetujui',
			'Tidak Disetujui' 	=> 'Tidak Disetujui'
		);
		
		$pers7an = form_dropdown('pers7an', $options, 'k');
		
		// view
		$data['action'] = site_url('/subsystem_class/proposal/tanggapi_request_proses');
		$data['id_req'] = form_hidden('id_req', $req->id_req);
		$data['waktu'] = ': '.$req->waktu;
		$data['nim'] = ': '.$req->nim;
		$data['nama'] = ': '.$req->nama;
		$data['stts_req'] = ': '.$req->stts_req;
		$data['pers7an'] = $pers7an;
		
		$data['page_title'] = 'Persetujuan request pembimbing';
		$this->template->display('subsystem_view/proposal/tanggapan_req_form', $data, $this->tahap);
	}
	
	function tanggapi_request_proses()
	{
		echo $id_req = $this->input->post('id_req');
		$this->Pembimbingan_model->penyetujuan_req_pembb($id_req);
		redirect('subsystem_class/proposal/tanggapi_request_form/'.$id_req);
	}
}