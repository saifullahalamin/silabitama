<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembimbingan extends CI_Controller {

	private $tahap;

	function __construct()
	{
		parent::__construct();
		$this->tahap = 'pembimbingan';
		
		// load model
		$this->load->model('subsystem_model/pembimbingan_model','',true);
		$this->load->model('user_model/dsn_model','',true);
		$this->load->model('sipbta_model','',true);
		
		// look and fill table
		$this->table->set_template(array ('row_alt_start' => '<tr class="alt">'));
	}
		
	function index()
	{
		
	}
	
	// mhs
	function show_dist_pembb($offset = 0)
	{
		$limit = 20;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$paged_data = $this->pembimbingan_model->dist_pembb($limit,$offset,'D.nm_dsn ASC, A.nim ASC')->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('subsystem_class/pembimbingan/show_dist_pembb/');
		$conf['total_rows'] = $this->pembimbingan_model->dist_pembb()->num_rows(); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Nama pembimbing', 'Nama mahasiswa', 'NIM');
		$i = 0 + $offset;
		foreach ($paged_data as $pembb)
		{
			$dsn = anchor('user_class/dosen/show_profil_dsn/'.$pembb->id_dsn,$pembb->nm_dsn);
			$mhs = anchor('user_class/mahasiswa/show_profil_mhs/'.$pembb->nim, $pembb->nama);
			$this->table->add_row(++$i, $dsn, $mhs, $pembb->nim);
		}
		
		
		// view
		
		if($conf['total_rows'] != 0){ 				
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											
			$data['pemberitahuan'] = 'Belum ada distribusi pembimbing';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		
		$data['link'] = '';
		
		$data['page_title'] = 'Distribusi pembimbing';
		
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	
	function show_akumulasi_bimbb($id_ta){}
	
	function show_cttn_bimbb(){}
	
	function ubah_jdl_form($hal_asal = '')
	{
		// load data
		$nim = $this->session->userdata('id_user');
		$jdlx = $this->sipbta_model->ta('select', '', '', '', '', $nim,'','','Aktif');
		$jdl = $jdlx->row();
		
		$r = $this->sipbta_model->ta('select', '', '', 'C.wkt DESC', '', $nim);
		$riwayat_jdl = $r->result();
		$count = $r->num_rows(); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('');
		if($count > 0) $this->table->set_heading('No','Judul TA', 'Pembimbing', 'Waktu pengubahan');
		$i=0;
		foreach ($riwayat_jdl as $r_jdl)
		{
			$this->table->add_row(++$i, $r_jdl->jdl, $r_jdl->nm_dsn,$r_jdl->wkt);
		}
		
		if($count > 0) $table = $this->table->generate(); else $table = '';
		
		// view 
		$data['action'] = site_url('/subsystem_class/pembimbingan/ubah_jdl_proses');
		$data['id_jdl'] = form_hidden('id_ta',$jdl->id_ta);
		$data['jdl_skrg'] = $jdl->jdl;
		if($hal_asal == 1){
			$kembali = 'mendaftar_sem';
		}elseif($hal_asal == 2){
			$kembali = 'mendaftar_pendd';
		}else $kembali = '../../../';
		$data['kembali'] = '<a href= "' . site_url('user_class/mahasiswa/'.$kembali) . '" data-role="button" data-inline="true">Kembali</a>';
		$data['table'] = $table;
		
		$data['page_title'] = 'Form pengubahan judul'; 
		$this->template->display('subsystem_view/pembimbingan/ubah_jdl', $data, $this->tahap);
	}
	function ubah_jdl_proses()
	{
		$id_ta = $this->input->post('id_ta');
		$jdl_baru = $this->input->post('jdl_baru');
		
		$this->sipbta_model->ubah_jdl($id_ta, $jdl_baru);
		redirect('user_class/mahasiswa/ubah_jdl');
	}
	
	// kaprodi
	function dist_pembb_form()
	{
		$prodi = $this->dsn_model->is_kaprodi($this->session->userdata('id_user'))->row()->id_prodi;
		
		$id_dsn = $this->input->post('rek_pembb');
		if($id_dsn == '') $id_dsn = 'k';
		
		//load data ==========
		$undist = $this->pembimbingan_model->un_dist_pembb($prodi);
		$dist = $this->pembimbingan_model->dist_pembb('','','','','','','',$id_dsn);
		
		$mhs = $undist->result();
		$mhs_1 = $dist->result();
		
		if($undist->num_rows() > 0){
			$this->table->set_template(
				array ( 
					'table_open'          => '<table id="list_mhs" border="0" cellpadding="4" cellspacing="0">',
					'row_alt_start' => '<tr class="alt">'
				)
			);
			
			// generate table mhs_un_dist =============
			$this->table->set_empty("&nbsp;");
			foreach ($mhs as $m)
			{
				$dd = array(
					    'name'        => 'mhs[]',
					    'value'       => $m->nim
					    );
				$this->table->add_row(form_checkbox($dd),$m->nim, $m->nama);
			}
			$list_mhs = $this->table->generate();
		} else $list_mhs = "<table id=\"list_mhs\"><tr><td>Semua mahasiswa prodi $prodi sudah mendapat dosen pembimbing</td></tr></table>";
		
		// clearing table ===========
		$this->table->clear();
		if($dist->num_rows() > 0){
			$this->table->set_template(
				array ( 
					'table_open'          => '<table id="mhs_choosen" border="0" cellpadding="4" cellspacing="0">',
					'row_alt_start' => '<tr class="alt">'
				)
			);
			// generate table mhs_dist ============
			$this->table->set_empty("&nbsp;");
			foreach ($mhs_1 as $m)
			{
				$this->table->add_row('',$m->nim, $m->nama);
			}
			$mhs_dist = $this->table->generate();
		} else $mhs_dist = "<table id=\"mhs_choosen\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\"></table>";
		
		// view ===========
		$def = $this->input->post('rek_pembb');
		$data['action1'] = site_url("subsystem_class/pembimbingan/dist_pembb_form");
		$data['action2'] = site_url("subsystem_class/pembimbingan/dist_pembb_proses");
		$data['dsn_opt12'] = $this->dsn_model->dsn_option($def);
		$data['id_dsn'] = form_hidden('id_dsn', $def);
		$data['list_mhs'] = $list_mhs;
		$data['mhs_choosen'] = $mhs_dist;
		
		$data['page_title'] = 'Penentuan distribusi pembimbing'; 
		$this->template->display('subsystem_view/pembimbingan/dist_pembb_form', $data, $this->tahap);
	}
	function coba_model($nim){
		$this->pembimbingan_model->tentukan_pembb(0,$nim);
	}
	function dist_pembb_proses()
	{
		$id_dsn = $this->input->post('id_dsn');
		$nims 	= $this->input->post('mhs');
		
		foreach($nims as $nim){
			$this->pembimbingan_model->tentukan_pembb($id_dsn,$nim);
		}
		redirect('user_class/kaprodi/tentukan_dist_pembb');
		//print_r($this->input->post());
		//echo "<pre>";
		//print_r($_POST);
	}
	
	// opsdik
	function umumkan_dist_pembb_proses(){}
	
	// pembbimbing
	function catat_progres_bimbb_form($id_bimbb)
	{
		// load data
		$riwayat_bimbb = $this->pembimbingan_model->riwayat_bimbb('select',$id_bimbb)->result();
		$count = $this->pembimbingan_model->riwayat_bimbb('select',$id_bimbb,'','','count'); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('');
		if($count > 0) $this->table->set_heading('Bimbingan ke', 'Waktu', 'Bab', 'Materi bimbingan');
		$i=0;
		foreach ($riwayat_bimbb as $bimbb)
		{
			$this->table->add_row(++$i, $bimbb->wkt_bimbb, $bimbb->bab,$bimbb->materi_bimbb);
		}
		
		// view
		$dist_pembb = $this->pembimbingan_model->dist_pembb('','','',$id_bimbb)->row();
		
		$data['action'] = site_url('/subsystem_class/pembimbingan/catat_progres_bimbb_proses');
		$data['id_bimbb'] =  form_hidden('id_bimbb', $id_bimbb);
		$data['nim'] = ': '.$dist_pembb->nim;
		$data['nama'] = ': '.$dist_pembb->nama;
		$data['prodi'] = ': '.$dist_pembb->nm_prodi;
		$data['button'] = '<button type="submit" name="submit" data-inline="true">Submit</button>';
		$data['button'] .= anchor('user_class/pembimbing/lihat_list_mhs_bimbb" data-role="button" data-inline="true','Batal');
		$data['table'] = $this->table->generate();
		
		$data['page_title'] = 'Pembimbingan';
		$this->template->display('subsystem_view/pembimbingan/catat_progres_bimbb_form', $data, $this->tahap);
	}
	
	function catat_progres_bimbb_proses()
	{
		$id_bimbb = $this->input->post('id_bimbb');
		$bab = $this->input->post('bab');
		$materi = $this->input->post('materi');
		$this->pembimbingan_model->riwayat_bimbb('insert', $id_bimbb, $bab, $materi);
		redirect('subsystem_class/pembimbingan/catat_progres_bimbb_form/'.$id_bimbb);
	}
	
}