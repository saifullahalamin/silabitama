<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendadaran extends CI_Controller {

	private $tahap;

	function __construct()
	{
		parent::__construct();
		$this->tahap = 'pendadaran';
		
		$this->load->model('user_model/dsn_model','',true);
		
		$this->load->model('sipbta_model','',true);
		$this->load->model('subsystem_model/pendadaran_model','',true);
	}
	
	function set_tahap()
	{
		$this->session->set_flashdata('tahapan', 'pendadaran');
	}
		
	function index()
	{
		
	}
	
	// mhs
	function show_prasyarat_pendd(){}
	
	function pendf_pendd_form()
	{		
		$nim = $this->session->userdata('id_user');
		
		$ta = $this->sipbta_model->ta('select','','','','', $nim, '','', 'Aktif','Aktif')->row();
		
		$data['nim'] 			= ': '.$ta->nim;
		$data['nama'] 			= ': '.$ta->nama;
		$data['jdl'] 			= ': '.$ta->jdl.' (ingin merubah judul? klik '.anchor('user_class/mahasiswa/ubah_jdl/2','di sini').').';
		$data['jdl'] 			.= form_hidden('id_ta', $ta->id_ta);
		$data['pembb'] 			= ': '.$ta->nm_dsn;
		$data['tgl_sem'] 		= '';
		$data['pukul'] 			= '';
		$data['ket'] 			= 'Dengan ini saya mendaftar pendadaran TA sesuai ketentuan yang berlaku.
									<br />(ketentuan dan syarat mendaftar seminar TA lik <a href="google.com">disini</a>).<br />';
		$data['button'] 		= '<button type="submit" name="jc1" data-inline="true">Daftar</button>';
		$data['button'] 		.= anchor('user_class/mahasiswa/mendaftar_pendd" data-role="button" data-inline="true','Batal');
		
		$data['action'] 		= site_url('/subsystem_class/pendadaran/pendf_pendd_proses');
		$data['page_title'] 	= 'Form pendaftaran pendadaran';
		$this->template->display('subsystem_view/seminar/pendf_seminar_form', $data, $this->tahap);
	}
	
	function pendf_pendd_proses()
	{
		$id_ta = $this->input->post('id_ta');
		$this->pendadaran_model->daftar_pendd($id_ta);
		redirect('user_class/mahasiswa/mendaftar_pendd');
	}
	
	function pendd_dets($id_pendd)
	{
		$det_pendd = $this->pendadaran_model->list_pendf_pendd('no_gb','','','', '', $id_pendd);
		$pendd = $det_pendd->row();
		$ta = $this->sipbta_model->ta('select', '','','',$pendd->id_ta, '','','','Aktif','Aktif')->row();
		$i = 0;
		
		foreach ($det_pendd->result() as $penguji){
			if($ta->id_dsn != $penguji->dsn_penguji){
		   		$pn[$i]['penguji']			= $penguji->nm_dsn;
		   		$pn[$i]['tt_bhs']			= $penguji->tt_bhs;
		   		$pn[$i]['tt_fp']			= $penguji->tt_fp;
		   		$pn[$i]['cp_tertulis']		= $penguji->cp_tertulis;
		   		$pn[$i]['cp_lisan']			= $penguji->cp_lisan;
		   		$pn[$i]['b_permasalahan']	= $penguji->b_permasalahan;
		   		$pn[$i]['pm_dp']			= $penguji->pm_dp;
		   		$pn[$i]['pm_prog']			= $penguji->pm_prog;
		   		$pn[$i]['pm_uf']			= $penguji->pm_uf;
		   		$i++;
		   }else{
		   		$pn[2]['penguji']			= $penguji->nm_dsn;
		   		$pn[2]['tt_bhs']			= $penguji->tt_bhs;
		   		$pn[2]['tt_fp']				= $penguji->tt_fp;
		   		$pn[2]['cp_tertulis']		= $penguji->cp_tertulis;
		   		$pn[2]['cp_lisan']			= $penguji->cp_lisan;
		   		$pn[2]['b_permasalahan']	= $penguji->b_permasalahan;
		   		$pn[2]['pm_dp']				= $penguji->pm_dp;
		   		$pn[2]['pm_prog']			= $penguji->pm_prog;
		   		$pn[2]['pm_uf']				= $penguji->pm_uf;
		   }
		}
		
		// view
		$data['action'] 			= '#';
		$data['idid'] 				= $pendd->id_ta;
		$data['nim'] 				= ': '.$pendd->nim.form_hidden('id_pendd', $id_pendd);
		$data['nama'] 				= ': '.$pendd->nama;
		$data['jdl'] 				= ': '.$ta->jdl;
		$data['pembb'] 				= ': '.$ta->nm_dsn.form_hidden('pembb_ta', $ta->id_dsn);
		$data['tgl_daft'] 			= ': '.$pendd->tgl_pendf;
		$data['penguji_1'] 			= ': -';
		$data['penguji_2'] 			= ': -';
		$data['nilai_peng'] 		= $pn;
		$data['stts_pendd']			= ': '.$pendd->stts_pendd;
		$data['tgl_pendd'] 			= ': -';
		$data['pukul'] 				= ': -';
		$data['button'] 			= anchor('user_class/mahasiswa/mendaftar_pendd" data-role="button" data-inline="true','Kembali');
		
		if(isset($pn[0]['penguji'])) $data['penguji_1'] = ': '.$pn[0]['penguji'];
		if(isset($pn[1]['penguji'])) $data['penguji_2']	= ': '.$pn[1]['penguji']; 
		$tgl_wkt = explode(" ", $pendd->tgl_pendd);
		if(isset($tgl_wkt[1])) $data['tgl_pendd'] 		= ': '.$tgl_wkt[0];
		if(isset($tgl_wkt[1])) $data['pukul'] 			= ': '.$tgl_wkt[1];
		
		return $data;
	}
	
	function pendd_det($id_pendd)
	{
		$data = $this->pendd_dets($id_pendd);
		
		$data['page_title'] = 'Detail pendadaran';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
		
	}
	
	
	
	function show_jdwl_pendd(){}
	
	function show_jdwl_pendd_all()
	{
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$pendd_paged = $this->pendadaran_model->list_pendf_pendd('',$limit,$offset,'tgl_pendd DESC')->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] 		= site_url('user_class/kaprodi/lihat_jdwl_sem/');
		$conf['total_rows'] 	= $this->pendadaran_model->list_pendf_pendd()->num_rows(); // count all result
		$conf['per_page'] 		= $limit; 
		$conf['uri_segment'] 	= $uri_segment;
		$conf['num_links'] 		= 3;
		$conf['first_link'] 	= 'Awal';
		$conf['last_link'] 		= 'Akhir';
		$this->pagination->initialize($conf); 
				
		// generate list data
		$data['jadwal'] = '';
		foreach ($pendd_paged as $jdwl)
		{
			$jdl = $this->sipbta_model->jdl_ta($jdwl->id_ta, 'Aktif')->row();
			
			$det_pendd = $this->pendadaran_model->list_pendf_pendd('no_ob','','','', '', $jdwl->id_pendd);
			$pendd = $det_pendd->row();
			$ta = $this->sipbta_model->ta('select', '','','',$pendd->id_ta, '','','','Aktif','Aktif')->row();
			$pd = array();
			
			foreach ($det_pendd->result() as $penguji){
			   if($ta->id_dsn != $penguji->dsn_penguji){
			   		$pd[] = $penguji->nm_dsn;
			   		if(is_null($penguji->nm_dsn)) $pd[] = '';
			   }	
			}
			
			$data['jadwal'] .= "<div id=\"jdwl_item\">
									<div id=\"".str_replace(" ","","$jdwl->stts_pendd")."\" class=\"title\">
										<div id=\"$jdwl->id_prodi\" class=\"prodi\"></div>
										<div class=\"waktu\">
											$jdwl->stts_pendd <br />
											$jdwl->tgl_pendd
										</div>
									</div>
									<div id=\"item_det\" class=\"det\">
										Nama : <strong>$jdwl->nama</strong> <br />
										Dosen pembimbing TA : $ta->nm_dsn <br />
										Dosen penguji 1 : ".@$pd[0]." <br />
										Dosen penguji 2 : ".@$pd[1]." <br />
										Judul TA : $jdl->jdl
									</div>
								</div>";
		}
		
		// view
		$data['id_jdwl'] = 'pendadaran';
		$data['page_title'] = 'Jadwal pendadaran';
		
		$this->template->display('subsystem_view/seminar/jadwal_sem', $data, $this->tahap);
	}
	
	function show_revisi(){}
	
	// kaprodi
	function show_list_pendf_pendd(){}
	
	function penentuan_penguji_form(){}
	
	function penentuan_penguji_proses(){}
	
	// opsdik
	function prasyarat_pendd_form(){}
	
	function prasyarat_pendd_proses(){}
	
	function list_pendf_pendd()
	{
		// load data
		$list_pendf = $this->pendadaran_model->list_pendf_pendd();
		$list_pendf_pendd = $list_pendf->result();
		$count = $list_pendf->num_rows(); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('');
		if($count > 0) $this->table->set_heading('No', 'Nim', 'Nama','Tanggal daftar', 'Status', 'Tindakan');
		$i=0;
		foreach ($list_pendf_pendd as $pendf)
		{
			$def_link = anchor('subsystem_class/pendadaran/tanggapi_pendf_pendd_form/'.$pendf->id_pendd,'Tentukan jadwal',array('class'=>'view'));
			if($pendf->stts_pendd == 'Sudah terjadwal') {
				$link = str_replace("Tentukan jadwal","Lihat",$def_link);
				$link .= ' / '.anchor('subsystem_class/pendadaran/tanggapi_pendf_pendd_form/'.$pendf->id_pendd.'/edit','Ubah',array('class'=>'view'));
			}
			elseif ($pendf->stts_pendd == 'Belum terjadwal') $link = $def_link;
			else $link = str_replace("Tentukan jadwal","Lihat",$def_link);
			if($this->session->userdata('user') == 'kaprodi') 
			$link .= ' / '.anchor('subsystem_class/pendadaran/tanggapi_pendf_pendd_form/'.$pendf->id_pendd.'/tentukan_penguji', 'Tentukan Penguji', array('class'=>'view'));
			$this->table->add_row(++$i, $pendf->nim,$pendf->nama, $pendf->tgl_pendf,$pendf->stts_pendd, $link);
		}
		
		if($count > 0) $list = $this->table->generate(); else $list = 'Belum ada mahasiswa yang mendaftar pendadaran.';
		
		$data['pagination'] = '';
		$data['table'] = $list;
		$data['page_title'] = 'List pendaftar pendadaran';
		$this->template->display('subsystem_view/seminar/list_pendf_sem', $data, $this->tahap);
	}
	
	function tanggapi_pendf_pendd_form($id_pendd, $opt = '')
	{
		$user = $this->session->userdata('user');
		
		// load data
		$det_pendd = $this->pendadaran_model->list_pendf_pendd('no_gb','','','', '', $id_pendd);
		$pendd = $det_pendd->row();
		$ta = $this->sipbta_model->ta('select', '','','',$pendd->id_ta, '','','','Aktif','Aktif')->row();
		
		foreach ($det_pendd->result() as $penguji){
		   if($ta->id_dsn != $penguji->dsn_penguji){
		   		$pd[] = $penguji->nm_dsn;
				$pd_id[] = $penguji->dsn_penguji;
		   }	
		}
		// view
		$data['action'] 			= site_url('/subsystem_class/pendadaran/tanggapi_pendf_pendd_proses');
		$data['nim'] 				= ': '.$pendd->nim.form_hidden('id_pendd', $id_pendd);
		$data['nama'] 				= ': '.$pendd->nama;
		$data['jdl']				= ': '.$ta->jdl;
		$data['pembb'] 				= ': '.$ta->nm_dsn.form_hidden('pembb_ta', $ta->id_dsn);
		$data['tgl_daft'] 			= ': '.$pendd->tgl_pendf;
		$data['penguji_1'] 			= ': -';
		$data['penguji_2'] 			= ': -';
		$data['stts_pendd'] 		= ': '.$pendd->stts_pendd;
		$data['tgl_pendd'] 			= ': -';
		$data['pukul'] 				= ': -';
		$data['button'] 			= '<button type="submit" name="jc1" data-inline="true">Tentukan</button>';
		$data['button'] 			.= anchor('user_class/opsdik/lihat_list_pendf_pendd" data-role="button" data-inline="true','Kembali');
			
		if($pendd->stts_pendd == 'Sudah dilaksanakan' || $pendd->stts_pendd == 'Sudah terjadwal' || $pendd->stts_pendd == 'Belum terjadwal'){
			
			if(isset($pd[0])) $data['penguji_1'] 			= ': '.$pd[0];
			if(isset($pd[1])) $data['penguji_2'] 			= ': '.$pd[1]; 
			$tgl_wkt = explode(" ", $pendd->tgl_pendd);
			if(isset($tgl_wkt[1])) $data['tgl_pendd'] 		= ': '.$tgl_wkt[0];
			if(isset($tgl_wkt[1])) $data['pukul'] 			= ': '.$tgl_wkt[1];
			$data['button'] = anchor('user_class/opsdik/lihat_list_pendf_pendd" data-role="button" data-inline="true','Kembali');
			
			if($user == 'kaprodi' && $pendd->stts_pendd == 'Belum terjadwal'){
				$data['penguji_1'] = $this->dsn_model->dsn_option('', 'penguji_1');
				$data['penguji_2'] = $this->dsn_model->dsn_option('', 'penguji_2');
			}elseif($user == 'opsdik' && $pendd->stts_pendd == 'Belum terjadwal'){
				$data['tgl_pendd'] = '<input name="tgl_pendd" type="date" />';
				$data['pukul'] = '<input name="pukul" type="time" />';
			}
			if($pendd->stts_pendd == 'Belum terjadwal' || $opt == 'edit' || $opt == 'tentukan_penguji'){
				$data['button'] = '<button type="submit" name="jc1" data-inline="true">Tentukan</button>';
				$data['button'] .= anchor('user_class/opsdik/lihat_list_pendf_pendd" data-role="button" data-inline="true','Kembali');
			}
			
			
			if(($opt == 'tentukan_penguji' || $opt == 'edit') && $user == 'kaprodi' && $pendd->stts_pendd != 'Sudah dilaksanakan'){
				$data['penguji_1'] = $this->dsn_model->dsn_option('', 'penguji_1');
				$data['penguji_2'] = $this->dsn_model->dsn_option('', 'penguji_2');
			}elseif($opt == 'edit' && $user == 'opsdik' && $pendd->stts_pendd != 'Sudah dilaksanakan'){
				$data['tgl_pendd'] = '<input name="tgl_pendd" type="date" />';
				$data['pukul'] = '<input name="pukul" type="time" />';
			}
			if($opt == 'edit' || $opt = 'tentukan_penguji') {
				if(isset($pd_id[0])) $data['penguji_1'] .= form_hidden('p1_asal', $pd_id[0]);
				if(isset($pd_id[1])) $data['penguji_2'] .= form_hidden('p2_asal', $pd_id[1]);
				$data['action'] = site_url('/subsystem_class/pendadaran/tanggapi_pendf_pendd_proses/'.$opt);	
			}
			
		}
		
		$data['page_title'] = 'Penentuan pendadaran';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
	}
	
	function tanggapi_pendf_pendd_proses($edit = '')
	{
		$user = $this->input->post('user');
		$id_pendd = $this->input->post('id_pendd');
		
		if($user == 'kaprodi'){ 				// kaprodi
			$p1_asal 	= $this->input->post('p1_asal');
			$p2_asal 	= $this->input->post('p2_asal');
			$penguji_1 	= $this->input->post('penguji_1');
			$penguji_2 	= $this->input->post('penguji_2');
			$this->pendadaran_model->tentukan_pendd($user, $id_pendd, $penguji_1, $penguji_2, $edit, $p1_asal, $p2_asal);
		}
		if($user == 'opsdik'){									// opsdik
			$pembb_ta 	= $this->input->post('pembb_ta');
			
			$tgl 		= $this->input->post('tgl_pendd');
			$jam 		= $this->input->post('pukul');
			$waktu 		= str_replace("/","-",$tgl).' '.$jam;
			
			$this->pendadaran_model->tentukan_pendd($user, $id_pendd, $pembb_ta, $waktu, $edit);	
		}
		
		redirect('subsystem_class/pendadaran/tanggapi_pendf_pendd_form/'.$id_pendd);
	}
	
	function cetak_berita_acara(){}
	
	function verifikasi_pendd_form($id_pendd)
	{
		// ambil detail pendadaran
		$data = $this->pendd_dets($id_pendd);
		
		// gabungkan view catatan revisi dengan view detail pendadaran.
		$data['pukul'] 		.= $this->load->view('subsystem_view/pendadaran/ver_pendd', '', true);
		$data['stts_pendd'] .= form_hidden('id_pendd', $id_pendd);
		$data['button']		 = '<button type="submit" data-inline="true">Simpan</button>';
		$data['button'] 	.= anchor('user_class/mahasiswa/mendaftar_pendd" data-role="button" data-inline="true','Kembali');
		$data['action']		 = site_url('/subsystem_class/pendadaran/verifikasi_pendd_proses');
		
		
		// tampilkan penggabungan
		$data['page_title']  = 'Revisis dan catatan pendadaran';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
	}
	
	function verifikasi_pendd_proses()
	{
		$id_pendd 	= $this->input->post('id_pendd');
		$cttn_pendd = $this->input->post('cttn_pendd');
		$cttn_rev 	= $this->input->post('cttn_rev');
		$bts_wkt 	= $this->input->post('bts_wkt');
	}
	
	// dsn
	function show_keputusan_sbg_penguji(){}
	
	// pembb
	function show_list_nilai(){}
	
	function penilaian_terahir_form(){}
	
	function penilaian_terahir_proses(){}
	
	function revisi_form($id_pendd, $opt = '')
	{
		// ambil detail pendadaran
		$data = $this->pendd_dets($id_pendd);
		
		// gabungkan view catatan revisi dengan view detail pendadaran.
		$da['tt_bhs'] 			= '<input type="text" name="tt_bhs" value="" placeholder="Bahasa penulisan">';
		$da['tt_fp'] 			= '<input type="text" name="tt_fp" value="" placeholder="Format penulisan">';
		$da['cp_tertulis'] 		= '<input type="text" name="cp_tertulis" value="" placeholder="Laporan tertulis">';
		$da['cp_lisan'] 		= '<input type="text" name="cp_lisan" value="" placeholder="Laporan lisan / Presentasi">';
		$da['b_permasalahan'] 	= '<input type="text" name="b_permasalahan" value="" placeholder="Bobot permasalahan">';
		$da['pm_dp'] 			= '<input type="text" name="pm_dp" value="" placeholder="Desain proses">';
		$da['pm_prog'] 			= '<input type="text" name="pm_prog" value="" placeholder="Programming">';
		$da['pm_uf'] 			= '<input type="text" name="pm_uf" value="" placeholder="User friendly">';
		
		$da['stts_rev'] 		= '<select name="stts_rev" data-native-menu="false">
									    <option>-- Revisi / Tidak -- </option>
									    <option value="Belum merevisi">Revisi</option>
									    <option value="Tidak ada revisi">Tidak Revisi</option>
									</select>';
		
		$da['cttn_pendd'] 		= ' <textarea cols="40" rows="8" name="cttn_pendd" placeholder="';
		$da['cttn_pendd'] 		.= 'Catatan ini hanya bisa dilihat oleh dosen penguji sebagai pengingat revisi TA mahasiswa"></textarea>';
		$da['cttn_pendd'] 		.= form_hidden('dsn_penguji', $this->session->userdata('id_user'));
		$da['cttn_rev'] 		= '<textarea cols="40" rows="8" name="cttn_rev" placeholder="Revisi TA mahasiswa"></textarea>';
		$da['bts_wkt'] 			= '<input type="date" data-clear-btn="false" name="bts_wkt" value="">';
		$da['opt'] 				= $opt;
		
		if($opt == 'det'){
			$id_dsn = $this->session->userdata('id_user');
			$detdet = $this->pendadaran_model->detail_pendd($id_pendd, $id_dsn)->row();
			$tn = $this->total_nilai($detdet);
			
			$da['tt_bhs'] 			= ': '.$detdet->tt_bhs;
			$da['tt_fp'] 			= ': '.$detdet->tt_fp;
			$da['cp_tertulis'] 		= ': '.$detdet->cp_tertulis;
			$da['cp_lisan'] 		= ': '.$detdet->cp_lisan;
			$da['b_permasalahan'] 	= ': '.$detdet->b_permasalahan;
			$da['pm_dp'] 			= ': '.$detdet->pm_dp;
			$da['pm_prog'] 			= ': '.$detdet->pm_prog;
			$da['pm_uf'] 			= ': '.$detdet->pm_uf;
			$da['nilai_angka']		= $tn;
			$da['stts_rev'] 		= ': '.$detdet->stts_rev;
			$da['cttn_pendd'] 		= ': '.$detdet->cttn_pendd;
			$da['cttn_rev']			= ': '.$detdet->cttn_rev;
			$da['bts_wkt'] 			= ': '.$detdet->bts_wkt;
		}
		
		$data['pukul'] .= $this->load->view('subsystem_view/pendadaran/revisi_dan_catatan_pendd', $da, true);
		$data['button'] = '<button type="submit" data-inline="true">Simpan</button>';
		$data['button'] .= anchor('user_class/penguji/beri_revisi" data-role="button" data-inline="true','Kembali');
		$data['action'] = site_url('/subsystem_class/pendadaran/revisi_proses');
		
		// tampilkan penggabungan
		$data['page_title'] = 'Revisi dan catatan pendadaran';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
	}
	
	function revisi_proses()
	{
		$id_pendd 				= $this->input->post('id_pendd');
		$dsn_penguji 			= $this->input->post('dsn_penguji');
		$stts_rev 				= $this->input->post('stts_rev');
		$n['tt_bhs'] 			= $this->input->post('tt_bhs');
		$n['tt_fp'] 			= $this->input->post('tt_fp');
		$n['cp_tertulis'] 		= $this->input->post('cp_tertulis');
		$n['cp_lisan'] 			= $this->input->post('cp_lisan');
		$n['b_permasalahan'] 	= $this->input->post('b_permasalahan');
		$n['pm_dp'] 			= $this->input->post('pm_dp');
		$n['pm_prog'] 			= $this->input->post('pm_prog');
		$n['pm_uf'] 			= $this->input->post('pm_uf');
		$cttn_pendd 			= $this->input->post('cttn_pendd');
		$cttn_rev 				= $this->input->post('cttn_rev');
		$bts_wkt 				= $this->input->post('bts_wkt');
		
		if($stts_rev == 'Tidak ada revisi') {
			$cttn_pendd = $stts_rev;
			$cttn_rev 	= $stts_rev;
		}
		
		$this->pendadaran_model->beri_revisi($id_pendd, $dsn_penguji,$stts_rev, $n, $cttn_pendd, $cttn_rev, $bts_wkt);
		redirect('subsystem_class/pendadaran/revisi_form/'.$id_pendd.'/det');
	}
	
	function total_nilai($nilai)
	{
		$tt = (( (int)$nilai->tt_bhs + (int)$nilai->tt_fp)/2) * (10/100);						// 10%
		$cp = (((int)$nilai->cp_tertulis + (int)$nilai->cp_lisan)/2) * (10/100);				// 10%
		$bp = (int)$nilai->b_permasalahan * (20/100);											// 20%
		$pm = (((int)$nilai->pm_dp + (int)$nilai->pm_prog + (int)$nilai->pm_uf)/3) * (60/100);	// 60%
		
		return $n = $tt + $cp + $bp + $pm;
	}
	
	function total_nilai_p($nilai)
	{
		$tt = (( (int)$nilai['tt_bhs'] + (int)$nilai['tt_fp'])/2) * (10/100);							// 10%
		$cp = (((int)$nilai['cp_tertulis'] + (int)$nilai['cp_lisan'])/2) * (10/100);					// 10%
		$bp = (int)$nilai['b_permasalahan'] * (20/100);													// 20%
		$pm = (((int)$nilai['pm_dp'] + (int)$nilai['pm_prog'] + (int)$nilai['pm_uf'])/3) * (60/100);	// 60%
		
		return $n = $tt + $cp + $bp + $pm;
	}
	
	function verifikasi_revisi_form($id_pendd)
	{
		// ambil detail pendadaran
		$data = $this->pendd_dets($id_pendd);
		$id_dsn = $this->session->userdata('id_user');
		$detdet = $this->pendadaran_model->detail_pendd($id_pendd, $id_dsn)->row();
		$da['stts_rev'] 	= ': '.$detdet->stts_rev;
		$da['nilai'] 		= ': '.$detdet->nilai;
		if($detdet->stts_rev == 'Belum merevisi') $da['nilai'] .= ' (Nilai sementara)';
		$da['nilai'] 		.= '</div>
								<div data-role="fieldcontain">
								    <label for="nilai2">Nilai setelah revisi</label> 
								    <select name="nilai2" data-native-menu="false">
									    <option>-- Nilai setelah revisi -- </option>
									    <option value="A">A</option>
									    <option value="A-">A-</option>
									    <option value="B+">B+</option>
									    <option value="B">B</option>
									    <option value="B-">B-</option>
									    <option value="C+">C+</option>
									    <option value="C">C</option>
									    <option value="C-">C-</option>
									    <option value="D">D</option>
									</select>'; 
		$da['cttn_pendd'] 	= ': '.$detdet->cttn_pendd;
		$da['cttn_rev'] 	= ': '.$detdet->cttn_rev;
		$da['bts_wkt'] 		= ': '.$detdet->bts_wkt.form_hidden('penguji', $this->session->userdata('id_user'));
		// gabungkan view catatan revisi dengan view detail pendadaran.
		$data['pukul'] 		.= $this->load->view('subsystem_view/pendadaran/revisi_dan_catatan_pendd', $da, true);
		$data['button'] 	= '<button type="submit" data-inline="true">Simpan</button>';
		$data['button'] 	.= anchor('user_class/penguji/beri_revisi" data-role="button" data-inline="true','Kembali');
		$data['action'] 	= site_url('/subsystem_class/pendadaran/verifikasi_revisi_proses');
		
		
		// tampilkan penggabungan
		$data['page_title'] = 'Verifikasi revisi';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
	}
	
	function verifikasi_revisi_proses()
	{
		$id_pendd 	= $this->input->post('id_pendd');
		$penguji 	= $this->input->post('penguji');
		$nilai2 	= $this->input->post('nilai2');
		
		$this->pendadaran_model->verifikasi_revisi($id_pendd, $penguji, $nilai2);
		redirect('subsystem_class/pendadaran/revisi_form/'.$id_pendd.'/det');
	}
	
	function angka2huruf($a)
	{
		if($a >= 85 && $a < 100)		$n = 'A';
		elseif($a >= 75 && $a < 85) 	$n = 'A-';
		elseif($a >= 65 && $a < 75) 	$n = 'B+';
		elseif($a >= 55 && $a < 65) 	$n = 'B';
		elseif($a >= 45 && $a < 55) 	$n = 'B-';
		elseif($a >= 35 && $a < 45) 	$n = 'C+';
		elseif($a >= 25 && $a < 35) 	$n = 'C';
		elseif($a >= 0 && $a < 25) 	$n = 'D';
		
		return $n;
	}
	
	function beri_nilai($id_pendd, $det = '')
	{
		// ambil detail pendadaran
		$data = $this->pendd_dets($id_pendd);
		$pn = $data['nilai_peng'];
		
		$data['pembb'] 		.= ' | '.$n1 = $this->total_nilai_p($pn[2]);
		$data['penguji_1'] 	.= ' | '.$n2 = $this->total_nilai_p($pn[0]);
		$data['penguji_2'] 	.= ' | '.$n3 = $this->total_nilai_p($pn[1]);
		
		if($det == ''){
			
			$data['pukul'] 	.= '</div>
									<div data-role="fieldcontain">
									    <label for="rata2">Rata - rata</label> :
									    '.$n_t = ($n1 + $n2 + $n3)/3;
			$data['pukul'] 	.= '</div>
									<div data-role="fieldcontain">
									    <label for="rek_n">Saran nilai TA</label> :
									    '.$n_t = $this->angka2huruf($n_t);
			$data['pukul'] 	.= form_hidden('id_ta', $data['idid']);
			$data['pukul'] 	.= '</div>
								<div data-role="fieldcontain">
								    <label for="nilai2">Nilai akhir</label> 
								    <select name="nilai2" data-native-menu="false">
									    <option>-- Nilai akhir -- </option>
									    <option value="A">A</option>
									    <option value="A-">A-</option>
									    <option value="B+">B+</option>
									    <option value="B">B</option>
									    <option value="B-">B-</option>
									    <option value="C+">C+</option>
									    <option value="C">C</option>
									    <option value="C-">C-</option>
									    <option value="D">D</option>
									</select>';
			$data['button'] = '<button type="submit" data-inline="true">Tentukan nilai</button>';
			$data['button'] .= anchor('user_class/penguji/beri_revisi" data-role="button" data-inline="true','Kembali');
			$data['action'] = site_url('/subsystem_class/pendadaran/beri_nilai_proses');	
		}else{
			$data['pukul'] .= '</div> 
								<div data-role="fieldcontain">
								<label for="nilai2">Nilai akhir</label> : '.$this->pendadaran_model->nilai_ta_saja($data['idid'])->row()->nilai;
			$data['button'] = anchor('user_class/penguji/beri_revisi" data-role="button" data-inline="true','Kembali');
			$data['action'] = '#';
		}
		
		// tampilkan penggabungan
		$data['page_title'] = 'Beri penilaian terahir';
		$this->template->display('subsystem_view/pendadaran/pel_pendd', $data, $this->tahap);
	}
	
	function beri_nilai_proses()
	{
		$id_pendd = $this->input->post('id_pendd');
		$id_ta = $this->input->post('id_ta');
		$nilai = $this->input->post('nilai2');
		
		$this->pendadaran_model->tentukan_nilai_ta($id_pendd, $id_ta, $nilai);
		redirect('subsystem_class/pendadaran/beri_nilai/'.$id_pendd.'/det');
	}
	
	// penguji
}