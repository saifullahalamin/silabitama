<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__)."/auth.php");

class Sipbta extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('sipbta_model', '', TRUE);
		$this->load->model('user_model/mhs_model', '', TRUE);
		$this->load->model('user_model/dsn_model', '', TRUE);
		$this->load->model('user_model/opsdik_model', '', TRUE);
		
		$this->load->library('ofc');
		
		// load auth
		$this->auth = new Auth();
		
		// look and fill table
		$this->table->set_template(array ('row_alt_start' => '<tr class="alt">'));
	}
		
	function index(){
		$data['page_title'] = 'Home';
		$this->template->display('home', $data);
	}
	
	function get_user_det($uname, $pass)
	{
		$admin = $this->sipbta_model->login3($uname, $pass); // 
		if($admin->num_rows() > 0){
			$det_user[0] = true;
			$det_user[1] = "admin"; 					// id
			$det_user[2] = "Administrator"; 			// nama
			$det_user[3] = "admin"; 					// level
		}else{
			$user = $this->sipbta_model->login($uname, $pass);
			if($user->num_rows() > 0){
				$det_user[0] = true; 					// status login
				$det_user[1] = $user->row()->id; 		// id
				$det_user[2] = $user->row()->nama; 		// nama
				$det_user[3] = $user->row()->level; 	// level
			} else {
				$user = $this->sipbta_model->login2($uname, $pass);
				$det_user[0] = true; 					// status login
				$det_user[1] = $user[0]; 				// id
				$det_user[2] = $user[1]; 				// nama
				$det_user[3] = $user[2]; 				// level
			}
			
			if($user == "Username yang anda masukkan belum terdaftar" 
			|| $user == "Maaf, password anda salah") {
				$det_user = null;
				$det_user[0] = false;
				$det_user[1] = $user;
			}
		}
		
		//print_r($admin);
		return $det_user; 	
		//http://localhost/project1/index.php/sipbta/get_user_det/132312090/abc
	}
	
	function sign_in()
	{
		$data['page_title'] = 'Sign In';
		$this->load->view('login_form', $data);
	}
	
	function sign_in_proses()
	{
		$uname = $this->input->post('username');
		$pass = $this->input->post('password');
		
		$user_det = $this->get_user_det($uname, $pass);
		
		if($user_det[0])
		{
			// cek apakah kaprodi.
			$kap_cek = $this->dsn_model->is_kaprodi($user_det[1])->num_rows();
			if($kap_cek > 0) $kap = true;
			else $kap = false;
			
			// up date session
			$data_user = array(
				'id_user' => $user_det[1],
				'nama_user' => $user_det[2],
				'user' => $user_det[3],
				'is_logged_in' => $user_det[0],
				'is_kaprodi' => $kap
			);
			
			$this->session->set_userdata($data_user);
			redirect('sipbta');
		}
		
		else
		{
			//$this->index();
			$back = anchor('','di sini');
			show_error("$user_det[1]. Silahkan klik ".$back." untuk kembali ke halaman home.");
		}
	}
	
	function sign_out()
	{
		//$this->session->sess_destroy();
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('nama_user');
		$this->session->unset_userdata('user');
		$this->session->unset_userdata('is_logged_in');
		$this->session->unset_userdata('is_kaprodi');
		redirect('sipbta');
	}
	
	function show_list_mhs($offset = 0)
	{
		
		
		$limit = 20;
		
		// offset
		$uri_segment = 3;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		// cek apakah kaprodi
		$user = $this->session->userdata('user');
		$kapr = $this->dsn_model->is_kaprodi($this->session->userdata('id_user'));
		if($user == 'kaprodi'){
			$bbbb = $this->sipbta_model->list_mhs($limit,$offset,'nama ASC',$kapr->row()->id_prodi)->result(); // limit, order by.
			$cccc = $this->sipbta_model->list_mhs('','','',$kapr->row()->id_prodi)->num_rows(); // count all result
		}else{
			$bbbb = $this->sipbta_model->list_mhs($limit,$offset,'nama ASC')->result(); // limit, order by.
			$cccc = $this->sipbta_model->list_mhs()->num_rows(); // count all result
		}
		
		$mhs_paged = $bbbb;
		
		// generate pagination
		$conf['base_url'] = site_url('sipbta/show_list_mhs/');
		$conf['total_rows'] = $cccc;
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'NIM', 'Nama', 'Prodi', 'Aksi');
		$i = 0 + $offset;
		foreach ($mhs_paged as $mhs)
		{
			$ac = anchor('user_class/mahasiswa/show_profil_mhs/'.$mhs->nim,'Lihat profil',array('class'=>'view'));
			$this->table->add_row(++$i, $mhs->nim, $mhs->nama, $mhs->nm_prodi, $ac);
		}
		$data['search'] = '';
		$data['pagination'] = $this->pagination->create_links();
		$data['table'] = $this->table->generate();
		$data['page_title'] = 'List mahasiswa';
		$this->template->display('list', $data, 'pendadaran');
	}
	
	function show_list_dsn($offset = 0)
	{
		$limit = 20;
		
		// offset
		$uri_segment = 3;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$dsn_paged = $this->sipbta_model->list_dsn($limit,$offset,'nm_dsn ASC')->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('sipbta/show_list_dsn/');
		$conf['total_rows'] = $this->sipbta_model->list_dsn('','','','','','','','count'); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Nama', 'Aksi');
		$i = 0 + $offset;
		foreach ($dsn_paged as $dsn)
		{
			$ac = anchor('user_class/dosen/show_profil_dsn/'.$dsn->id_dsn,'Lihat profil',array('class'=>'view'));
			$this->table->add_row(++$i, $dsn->nm_dsn, $ac);
		}
		
		$data['search'] = '';
		$data['pagination'] = $this->pagination->create_links();
		$data['table'] = $this->table->generate();
		$data['page_title'] = 'List dosen';
		$this->template->display('list', $data, 'pendadaran');
	}
	
	function progres_pengerjaan_ta_all($iddn = '')
	{
		$site_url = site_url('ofc2/progres_kaprodi');
		if($iddn != '') $site_url = site_url('ofc2/progres_kaprodi/'.$iddn);
		$data = array(
                        'chart_height'  => '400px',
                        'chart_width'   => '100%',
                        'data_url'      => $site_url
                     );
    
        $this->load->view('grafik', $data);
        
		//$data['page_title'] = 'progres ta'; $this->template->display('grafik', $data, 'pendadaran');
	}
	function show_iframe($title, $url)
	{
		$data['page_title'] = $title; 
		$data['url'] = $url; 
		$this->template->display('iframe', $data, 'pendadaran');
	}
	function progress_mhs_det($nim = '')
	{
		$data['page_title'] = 'progres ta'; $this->template->display('grafik', $data, 'pendadaran');
	}
	
	function progres_pengerjaan_ta($iddn = '')
	{
		//$this->sipbta->progres_pengerjaan_ta_all();
		
		$url = site_url('/sipbta/progres_pengerjaan_ta_all/'.$iddn);
		$title = 'Progres pengerjaan TA';
		$this->show_iframe($title, $url);
	}
	
	function progres_dosen($iddn = '')
	{
		//$this->sipbta->progress_dsn_all();
		
		$url = site_url('/sipbta/progress_dsn_all/'.$iddn = '');
		$title = 'Statistik dosen penguji';
		$this->show_iframe($title, $url);
	}
	
	function progress_dsn_all($iddn = '')
	{
		$data = array(
                        'chart_height'  => '500px',
                        'chart_width'   => '100%',
                        'data_url'      => site_url('ofc2/progres_penguji')
                     );
    
        $this->load->view('grafik', $data);
        
		//$data['page_title'] = 'progres ta'; $this->template->display('grafik', $data, 'pendadaran');
	}
	
	function progress_dsn_det($iddn = '')
	{
		$data = array(
                        'chart_height'  => '400px',
                        'chart_width'   => '80%',
                        'data_url'      => site_url('ofc2/progres_kaprodi')
                     );
    
        $this->load->view('grafik', $data);
        
		//$data['page_title'] = 'progres ta'; $this->template->display('grafik', $data, 'pendadaran');
	}
	
	function about()
	{
		if($this->auth->is_sign_in())
		{
			die('loged in');
		} else
		{
			die('not loged in');
		}
	}

}