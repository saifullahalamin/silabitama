<?php

class Ofc2 extends CI_Controller {
  
	function __construct()
	{
        parent::__construct();
        $this->load->model('user_model/penguji_model', '', TRUE);
        $this->load->model('user_model/dsn_model', '', TRUE);
        $this->load->model('subsystem_model/proposal_model', '', TRUE);
        $this->load->model('subsystem_model/pembimbingan_model', '', TRUE);
        $this->load->model('subsystem_model/seminar_model', '', TRUE);
        $this->load->model('subsystem_model/pendadaran_model', '', TRUE);
	}

	function progres_kaprodi($iddn = '') 
	{
		$user = $this->session->userdata('user');
		if($user == 'pembimbing' ){						// pembimbing -- hanya statistik mhs bimbingannya saja
			$y_range = 5;
			$step_r = 5;
			
			$jdl = 0;
			$prop = 0;
			$b1 = (int) $this->pembimbingan_model->riwayat_bimbb_count_pembb($iddn, '1')->row()->jml;
			$b2 = (int) $this->pembimbingan_model->riwayat_bimbb_count_pembb($iddn, '2')->row()->jml;
			$b3 = (int) $this->pembimbingan_model->riwayat_bimbb_count_pembb($iddn, '3')->row()->jml;
			$b4 = (int) $this->pembimbingan_model->riwayat_bimbb_count_pembb($iddn, '4')->row()->jml;
			$b5 = (int) $this->pembimbingan_model->riwayat_bimbb_count_pembb($iddn, '5')->row()->jml;
			$sem = $this->seminar_model->sttk_sem($iddn,'Sudah dilaksanakan')->num_rows();
			$pendd = (int) $this->pendadaran_model->sttk_pendd($iddn,'Sudah dilaksanakan')->row()->jml;
			$ni = $this->pendadaran_model->nilai_ta('',$iddn)->num_rows();
			
			if($b1 > $y_range) $y_range = $b1;
			if($b2 > $y_range) $y_range = $b2;
			if($b3 > $y_range) $y_range = $b3;
			if($b4 > $y_range) $y_range = $b4;
			if($b5 > $y_range) $y_range = $b5;
			if($sem > $y_range) $y_range = $sem;
			if($pendd > $y_range) $y_range = $pendd;
			if($ni > $y_range) $y_range = $ni;
			
		}else{									// kaprodi -- semua mhs prodi  // lainnya -- semua mhs STMIK JAY
			$id_user = $this->session->userdata('id_user');
			$prodi = '';
			$y_range = 5;
			$step_r = 1;
			
			if($user == 'kaprodi') {
				$prodi = $this->dsn_model->is_kaprodi($id_user)->row()->id_prodi;
			}
			
			$jdl = $this->proposal_model->jdl_saja($prodi)->num_rows();
			$prop = $this->proposal_model->proposal_ta('','','','','','','','','Disetujui','','','','',$prodi)->num_rows();
			$b1 = (int) $this->pembimbingan_model->riwayat_bimbb_count_kap($prodi, '1')->row()->jml;
			$b2 = (int) $this->pembimbingan_model->riwayat_bimbb_count_kap($prodi, '2')->row()->jml;
			$b3 = (int) $this->pembimbingan_model->riwayat_bimbb_count_kap($prodi, '3')->row()->jml;
			$b4 = (int) $this->pembimbingan_model->riwayat_bimbb_count_kap($prodi, '4')->row()->jml;
			$b5 = (int) $this->pembimbingan_model->riwayat_bimbb_count_kap($prodi, '5')->row()->jml;
			$sem = $this->seminar_model->list_pendf_sem('','','','Sudah dilaksanakan','','','',$prodi)->num_rows();
			$pendd = $this->pendadaran_model->list_pendf_pendd('','','','','Sudah dilaksanakan','',$prodi)->num_rows();
			$ni = $this->pendadaran_model->nilai_ta($prodi)->num_rows();
			
			if($jdl > $y_range) $y_range = $jdl;
			if($prop > $y_range) $y_range = $prop;
			if($b1 > $y_range) $y_range = $b1;
			if($b2 > $y_range) $y_range = $b2;
			if($b3 > $y_range) $y_range = $b3;
			if($b4 > $y_range) $y_range = $b4;
			if($b5 > $y_range) $y_range = $b5;
			if($sem > $y_range) $y_range = $sem;
			if($pendd > $y_range) $y_range = $pendd;
			if($ni > $y_range) $y_range = $ni;
			
			if($y_range >10) $step_r = 5;
			if($y_range >20) $step_r = 10;
			if($y_range >100) $step_r = 50;
		}
		
		
		
		$this->load->helper('ofc2');
		
		$title = new title( 'Progres pengerjaan TA -- '.date("d M Y") );						// title
		$title->set_style( '{color: #567300; font-size: 14px}' );
		
		$bar = new bar_sketch( '#81AC00', '#567300', 5 );
		$bar->set_tooltip( '#val# Mahasiswa' );
		$bar->set_values( array($jdl,$prop,$b1,$b2,$b3,$b4,$b5,$sem,$pendd,$ni) );				// value
		
		$x = new x_axis();																		// x
		$x->steps(1);
		$xlabel = array('Judul', 'Proposal', 'Bab 1', 'Bab 2', 'Bab 3', 'Bab 4', 'Bab 5', 'Seminar', 'Pendadaran', 'Mendapat nilai');
		if($user == 'pembimbing') {
			$bar->set_values( array($b1,$b2,$b3,$b4,$b5,$sem,$pendd,$ni) );
			$xlabel = array('Bab 1', 'Bab 2', 'Bab 3', 'Bab 4', 'Bab 5', 'Seminar', 'Pendadaran', 'Mendapat nilai');
		}
		$x->set_labels_from_array($xlabel);
		
		$y = new y_axis();																		// y
		$y->set_range( 0, $y_range, $step_r);
		
		$animation_1= isset($_GET['animation_1'])?$_GET['animation_1']:'pop';					// animasi
		$delay_1    = isset($_GET['delay_1'])?$_GET['delay_1']:0.5;
		$cascade_1    = isset($_GET['cascade_1'])?$_GET['cascade_1']:1;
		$bar->set_on_show(new bar_on_show($animation_1, $cascade_1, $delay_1));
		
		$chart = new open_flash_chart();														// chart
		$chart->set_title( $title );
		$chart->add_element( $bar );
		$chart->set_x_axis( $x );
		$chart->set_y_axis( $y );
		
		echo $chart->toPrettyString();
	}
	
	function progres_penguji()
	{
		$y_range = 4;
		$ak = $this->penguji_model->akumulasi_penguji();
		
		$penguji = array();
		$akumulasi = array();
		
		foreach ($ak->result() as $row){
			$penguji[] = $row->nm_dsn;
			$jml_p = (int) $row->akum;
			$akumulasi[] = $jml_p;
			if($jml_p > $y_range) $y_range = $jml_p;
		}
		
		$this->load->helper('ofc2');
		
		$title = new title( 'Progres dosen penguji -- '.date("d M Y") );						// title
		$title->set_style( '{color: #567300; font-size: 14px}' );
		
		$bar = new bar_sketch( '#81AC00', '#567300', 5 );
		$bar->set_tooltip( '#val# kali menguji' );
		$bar->set_values( $akumulasi );															// value
		
		$x = new x_axis();																		// x
		$x->steps(1);
		$x->set_labels_from_array($penguji);
		$x->labels->rotate(90);
		$x->labels->set_size( 12 );
		
		$y = new y_axis();																		// y
		$y->set_range( 0, $y_range, 1);
		
		$animation_1= isset($_GET['animation_1'])?$_GET['animation_1']:'pop';					// animasi
		$delay_1    = isset($_GET['delay_1'])?$_GET['delay_1']:0.5;
		$cascade_1    = isset($_GET['cascade_1'])?$_GET['cascade_1']:1;
		$bar->set_on_show(new bar_on_show($animation_1, $cascade_1, $delay_1));
		
		$chart = new open_flash_chart();														// chart
		$chart->set_title( $title );
		$chart->add_element( $bar );
		$chart->set_x_axis( $x );
		$chart->set_y_axis( $y );
		
		echo $chart->toPrettyString();
	}
}