<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Penguji extends CI_Controller {
	protected $tahap;
	
	function __construct()
	{
		parent::__construct();
		
		$this->tahap = 'pendadaran';
		
		// load model
		$this->load->model('subsystem_model/pendadaran_model','',true);
		// load controller
		$this->pendadaran = new Pendadaran();
		
	}
		
	function index()
	{
		
	}
	
	// tahap proposal -- tdk ada
	// tahap pembimbingan -- tdk ada
	// tahap seminar -- tdk ada
	
	// tahap pendadaran
	function lihat_jdwl_pendd()
	{
		$this->pendadaran->show_jdwl_pendd_all();
	}
	
	function beri_revisi()
	{
		$dsn_penguji = $this->session->userdata('id_user');
		
		// list mhs yang diuji
		$mhs_filtered = $this->pendadaran_model->list_pendf_pendd('no_gb', '','','','','','','',$dsn_penguji, 'pembb');
		$list_mhs = $mhs_filtered->result();
		$jml_mhs = $mhs_filtered->num_rows();
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'NIM', 'Nama mahasiswa', 'Status revisi', 'Action');
		$i = 0;
		foreach ($list_mhs as $mhs)
		{			
			if($mhs->stts_rev == 'X'){ 
				$links = anchor('subsystem_class/pendadaran/revisi_form/'.$mhs->id_pendd,'Penilaian / Revisi',array('class'=>'view'));
			}elseif($mhs->stts_rev == 'Tidak ada revisi' || $mhs->stts_rev == 'Sudah merevisi'){
				$links = anchor('subsystem_class/pendadaran/revisi_form/'.$mhs->id_pendd.'/det','Lihat',array('class'=>'view'));
				if($mhs->dsn_penguji == $mhs->pembb_aktif) 
				$links .= ' / '.anchor('user_class/penguji/beri_nilai/'.$mhs->id_pendd,'Beri penilaian terahir',array('class'=>'view'));
			}elseif($mhs->stts_rev == 'Sudah mendapat penilaian terahir'){
				$links = anchor('subsystem_class/pendadaran/beri_nilai/'.$mhs->id_pendd.'/det','Lihat',array('class'=>'view'));
			}else { 
				$links = anchor('subsystem_class/pendadaran/revisi_form/'.$mhs->id_pendd.'/det','Lihat',array('class'=>'view'));
				$links .= ' / '.anchor('subsystem_class/pendadaran/verifikasi_revisi_form/'.$mhs->id_pendd,'Verifikasi revisi',array('class'=>'view'));
			}
			$this->table->add_row(++$i, $mhs->nim, $mhs->nama, $mhs->stts_rev,  $links);
		}
		
		// view
		if($jml_mhs != 0){ 					
			$data['pemberitahuan'] = '';
			$data['pagination'] = '';
			$data['table'] = $this->table->generate();
		}else {								
			$data['pemberitahuan'] = 'Belum ada mahasiswa yang diuji';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		$data['link'] = '';
		
		$data['page_title'] = 'Revisi pendadaran';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data, $this->tahap);
	}
	
	function verifikasi_revisi($id_pendd)
	{
		$this->pendadaran->verifikasi_revisi_form($id_pendd);
	}
	
	function beri_nilai($id_pendd, $det = '')
	{
		$this->pendadaran->beri_nilai($id_pendd, $det);
	}
	
	function lihat_nilai_terahir(){}
}