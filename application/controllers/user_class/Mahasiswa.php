<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../sipbta.php");
require_once(dirname(__FILE__)."/../subsystem_class/proposal.php");
require_once(dirname(__FILE__)."/../subsystem_class/pembimbingan.php");
require_once(dirname(__FILE__)."/../subsystem_class/seminar.php");
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Mahasiswa extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		// load model
		$this->load->model('user_model/mhs_model', '', TRUE);
		$this->load->model('sipbta_model', '', TRUE);
		$this->load->model('subsystem_model/proposal_model', '', TRUE);
		$this->load->model('subsystem_model/pembimbingan_model', '', TRUE);
		$this->load->model('subsystem_model/seminar_model', '', TRUE);
		$this->load->model('subsystem_model/pendadaran_model', '', TRUE);
		
		// load controller
		$this->sipbta = new Sipbta();
		$this->proposal = new Proposal();
		$this->seminar = new Seminar();
		$this->pembimbingan = new Pembimbingan();
		$this->pendadaran = new Pendadaran();
		
	}
		
	function index()
	{
		echo "ini adalah index class mhs";
		$this->seminar->pendf_sem_form();
		echo "<br />";
		$this->proposal->aadc();
	}

	// sipbta
	function det_mhs($nim)
	{
		return $this->mhs_model->get_detail($nim)->row();
	}
	
	function mhs_progress($nim)
	{
		@$prop 	= $this->proposal_model->proposal_ta('','','','',$nim)->row()->stts_acc;
		@$jdl 	= $this->sipbta_model->ta('select','','','wkt DESC','',$nim)->row()->jdl;
		@$pb 	= $this->pembimbingan_model->dist_pembb('','','','',$nim)->row()->nm_dsn;
		@$ab 	= $this->pembimbingan_model->riwayat_bimbb('select','',$nim)->num_rows();
		@$sem 	= $this->seminar_model->list_pendf_sem('','','','','',$nim)->row()->stts_sem;
		@$pendd = $this->pendadaran_model->list_pendf_pendd('','','','','','','',$nim)->row()->stts_pendd;
		
		// ===================
		$progg['prop'] 			= $prop; 
		$progg['jdl_ta'] 		= $jdl;
		$progg['pembb'] 		= $pb; 
		$progg['akum_bimbb'] 	= $ab." Kali bimbingan"; 
		$progg['sem'] 			= $sem;
		$progg['pendd'] 		= $pendd;
		return $progg;
	}
	
	function show_profil_mhs($nim = '')
	{
		$data['page_title'] = 'Profil Mahasiswa';
		
		if($nim == '') $nim = $this->session->userdata('id_user');
		$profil = $this->mhs_model->profil_mhs($nim)->row();
		
		$p = $this->mhs_progress($nim);
		
		$jk = $profil->jk;
		if($profil->jk == 'L') $jk = 'Laki laki';
		elseif($profil->jk == 'P') $jk = 'Perempuan';
		
		$atts = array(
              'width'      => '900',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
		if($profil->fb != '') $profil->fb = anchor_popup($profil->fb,$profil->fb, $atts);
		$profil = array(
			"NIM" => '<strong>'.$profil->nim.'</strong>',
			"Nama" => '<strong>'.$profil->nama.'</strong>',
			"Nomor HP" => @$profil->no_telp,
			"Akun facebook" => @$profil->fb,
			"Jenis kelamin" => $jk,
			"Kabupaten" => @$profil->kab,
			"Provinsi" => @$profil->prov,
			" " => ' ',
			"Proposal" => $p['prop'],
			"Judul TA" => $p['jdl_ta'],
			"Dosen pembimbing TA" => $p['pembb'],
			"Akumulasi bimbingan" => $p['akum_bimbb'],
			"Seminar" => $p['sem'],
			"Pendadaran" => $p['pendd']
			);
		
		// generate table data
		$this->table->set_empty("-");
		$this->table->set_heading('Profil Mahasiswa', '');
		foreach ($profil as $key => $item)
		{	
			$this->table->add_row($key, $item);
		}
		
		$temp = array ( 'table_open'  => '<table border="0" cellpadding="4" cellspacing="0" class="profil">', 
						'row_alt_start' => '<tr class="alt">' 
						);
		$this->table->set_template($temp);
		$data['table'] = $this->table->generate();
		if($this->uri->segment(4) != '') $data['table'] .= '<a data-rel="back" data-role="button" data-inline="true">Kembali</a>';
		else $data['table'] .= anchor('user_class/mahasiswa/edit_profil" data-role="button" data-inline="true"','Edit profil');
		
		$data['page_title'] = 'Profil Mahasiswa';
		$this->template->display('user_view/profil_dosen', $data);
	}
	
	function edit_profil()
	{
		$nim = $this->session->userdata('id_user');
		$profil = $this->mhs_model->profil_mhs($nim)->row();
		
		$data['nim'] = $profil->nim;
		$data['nama'] = $profil->nama;
		$data['telp'] = $profil->no_telp;
		$data['fb'] = $profil->fb;
		
		$data['jk'] =  form_dropdown('jk" data-native-menu="false', array('L'=>'Laki laki', 'P'=>'Perempuan'), '$profil->jk');
		$data['kab'] = $profil->kab;
		$data['prov'] = $profil->prov;
		
		$data['action'] = site_url('/user_class/mahasiswa/edit_profil_proses');
		$data['page_title'] = 'Profil Mahasiswa';
		$this->template->display('user_view/edit_profil_mhs.php', $data);
	}
	
	function edit_profil_proses()
	{
		$nim = $this->session->userdata('id_user');
		$jk = $this->input->post('jk');
		$hp = $this->input->post('hp');
		$fb = $this->input->post('fb');
		$kab = $this->input->post('kab');
		$prov = $this->input->post('prov');
		
		$this->mhs_model->edit_profil($nim, $jk, $hp, $fb, $kab, $prov);
		redirect('user_class/mahasiswa/show_profil_mhs');
	}
	function list_dsn($offset = 0)
	{
		$this->sipbta->show_list_dsn($offset);
	}
	
	function lihat_profil_dsn(){}
	
	function lihat_progres_pengerjaan_ta()
	{
		$this->sipbta->progres_pengerjaan_ta();
	}
	
	// tahap proposal
	function ajukan_jdl()
	{
		$this->proposal->pengajuan_jdl();
	}
	
	function ajukan_prop()
	{
		$this->proposal->pengajuan_prop();
	}
	
	function request_pembb()
	{
		$this->proposal->request_pembb_form();
	}
	
	function lihat_tanggapan_request_pembb(){}
	
	// tahap pembimbingan
	function lihat_dist_pembb($offset = 0)
	{
		$this->pembimbingan->show_dist_pembb($offset);
	}
	
	function lihat_akumulasi_bimb(){}
	
	function lihat_cttn_bimbb(){}
	
	function ubah_jdl($id = "")
	{
		$this->pembimbingan->ubah_jdl_form($id);
	}
	
	// tahap seminar
	function lihat_prasyarat_sem(){}
	
	function request_sem(){}
	
	function lihat_tanggapan_request_sem(){}
	
	function mendaftar_sem()
	{
		$nim = $this->session->userdata('id_user');
		
		$sem = $this->seminar_model->list_pendf_sem('','','tgl_pendf DESC','','',$nim);
	
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Tanggal daftar', 'Ruang seminar', 'Penilaian', 'Status', 'Aksi');
		$i=0;
		foreach ($sem->result() as $se)
		{
			$this->table->add_row(++$i, $se->tgl_pendf, $se->ruang, $se->penilaian,  $se->stts_sem, 
				anchor('subsystem_class/seminar/seminar_det/'.$se->id_sem,'Detail',array('class'=>'view')));
		}
		
		$data['pemberitahuan'] = '<div class="data">'.$this->table->generate().'</div>';
		$data['pemberitahuan'] .= '<p>'.anchor('subsystem_class/seminar/pendf_sem_form/" data-role="button" data-inline="true','mendaftar seminar',array('class'=>'view')).'</p>';
		$data['page_title'] = 'Riwayat seminar';
		$this->template->display('pemberitahuan', $data, 'seminar');
		
		//$this->seminar->pendf_sem_form();
	}
	
	function lihat_jdwl_sem()
	{
		$this->seminar->show_jdwl_sem_all();
	}
	
	function lihat_nilai_sem_pendd()
	{
		$this->mendaftar_sem();
	}
	
	// tahap pendadaran
	function lihat_prasyarat_pendd(){}
	
	function mendaftar_pendd()
	{
		$nim = $this->session->userdata('id_user');
		
		$pendd = $this->pendadaran_model->list_pendf_pendd('','','','tgl_pendf DESC','','','',$nim);
	
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'Tanggal daftar', 'Status', 'Aksi');
		$i=0;
		foreach ($pendd->result() as $pe)
		{
			$this->table->add_row(++$i, $pe->tgl_pendf, $pe->stts_pendd, 
				anchor('subsystem_class/pendadaran/pendd_det/'.$pe->id_pendd,'Detail',array('class'=>'view')));
		}
		
		$data['pemberitahuan'] = '<div class="data">'.$this->table->generate().'</div>';
		$data['pemberitahuan'] .= '<p>'.anchor('subsystem_class/pendadaran/pendf_pendd_form/" data-role="button" data-inline="true','mendaftar Pendadaran',array('class'=>'view')).'</p>';
		$data['page_title'] = 'Riwayat Pendadaran';
		$this->template->display('pemberitahuan', $data, 'seminar');
		
		//$this->pendadaran->pendf_pendd_form();
	}
	
	function lihat_jdwl_pendd()
	{
		$this->pendadaran->show_jdwl_pendd_all();
	}
	
	function lihat_revisi(){}
}
