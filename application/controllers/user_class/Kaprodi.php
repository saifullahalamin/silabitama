<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../sipbta.php");
require_once(dirname(__FILE__)."/../subsystem_class/proposal.php");
require_once(dirname(__FILE__)."/../subsystem_class/pembimbingan.php");
require_once(dirname(__FILE__)."/../subsystem_class/seminar.php");
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Kaprodi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		// load controller
		$this->sipbta = new Sipbta();
		$this->proposal = new Proposal();
		$this->seminar = new Seminar();
		$this->pembimbingan = new Pembimbingan();
		$this->pendadaran = new Pendadaran();
		
	}
		
	function index()
	{
		
	}
	
	// sipbta
	function lihat_list_mhs($offset = 0)
	{
		$this->sipbta->show_list_mhs($offset);
	}
	
	function lihat_list_dsn($offset = 0)
	{
		$this->sipbta->show_list_dsn($offset);
	}
	
	function lihat_profil_mhs(){}
	
	function lihat_profil_dsn(){}
	
	function lihat_progres_pengerjaan_ta()
	{
		$this->sipbta->progres_pengerjaan_ta();
	}
	
	function lihat_progres_dosen()
	{
		$this->sipbta->progres_dosen();
	}
	function iframe()
	{
		$this->sipbta->show_iframe();
	}
	// tahap proposal
	function lihat_list_pengajuan_jdl()
	{
		$this->proposal->show_list_pengajuan_jdl();
	}
	
	function lihat_list_pengajuan_prop()
	{
		$this->proposal->show_list_pengajuan_prop();
	}
	
	function lihat_list_jdl_ta(){}
	
	// tahap pembimbingan
	function tentukan_dist_pembb()
	{
		$this->pembimbingan->dist_pembb_form();
	}
	
	function lihat_dist_pembb($offset = 0)
	{
		$this->pembimbingan->show_dist_pembb($offset);
	}
	
	// tahap seminar
	function lihat_jdwl_sem()
	{
		$this->seminar->show_jdwl_sem_all();
	}
	
	// tahap pendadaran
	function lihat_list_pendf_pendd()
	{
		$this->pendadaran->list_pendf_pendd();
	}
	
	function tentukan_penguji(){}
}