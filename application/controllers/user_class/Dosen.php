<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../sipbta.php");
require_once(dirname(__FILE__)."/../subsystem_class/proposal.php");
require_once(dirname(__FILE__)."/../subsystem_class/pembimbingan.php");
require_once(dirname(__FILE__)."/../subsystem_class/seminar.php");
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Dosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		// load model
		$this->load->model('user_model/dsn_model', '', TRUE);
		$this->load->model('user_model/penguji_model', '', TRUE);
		$this->load->model('subsystem_model/pembimbingan_model', '', TRUE);
		
		// load controller
		$this->sipbta = new Sipbta();
		$this->proposal = new Proposal();
		$this->seminar = new Seminar();
		$this->pembimbingan = new Pembimbingan();
		$this->pendadaran = new Pendadaran();
	}
		
	function index()
	{
		
	}
	
	// sipbta
	function swicth($level){
		$level2 = $level;
		// cek apakah benar2 kaprodi
		if($level == "kaprodi"){
			$a = $this->session->userdata('id_user');
			$b = $this->dsn_model->is_kaprodi($a)->num_rows();
			if($b == 0) $level2 = "dosen";
		}
		
		// update sesion
		$this->session->set_userdata('user', $level2);
		
		// redirect ke home
		redirect('sipbta');
	}
	function det_dsn($iddn)
	{
		return $this->dsn_model->get_detail($iddn)->row();
	}
	
	function show_profil_dsn($id_dsn = '')
	{
		if($id_dsn == '') $id_dsn = $this->session->userdata('id_user');
		
		$dsn = $this->dsn_model->p_dsn($id_dsn)->row();
		
		$xkali = $this->penguji_model->akumulasi_penguji($id_dsn)->row();
		$list_mhs_bimbb = $this->pembimbingan_model->dist_pembb('','','','','','','',$id_dsn);
		$list_mb = '';
		if ($list_mhs_bimbb->num_rows() > 0)
		{
		   foreach ($list_mhs_bimbb->result() as $row)
		   {
		     $list_mb .= anchor('user_class/mahasiswa/show_profil_mhs/'.$row->nim,"$row->nama").'<br />';
		   }
		}
		
		$atts = array(
              'width'      => '800',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
		if($dsn->fb != '') $dsn->fb = anchor_popup($dsn->fb,$dsn->fb, $atts);
		$profil = array(
			"Nama" => '<strong>'.$dsn->nm_dsn.'</strong>',
			"Nomor HP" => $dsn->no_telp,
			"Akun facebook" => $dsn->fb,
			"Kabupaten" => $dsn->kab,
			"Provinsi" => $dsn->prov,
			"Akumulasi menjadi penguji" => $xkali->akum.' kali menjadi penguji'
			);
		
		// generate table data
		$this->table->set_empty("-");
		$this->table->set_heading('Profil Dosen', '');
		foreach ($profil as $key => $item)
		{	
			$this->table->add_row($key, $item);
		}
		
		// last row
		$mhs_b = array('data' => 'Mahasiswa bimbingan', 'class' => 'p1_mhs_bimbb');
		$l_b = array('data' => $list_mb, 'class' => 'p1_mhs_bimbb');
		$this->table->add_row($mhs_b, $l_b);
		
		$temp = array ( 'table_open'  => '<table border="0" cellpadding="4" cellspacing="0" class="profil">', 
						'row_alt_start' => '<tr class="alt">' 
						);
		$this->table->set_template($temp);
		
		$data['table'] = $this->table->generate();
		
		if($this->uri->segment(4) != '') $data['table'] .= '<a data-rel="back" data-role="button" data-inline="true">Kembali</a>';
		else $data['table'] .= anchor('user_class/dosen/edit_profil" data-role="button" data-inline="true"','Edit profil');
		
		$data['page_title'] = 'Profil Mahasiswa';
		$this->template->display('user_view/profil_dosen', $data);
	}
	
	function edit_profil()
	{
		$id_dsn = $this->session->userdata('id_user');
		$profil = $this->dsn_model->p_dsn($id_dsn)->row();
		
		$data['nama'] = $profil->nm_dsn;
		$data['telp'] = $profil->no_telp;
		$data['fb'] = $profil->fb;
		
		$data['jk'] =  form_dropdown('jk" data-native-menu="false', array('L'=>'Laki laki', 'P'=>'Perempuan'), '$profil->jk');
		$data['kab'] = $profil->kab;
		$data['prov'] = $profil->prov;
		
		$data['action'] = site_url('/user_class/dosen/edit_profil_proses');
		$data['page_title'] = 'Profil Mahasiswa';
		$this->template->display('user_view/edit_profil_dsn.php', $data);
	}
	
	function edit_profil_proses()
	{
		$id_dsn = $this->session->userdata('id_user');
		$hp = $this->input->post('hp');
		$fb = $this->input->post('fb');
		$kab = $this->input->post('kab');
		$prov = $this->input->post('prov');
		
		$this->dsn_model->edit_profil($id_dsn, $hp, $fb, $kab, $prov);
		redirect('user_class/dosen/show_profil_dsn');
	}
	
	function lihat_list_mhs($offset = 0)
	{
		$this->sipbta->show_list_mhs($offset);
	}
	
	function lihat_profil_mhs(){}
	
	// tahap proposal
	function lihat_request_mjd_pembb()
	{
		$this->proposal->show_list_request_pembb();
	}
	
	function tanggapai_request_mjd_pembb(){}
	
	// tahap pembimbingan
	function lihat_dist_pembb($offset = 0)
	{
		$this->pembimbingan->show_dist_pembb($offset);
	}
	
	// tahap seminar
	function lihat_request_mjd_pengg_pembb_sem(){}
	
	// tahap pendadaran
	function lihat_pemberitahuan_mjd_penguji(){}
}