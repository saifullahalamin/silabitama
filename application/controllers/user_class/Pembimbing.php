<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../sipbta.php");
require_once(dirname(__FILE__)."/../subsystem_class/pembimbingan.php");
require_once(dirname(__FILE__)."/../subsystem_class/seminar.php");
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Pembimbing extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		// load model
		$this->load->model('subsystem_model/pembimbingan_model','',true);
		$this->load->model('subsystem_model/seminar_model','',true);
		
		// load controller
		$this->sipbta = new Sipbta();
		$this->seminar = new Seminar();
		$this->pembimbingan = new Pembimbingan();
		$this->pendadaran = new Pendadaran();
	}
		
	function index()
	{
		
	}
	
	// tahap proposal -- tdk ada
	
	// tahap pembimbingan
	function lihat_list_mhs_bimbb()
	{
		$id_dsn = $this->session->userdata('id_user');
		
		$limit = 20;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$mhs_bimbb_paged = $this->pembimbingan_model->dist_pembb($limit,$offset,'','','','','',$id_dsn)->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('user_class/pembimbing/lihat_list_mhs_bimbb/');
		$conf['total_rows'] = $this->pembimbingan_model->dist_pembb('','','','','','','',$id_dsn,'','','','count'); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'NIM', 'Nama', 'aksi');
		$i = 0 + $offset;
		foreach ($mhs_bimbb_paged as $mhs)
		{
			$this->table->add_row(++$i, $mhs->nim, $mhs->nama, 
				anchor('subsystem_class/pembimbingan/catat_progres_bimbb_form/'.$mhs->id_bimbb,'Catat bimbingan',array('class'=>'view')));
		}
		
		
		// view
		if($conf['total_rows'] != 0){ 					// sudah punya mahasiswa bimbingan
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum punya mahasiswa bimbingan
			$data['pemberitahuan'] = 'Anda belum memiliki mahasiswa bimbingan';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		$data['link'] = anchor('user_class/dosen/lihat_request_mjd_pembb" data-role="button" data-inline="true','Lihat request menjadi pembimbing');
		
		$data['page_title'] = 'List mahasiswa bimbingan';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data);
	}
	function catat_progres_bimbb(){}
	
	function lihat_progres_pengerjaan_ta_mhs_bimbb()
	{
		$iddn = $this->session->userdata('id_user');
		$this->sipbta->progres_pengerjaan_ta($iddn);
	}
	
	// tahap seminar
	function lihat_request_sem(){}
	
	function tanggapi_req_sem(){}
	
	function menilai_sem()
	{
		$id_dsn = $this->session->userdata('id_user');
		
		$limit = 3;
		
		// offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// load data
		$mhs_bimbb_paged = $this->seminar_model->list_pendf_sem($limit,$offset,'','','','','','',$id_dsn)->result(); // limit, order by.
		
		// generate pagination
		$conf['base_url'] = site_url('user_class/pembimbing/lihat_list_mhs_bimbb/');
		$conf['total_rows'] = $this->seminar_model->list_pendf_sem('','','','','','','','',$id_dsn)->num_rows(); // count all result
		$conf['per_page'] = $limit; 
		$conf['uri_segment'] = $uri_segment;
		$conf['num_links'] = 3;
		$conf['first_link'] = 'Awal';
		$conf['last_link'] = 'Akhir';
		$this->pagination->initialize($conf); 
		
		// generate table data
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'NIM', 'Nama', 'Tanggal seminar', 'Ruang', 'Penilaian', 'Aksi');
		$i = 0 + $offset;
		foreach ($mhs_bimbb_paged as $mhs)
		{
			$this->table->add_row(++$i, $mhs->nim, $mhs->nama, $mhs->tgl_sem, $mhs->ruang, $mhs->penilaian,
				anchor('subsystem_class/seminar/penilaian_sem_form/'.$mhs->id_sem,'Lihat',array('class'=>'view')));
		}
		
		// view
		if($conf['total_rows'] != 0){ 					// sudah punya mahasiswa bimbingan
			$data['pemberitahuan'] = '';
			$data['pagination'] = $this->pagination->create_links();
			$data['table'] = $this->table->generate();
		}else {											// belum punya mahasiswa bimbingan
			$data['pemberitahuan'] = 'Anda belum memiliki mahasiswa bimbingan';
			$data['pagination'] = '';
			$data['table'] = '';
			
		}
		$data['link'] = anchor('user_class/dosen/lihat_request_mjd_pembb" data-role="button" data-inline="true','Lihat request menjadi pembimbing');
		
		$data['page_title'] = 'List mahasiswa bimbingan';
		$this->template->display('subsystem_view/proposal/list_pengajuan_jdl', $data);
		
		//$this->seminar->penilaian_sem_form();
	}
	
	// tahap pendadaran
	function lihat_list_nilai_dr_penguji()
	{
		
	}
	
	function beri_penilaian_terahir(){}
}