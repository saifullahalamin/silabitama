<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// the controller route.
require_once(dirname(__FILE__)."/../sipbta.php");
require_once(dirname(__FILE__)."/../subsystem_class/pembimbingan.php");
require_once(dirname(__FILE__)."/../subsystem_class/seminar.php");
require_once(dirname(__FILE__)."/../subsystem_class/pendadaran.php");

class Opsdik extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		// load model
		$this->load->model('user_model/opsdik_model', '', TRUE);
		
		// load controller
		$this->sipbta = new Sipbta();
		$this->seminar = new Seminar();
		$this->pembimbingan = new Pembimbingan();
		$this->pendadaran = new Pendadaran();
	}
		
	function index()
	{
		
	}
	
	function det_opsdik($npp)
	{
		return $this->opsdik_model->get_detail($npp)->row();
	}
	
	// sipbta
	function lihat_list_mhs($offset = 0)
	{
		$this->sipbta->show_list_mhs($offset);
	}
	
	function lihat_list_dsn($offset = 0)
	{
		$this->sipbta->show_list_dsn($offset);
	}
	
	function lihat_profil_mhs(){}
	
	function lihat_profil_dsn(){}
	
	function lihat_progres_pengerjaan_ta()
	{
		$this->sipbta->progres_pengerjaan_ta();
	}
	
	// tahap proposal -- tidak ada
	// tahap pembimbingan
	function lihat_dist_pembb($offset = 0)
	{
		$this->pembimbingan->show_dist_pembb($offset);
	}
	
	function umumkan_dist_pembb(){}
	
	// tahap seminar
	function umumkan_prasyarat_sem(){}
	
	function lihat_list_pendf_sem()
	{
		$this->seminar->list_pendf_sem();
	}
	
	function cetak_presensi_sem(){}
	
	// tahap pendadaran
	function umumkan_prasyarat_pendd(){}
	
	function lihat_list_pendf_pendd()
	{
		$this->pendadaran->list_pendf_pendd();
	}
	
	function cetak_berita_acara(){}
	
	function verifikasi_pendd(){}
}