<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model/mhs_model', '', TRUE);
		$this->load->model('user_model/dsn_model', '', TRUE);
		$this->load->model('user_model/kaprodi_model', '', TRUE);
		$this->load->model('user_model/opsdik_model', '', TRUE);
	}
		
	function edit_data_prodi($id_prodi = "")
	{
		$profil = $this->kaprodi_model->prodi($id_prodi)->row();
		
		@$data['id_prodi1'] = form_hidden("id_prodi1", $profil->id_prodi);
		@$data['id_prodi'] = $profil->id_prodi;
		@$data['nm_prodi'] = $profil->nm_prodi;
		@$data['kaprodi'] = $this->dsn_model->dsn_option($profil->kaprodi, 'kaprodi');
		
		if($id_prodi != ""){ // edit
			$data['action'] = site_url('/user_class/admin/edit_data_prodi_proses/edit');
			$data['page_title'] = 'Edit Prodi';
		}else{			// tambah
			$data['action'] = site_url('/user_class/admin/edit_data_prodi_proses/tambah');
			$data['page_title'] = 'Tambah Prodi';
		}
		
		$this->template->display('user_view/admin/edit_data_prodi', $data);	
	}
	
	function edit_data_prodi_proses($aksi = "")
	{
		if($aksi == "edit"){
			$id_prodi1 = $this->input->post('id_prodi1');
			$id_prodi = strtoupper($this->input->post('id_prodi'));
			$nm_prodi = ucwords($this->input->post('nm_prodi'));
			$kaprodi = $this->input->post('kaprodi');
			
			$this->kaprodi_model->edit_data_prodi($id_prodi1, $id_prodi, $nm_prodi, $kaprodi);
		
		} else{
			$id_prodi = strtoupper($this->input->post('id_prodi'));
			$nm_prodi = ucwords($this->input->post('nm_prodi'));
			$kaprodi = $this->input->post('kaprodi');
			
			$this->kaprodi_model->add_prodi($id_prodi, $nm_prodi, $kaprodi);
		}
		
		redirect('user_class/admin/edit_data_prodi');
	}
	
	function edit_data_mhs($nim = "")
	{
		$profil = $this->mhs_model->profil_mhs($nim)->row();
		
		@$data['nim1'] = form_hidden("nim1", $profil->nim);
		@$data['nim'] = $profil->nim;
		@$data['nama'] = $profil->nama;
		
		if($nim != ""){ // edit
			$data['action'] = site_url('/user_class/admin/edit_data_mhs_proses/edit');
			$data['page_title'] = 'Edit Profil Mahasiswa';
		}else{			// tambah
			$data['action'] = site_url('/user_class/admin/edit_data_mhs_proses/tambah');
			$data['page_title'] = 'Tambah Mahasiswa';
		}
		
		$this->template->display('user_view/admin/edit_data_mhs', $data);
		
	}
	
	function edit_data_mhs_proses($aksi = "")
	{
		if($aksi == "edit"){
			$nim1 = $this->input->post('nim1');
			$nim = $this->input->post('nim');
			$nama = ucwords($this->input->post('nama'));
			
			$this->mhs_model->edit_data_mhs($nim1, $nim, $nama);
		
		} else{
			$nim = $this->input->post('nim');
			$nama = ucwords($this->input->post('nama'));
			
			$this->mhs_model->add_mhs($nim, $nama);
		}
		
		redirect('user_class/admin/edit_data_mhs');
	}
	
	function edit_data_dsn($iddn = "")
	{
		$profil = $this->dsn_model->p_dsn($iddn)->row();
		
		@$data['id_dsn1'] = form_hidden("id_dsn1", $profil->id_dsn);
		@$data['id_dsn'] = $profil->id_dsn;
		@$data['nm_dsn'] = $profil->nm_dsn;
		
		if($iddn != ""){ // edit
			$data['action'] = site_url('/user_class/admin/edit_data_dsn_proses/edit');
			$data['page_title'] = 'Edit Data Dosen';
		}else{			// tambah
			$data['action'] = site_url('/user_class/admin/edit_data_dsn_proses/tambah');
			$data['page_title'] = 'Tambah Dosen';
		}
		
		$this->template->display('user_view/admin/edit_data_dsn', $data);
	}
	
	function edit_data_dsn_proses($aksi = "")
	{
		if($aksi == "edit"){
			$id_dsn1 = $this->input->post('id_dsn1');
			$id_dsn = $this->input->post('id_dsn');
			$nm_dsn = ucwords($this->input->post('nm_dsn'));
			
			$this->dsn_model->edit_data_dsn($id_dsn1, $id_dsn, $nm_dsn);
			
		} else{
			$id_dsn = $this->input->post('id_dsn');
			$nm_dsn = $this->input->post('nm_dsn');
			
			$this->dsn_model->add_dsn($id_dsn, $nm_dsn);
		}
		
		redirect('user_class/admin/edit_data_dsn');
	}
	
	function edit_data_ops($npp = "")
	{
		$profil = $this->opsdik_model->get_detail($npp)->row();
		
		@$data['npp1'] = form_hidden("npp1", $profil->npp);
		@$data['npp'] = $profil->npp;
		@$data['nama'] = $profil->nama;
		
		if($npp != ""){ // edit
			$data['action'] = site_url('/user_class/admin/edit_data_ops_proses/edit');
			$data['page_title'] = 'Edit Data Personil Opsdik';
		}else{			// tambah
			$data['action'] = site_url('/user_class/admin/edit_data_ops_proses/tambah');
			$data['page_title'] = 'Tambah Personil Opsdik';
		}
		
		$this->template->display('user_view/admin/edit_data_ops', $data);
		
	}
	
	function edit_data_ops_proses($aksi = "")
	{
		if($aksi == "edit"){
			$npp1 = $this->input->post('npp1');
			$npp = $this->input->post('npp');
			$nama = $this->input->post('nama');
			
			$this->opsdik_model->edit_data_ops($npp1, $npp, $nama);
			
		} else{
			$npp = $this->input->post('npp');
			$nama = $this->input->post('nama');
			
			$this->opsdik_model->add_ops($npp, $nama);
		}
		
		redirect('user_class/admin/edit_data_ops');
	}
}