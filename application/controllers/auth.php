<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function is_sign_in()
	{
		$is_login = $this->session->userdata('is_logged_in');
		if($is_login == true)
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	function is_mhs()
	{
		$user = $this->session->userdata('user');
		if($user == 'mahasiswa')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_kaprodi()
	{
		$user = $this->session->userdata('user');
		if($user == 'kaprodi')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_opsdik()
	{
		$user = $this->session->userdata('user');
		if($user == 'opsdik')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_dsn()
	{
		$user = $this->session->userdata('user');
		if($user == 'dosen')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_pembimbing()
	{
		$user = $this->session->userdata('user');
		if($user == 'pembimbing')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_penguji()
	{
		$user = $this->session->userdata('user');
		if($user == 'penguji')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function is_admin()
	{
		$user = $this->session->userdata('user');
		if($user == 'admin')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function dont_have_permission()
	{
		die('maaf anda tdk punya akses');
	}
}