<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Template {
	protected $_ci;
	
    function __construct()
    {
    	//parent::__construct();
    	$this->_ci =& get_instance();
    }
    
    function display($template, $data = null, $tahapan = '')
    {
    	$data['xyz'] = $tahapan;
    	
    	$data['_meta_script_css']=$this->_ci->load->view('includes/meta_script_css', $data, true);
    	$data['_top_header']=$this->_ci->load->view('includes/header_top', $data, true);
    	$data['_header_middle']=$this->_ci->load->view('includes/header_middle', $data, true);
    	$data['_slider']=$this->_ci->load->view('includes/slider', $data, true);
    	$data['_main_menu']=$this->_ci->load->view('includes/main_menu', $data, true);
    	$data['_main_content']=$this->_ci->load->view($template, $data, true);
    	$data['_right_side']=$this->_ci->load->view('includes/right_side', $data, true);
    	$data['_login_pop']= $this->_ci->load->view('login_form', $data, true);
    	$data['_footer']=$this->_ci->load->view('includes/footer', $data, true);
    	$this->_ci->load->view('includes/themes', $data);
    }
}

/* End of file template.php */
/* Location: ./application/libraries/template.php */