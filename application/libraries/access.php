<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access {

	function __construct()
	{
		$this->CI =& get_instance();
		//$auth = $this->CI->config->item('auth');
		
		//$this->users_model =& $this->CI->users_model;	
	}
		
		// cek login
	function login($username, $password)
	{
		//$result = $this->users_model->get_login_info($username);
		if ($result) // result found
		{
			$password = md5($password);
			if ($password === $result->password)
			{
				//start session
				$this->CI->session->set_userdata('user_id', $result->user_id);
				return TRUE;
			}
		}
		return FALSE;
	}
	
		// cek apakah sudah login
	function is_login()
	{
		return(($this->CI->session->userdata('user_id')) ? TRUE : FALSE);
	}
		
		// log out
	function logout()
	{
		$this->CI->session->unset_userdata('user_id');
	}
}

/* End of file access.php */
/* Location: ./application/libraries/access.php */