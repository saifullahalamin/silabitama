<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Subsystem_class {
	
	protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
		$this->CI->load->library("../controllers/subsystem_class/seminar");
		$this->CI->load->library("../controllers/subsystem_class/proposal");
		$this->CI->load->library("../controllers/subsystem_class/seminar");
		$this->CI->load->library("../controllers/subsystem_class/pendadaran");
    }
    
    public function proposal()
    {
    	$this->CI->load->library("../controllers/subsystem_class/proposal");
    }
    
     public function pembimbingan()
    {
    	$this->CI->load->library("../controllers/subsystem_class/pembimbingan");
    }
    
     public function seminar()
    {
    	$this->CI->load->library("../controllers/subsystem_class/seminar");
    }
    
     public function pendadaran()
    {
    	$this->CI->load->library("../controllers/subsystem_class/pendadaran");
    }
}