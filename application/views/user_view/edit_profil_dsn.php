<form action="<?php echo $action; ?>" method="post">
<div data-role="fieldcontain">
    <label>Nama</label>: <?php echo $nama; ?>
</div>
<div data-role="fieldcontain">
    <label for="hp">No telp</label>: 
    <input data-inline="true" type="tel" data-clear-btn="true" name="hp" value="<?php echo $telp; ?>" placeholder="+6200000 -- no hape yang bisa dihubungi">
</div>
<div data-role="fieldcontain">
    <label>Akun facebook</label>: 
    <input type="text" data-clear-btn="true" name="fb" value="<?php echo $fb; ?>" placeholder="Akun facebook saya">
</div>
<div data-role="fieldcontain">
    <label>Kab</label>: 
    <input type="text" data-clear-btn="true" name="kab" value="<?php echo $kab; ?>" placeholder="Saya berasal dari kabupaten ...">
</div>
<div data-role="fieldcontain">
    <label>Provinsi</label>: 
    <input type="text" data-clear-btn="true" name="prov" value="<?php echo $prov; ?>" placeholder="Saya berasal dari provinsi ...">
</div>
<input type="submit" data-inline="true" value="Simpan">