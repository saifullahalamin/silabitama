<div id="footer1">
<div class="ui-grid-b ui-responsive">
    <div class="ui-block-a">
    	<div class="ui-body ui-body-a">
    		<ul id="f1_left">
    			<li><a href="http://ta.stmikayani.ac.id">Pedoman Tugas Akhir</a></li>
    			<li><a href="http://sijay.stmikayani.ac.id/portal/">Portal Kampus STMIK Jend. A Yani</a></li>
    			<li><a href="http://semut.stmikayani.ac.id">Website Kampus STMIK Jend. A Yani</a></li>
    		</ul>
    	</div>
	</div>
    <div class="ui-block-b">
    	<div class="ui-body ui-body-a">
    		<div class="center">
    			<a href="http://semut.stmikayani.ac.id/">
    				<?php echo "<img id=\"SIJAY\" src=\"".base_url('public/images/logo STMIK JAY.png')."\" alt=\"Seminar\" width=\"80%\"/>";?>
    			</a>
    		</div>	
    	</div>    
	</div>
    <div class="ui-block-c">
    	<div class="ui-body ui-body-a">
    		<ul id="f1_right">
    			<li><a href="https://www.facebook.com/groups/ta.stmikayani/">Group Facebook Bimbingan Tugas Akhir</a></li>
    			<li><a href="">Akun Twitter Kampus STMIK Jend. A Yani</a></li>
    			<li><a href="https://www.facebook.com/groups/stmikayani/">Group Facebook Kampus STMIK Jend. A Yani</a></li>
    		</ul>
    	</div>
	</div>
</div><!-- /grid-d -->
</div><!-- ================================ (footer 1) -->
<div id="footer2">
&copy 2013 - Saifullah al amin<br />
D3 - Manajemen Informatika STMIK Jenderal Achmad Yani Yogyakarta
</div><!-- ================================ (footer 2) -->