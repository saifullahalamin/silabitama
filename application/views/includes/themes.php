<!DOCTYPE html>
<html>
 <head>
 	<?php echo $_meta_script_css;?>
  	<title><?php echo $page_title; ?></title>
 </head>
 <body>
 	<div data-role="page">
 		<div data-role="header" data-position="fixed">
 			<div id="top_header">
 				<?php echo $_top_header;?>
 			</div>
 		</div><!-- ==================================== /header_top (header 1) -->
 		<div data-role="header">
 			<div id="middle_header">
	 			<?php echo $_header_middle;?>
 			</div> 			
 			<div id="active">
 				
 			</div>
 		</div><!-- ==================================== /header_middle (header 2) -->
 		
 		<div data-role="content">
	    	<div id="slider">
	    		<?php echo $_slider; ?>
	    	</div> <!-- =============================== /slider -->
	
	    	<div>
		    	<div id="main_content">
		    		<?php echo $_main_content; ?>
		    		<?php echo $_login_pop; ?>
		    	</div>
	    	</div>
	    </div><!-- ==================================== /content -->
	    
	    <div data-role="panel" data-position-fixed="true" id="nav-panel">
	    	<?php echo $_main_menu; ?>
	    </div><!-- ==================================== /panel -->
	    
	    <div  data-role="footer">
	    	<?php echo $_footer; ?>
	    </div><!-- ==================================== /footer -->
	</div><!-- /page -->
 </body>
</html>