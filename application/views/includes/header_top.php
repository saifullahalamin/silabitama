<?php
if($this->session->userdata('is_logged_in'))
{
	$dsn_0 		= '<a href="'.site_url('/user_class/dosen/swicth/dosen').'" data-mini="true" data-role="button" data-inline="true"';
	$dsn_0 		.= ' data-icon="edit" data-theme="a">Dosen</a>';
	$dsn_pemb 	= '<a href="'.site_url('/user_class/dosen/swicth/pembimbing').'" data-mini="true" data-role="button" data-inline="true"';
	$dsn_pemb 	.= ' data-icon="edit" data-theme="a">Pembimbing</a>';
	$dsn_peng 	= '<a href="'.site_url('/user_class/dosen/swicth/penguji').'" data-mini="true" data-role="button" data-inline="true"';
	$dsn_peng 	.= ' data-icon="edit" data-theme="a">Penguji</a>';
	$dsn_kap 	= '<a href="'.site_url('/user_class/dosen/swicth/kaprodi').'" data-mini="true" data-role="button" data-inline="true"';
	$dsn_kap 	.= ' data-icon="edit" data-theme="a">Kaprodi</a>';
	
	$user = "";
	$level = $this->session->userdata('user');
	if($level == "dosen" ){
		$user = "";
		if($this->session->userdata('is_kaprodi')) $user .= $dsn_kap;
		$user .= $dsn_pemb;
		$user .= $dsn_peng;
	}else if($level == "pembimbing"){
		$user = "";
		if($this->session->userdata('is_kaprodi')) $user .= $dsn_kap;
		$user .= $dsn_0;
		$user .= $dsn_peng;
	}else if($level == "penguji"){
		$user = "";
		if($this->session->userdata('is_kaprodi')) $user .= $dsn_kap;
		$user .= $dsn_0;
		$user .= $dsn_pemb;
	}else if($level == "kaprodi"){
		$user = "";
		$user .= $dsn_0;
		$user .= $dsn_pemb;
		$user .= $dsn_peng;
		
	}
	
	// menampilkan tombol sign out
	$user   .= '<a href="#" data-role="button" data-icon="star" data-iconpos="left" data-theme="a"';
	$user  .= ' data-mini="true" data-inline="true">'.$this->session->userdata('nama_user').' <user>('.$this->session->userdata('user').') </user></a>';
	$sign_in_out = '<a href="'.site_url('/sipbta/sign_out').'" data-mini="true" data-role="button" data-inline="true"';
	$sign_in_out .= ' data-icon="delete" data-theme="a">Sign Out</a>';
} 
else 
{
	// menampilkan tombol sign in
	$user = '';
	$sign_in_out  = '<a href="#loginPop" data-rel="popup" data-position-to="window" data-mini="true" data-role="button"';
	$sign_in_out .= 'data-inline="true" data-icon="check" data-theme="a" data-transition="pop">Sign In</a>';
}

$left  = '<div id="left">';
$left .= '<a href="'.base_url().'" data-role="button" data-icon="home" data-iconpos="notext" data-theme="a" data-inline="true" data-ajax="false">Home</a>';
$left .= '<a href="#nav-panel" data-role="button" data-icon="grid" data-iconpos="notext" data-theme="a" data-inline="true">Menu</a>';
$left .= '<a href="#" data-role="button" data-icon="info" data-iconpos="notext" data-theme="a" data-inline="true">Info</a>';
$left .= '</div>';

$right  = '<div id="right">';
$right .= $user;
$right .= $sign_in_out;
$right .= '</div>';

echo $left;
echo $right;
?>