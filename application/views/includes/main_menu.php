<?php
// tentukan user
@$user = $this->session->userdata('user');
@$is_logged = $this->session->userdata('is_logged_in');

// menu item
$list = null;

//unset($list['Proposal']);
//unset($list['Proposal'][0]);
//$list['Proposal'][]=anchor('proposal/pendf_jdl_ta','Pengajuan Judul2');
//print_r($list);
$atts = array(
              'width'      => '900',
              'height'     => '600',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => 'auto',
              'screeny'    => 'auto'
            );
// tentukan menu item untuk user
if($is_logged == true){
	$list = array(
	'SILABITAMA' => array(),
	'Proposal' => array(),
	'Pembimbingan' => array(),
	'Seminar' => array(),
	'Pendadaran' => array()
	);
	
	if($user == 'mahasiswa')
	{
		$list['SILABITAMA'][]=anchor('user_class/mahasiswa/show_profil_mhs','Profil saya');
		$list['SILABITAMA'][]=anchor('user_class/mahasiswa/list_dsn','List dosen');
		$list['SILABITAMA'][]=anchor('user_class/mahasiswa/lihat_progres_pengerjaan_ta','Progres tugas akhir');
		
		$list['Proposal'][]=anchor('user_class/mahasiswa/ajukan_jdl','Pengajuan Judul');
		$list['Proposal'][]=anchor('user_class/mahasiswa/ajukan_prop','Pengajuan Proposal');
		
		$list['Pembimbingan'][]=anchor('user_class/mahasiswa/request_pembb','Request pembimbing');
		$list['Pembimbingan'][]=anchor('user_class/mahasiswa/lihat_dist_pembb','Distribusi pembimbing');
		$list['Pembimbingan'][]=anchor('user_class/mahasiswa/ubah_jdl/0','Ubah judul TA');
		
		$list['Seminar'][]=anchor_popup('user_class/mahasiswa/lihat_prasyarat_sem','Prasyarat seminar', $atts);
		$list['Seminar'][]=anchor('user_class/mahasiswa/mendaftar_sem','Pendaftaran Seminar');
		$list['Seminar'][]=anchor('user_class/mahasiswa/lihat_jdwl_sem','Jadwal seminar');
		$list['Seminar'][]=anchor('user_class/mahasiswa/lihat_nilai_sem_pendd','Penilaian seminar');
		
		$list['Pendadaran'][]=anchor_popup('user_class/mahasiswa/lihat_prasyarat_pendd','Prasyarat pendadaran',$atts);
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/mendaftar_pendd','Pendaftaran pendadaran');
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/lihat_jdwl_pendd','Jadwal pendadaran');
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/lihat_revisi','Lihat revisi');
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/lihat_nilai_sem_pendd','Nilai pendadaran');
	}
	elseif($user == 'dosen')
	{
		$list['SILABITAMA'][]=anchor('user_class/dosen/show_profil_dsn','Profil saya');
		$list['SILABITAMA'][]=anchor('user_class/dosen/lihat_list_mhs','List mahasiswa');
		
		$list['Proposal'][]=anchor('user_class/dosen/lihat_request_mjd_pembb','List request pembimbing');
		
		$list['Pembimbingan'][]=anchor('user_class/dosen/lihat_dist_pembb','Distribusi pembimbing');
		
		$list['Seminar'][]=anchor('user_class/dosen/lihat_request_mjd_pengg_pembb_sem','Request menjadi pengganti pembimbing seminar');
		
		$list['Pendadaran'][]=anchor('user_class/dosen/lihat_pemberitahuan_mjd_penguji','Pemberitahuan menjadi penguji');
	}
	elseif($user == 'pembimbing')
	{
		unset($list['SILABITAMA']);
		unset($list['Proposal']);
		
		$list['Pembimbingan'][]=anchor('user_class/pembimbing/lihat_list_mhs_bimbb','Pembimbingan');
		$list['Pembimbingan'][]=anchor('user_class/pembimbing/lihat_progres_pengerjaan_ta_mhs_bimbb','Lihat progress mahasiswa bimbingan');
		
		$list['Seminar'][]=anchor('user_class/pembimbing/lihat_request_sem','Lihat request seminar');
		$list['Seminar'][]=anchor('user_class/mahasiswa/lihat_jdwl_sem','Jadwal seminar');
		$list['Seminar'][]=anchor('user_class/pembimbing/menilai_sem','Penilaian seminar');
		
		//$list['Pendadaran'][]=anchor('user_class/pembimbing/lihat_list_nilai_dr_penguji','Penilaian pendadaran');
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/lihat_jdwl_pendd','Jadwal pendadaran');
		
	}
	elseif($user == 'penguji')
	{
		unset($list['SILABITAMA']);
		unset($list['Proposal']);
		unset($list['Pembimbingan']);
		unset($list['Seminar']);
		$list['Pendadaran'][]=anchor('user_class/penguji/lihat_jdwl_pendd','Jadwal pendadaran');
		$list['Pendadaran'][]=anchor('user_class/penguji/beri_revisi','Revisi / penilaian pendadaran');
	}
	elseif($user == 'opsdik')
	{
		$list['SILABITAMA'][]=anchor('user_class/opsdik/lihat_list_mhs','List mahasiswa');
		$list['SILABITAMA'][]=anchor('user_class/opsdik/lihat_list_dsn','List dosen');
		$list['SILABITAMA'][]=anchor('user_class/opsdik/lihat_progres_pengerjaan_ta','Progres pengerjaan TA');
		
		unset($list['Proposal']);
		
		$list['Pembimbingan'][]=anchor('user_class/opsdik/lihat_dist_pembb','Distribusi pembimbing');
		
		$list['Seminar'][]=anchor('user_class/opsdik/umumkan_prasyarat_sem','Prasyarat seminar');
		$list['Seminar'][]=anchor('user_class/opsdik/lihat_list_pendf_sem','List pendaftar seminar');
		
		$list['Pendadaran'][]=anchor('user_class/opsdik/umumkan_prasyarat_pendd','Prasyarat pendadaran');
		$list['Pendadaran'][]=anchor('user_class/opsdik/lihat_list_pendf_pendd','List pendaftar pendadaran');
	}
	elseif($user == 'kaprodi')
	{
		$list['SILABITAMA'][]=anchor('user_class/kaprodi/lihat_list_mhs','List mahasiswa');
		$list['SILABITAMA'][]=anchor('user_class/kaprodi/lihat_list_dsn','List dosen');
		$list['SILABITAMA'][]=anchor('user_class/kaprodi/lihat_list_jdl_ta','List judul TA');
		$list['SILABITAMA'][]=anchor('user_class/kaprodi/lihat_progres_pengerjaan_ta','Progres pengerjaan TA');
		$list['SILABITAMA'][]=anchor('user_class/kaprodi/lihat_progres_dosen','Progres Dosen');
		
		$list['Proposal'][]=anchor('user_class/kaprodi/lihat_list_pengajuan_jdl','List pengajuan judul');
		$list['Proposal'][]=anchor('user_class/kaprodi/lihat_list_pengajuan_prop','List pengajuan proposal');
		
		$list['Pembimbingan'][]=anchor('user_class/kaprodi/tentukan_dist_pembb','Penentuan distribusi pembimbing');
		$list['Pembimbingan'][]=anchor('user_class/kaprodi/lihat_dist_pembb','Distribusi pembimbing');
		
		$list['Seminar'][]=anchor('user_class/kaprodi/lihat_jdwl_sem','Jadwal seminar');
		
		$list['Pendadaran'][]=anchor('user_class/kaprodi/lihat_list_pendf_pendd','List pendaftar pendadaran');
		$list['Pendadaran'][]=anchor('user_class/mahasiswa/lihat_jdwl_pendd','Jadwal pendadaran');
	}
	elseif($user == 'admin')
	{
		unset($list['SILABITAMA']);
		unset($list['Proposal']);
		unset($list['Pembimbingan']);
		unset($list['Seminar']);
		unset($list['Pendadaran']);
		
		$list['Menu Admin'][]=anchor('user_class/admin/edit_data_prodi','Edit data prodi');
		$list['Menu Admin'][]=anchor('user_class/admin/edit_data_mhs','Edit data mahasiswa');
		$list['Menu Admin'][]=anchor('user_class/admin/edit_data_dsn','Edit data dosen');
		$list['Menu Admin'][]=anchor('user_class/admin/edit_data_ops','Edit data opsdik');
	}	
}
else 
{
	$list = array('Guest' => array());
}

$menus = array_keys($list);

$div   = '<div  data-role="collapsible-set" data-inset="true" data-theme="a" data-content-theme="a" >';
foreach($menus as $menu){
		$div .= "<div data-role='collapsible'>";
		$div .= "<h3>".$menu."</h3>";
		$div .= ul($list[$menu])."</div>";	
	}
$div  .= '</div>';
echo $div;
?>