<?php
$keywords = 'sipbta, pengeloaan bimbingan, tugas akhir, STMIK, Yogyakarta, STMIK Jenderal Achmad Yani Yogyakarta,';
$keywords .= 'Sistem informasi pengelolaan bimbingan tugas akhir, pengelolaan bimbingan tugas akhir';

$meta = array(
	array('name' => 'robots', 'content' => 'no-cache'),
	array('name' => 'description', 'content' => 'Sistem informasi pengelolaan bimbingan tugas akhir STMIK Jenderal Achmad Yani Yogyakarta'),
	array('name' => 'keywords', 'content' => $keywords),
	array('name' => 'robots', 'content' => 'no-cache'),
	array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
	array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1')
);

echo meta($meta);
?>

<script src="<?php echo base_url(); ?>public/jqm/js/jquery.js"></script>
<script type="text/javascript">
	$(document).bind("mobileinit", function () {
	    $.mobile.ajaxEnabled = false;
	});
</script>
<script src="<?php echo base_url(); ?>public/jqm/js/jquery.mobile-1.3.0.min.js"></script>

<!-- end of slider script -->
<script src="<?php echo base_url(); ?>public/jqm/plugin/date/jquery.mousewheel.min.js"></script>
<script src="<?php echo base_url(); ?>public/jqm/plugin/date/jqm-datebox.core.min.js"></script>
<script src="<?php echo base_url(); ?>public/jqm/plugin/date/jqm-datebox.mode.flipbox.min.js"></script>
<script src="<?php echo base_url(); ?>public/jqm/plugin/date/jquery.mobile.datebox.i18n.en_US.utf8.js"></script>
<!-- date script -->

<!-- slider script -->
<script type="text/javascript" src="<?php echo base_url();?>public/slider/js/modernizr.custom.28468.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/slider/js/jquery.cslider.js"></script>
<script type="text/javascript">
	$(function() {
	
		$('#da-slider').cslider({
			autoplay	: true,
			bgincrement	: 450
		});
	
	});
</script>


<!-- end of date script -->

<link rel="stylesheet" href="<?php echo base_url(); ?>public/jqm/css/themes/default/jquery.mobile-1.3.0.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>public/jqm/plugin/date/jqm-datebox.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/master2.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/slider/css/style2.css" />

	