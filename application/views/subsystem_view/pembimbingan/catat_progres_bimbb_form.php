<form action="<?php echo $action; ?>" method="post">
	<div data-role="fieldcontain">
		<?php echo $id_bimbb; ?>
	    <label for="nim">NIM</label> <?php echo $nim; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="nama">Nama</label> <?php echo $nama; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="prodi">Prodi</label> <?php echo $prodi; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="bab">Bab</label> <input type="text" name="bab" placeholder="Sampai bab berapa?" value="">
	</div>
	<div data-role="fieldcontain">
	    <label for="materi">Catatan bimbingan</label> 
	    <textarea cols="40" rows="8" name="materi" placeholder="Masukkan catatan bimbingan yang dilaksanakan" value=""></textarea>
	</div>
	<p><?php echo $button; ?></p>
</form>
<div class="data"><?php echo $table; ?></div>