<script>
	$(document).ready(function(){
		
		var move = function(a,b){
			var src = a.find("input:checked").prop("checked",false).parents("tr");
			src.css({"font-weight": "normal"});
			
			for(var i=0,n=src.length; i<n; i++){
				var nim1 = $(src[i]).children("td:nth-child(2)").text();
				var dest = b.find("tr");						
				if(dest.length == 0){
					b.append(src[i]);
					continue;
				}
				var ketemu = false;
				for(var j=0,m=dest.length; j<m; j++){
					var nim2 = $(dest[j]).children("td:nth-child(2)").text();
					if(nim2 > nim1){
						ketemu = dest[j];
						break;
					}
				}
				if(ketemu) $(src[i]).insertBefore($(ketemu));
				else b.append(src[i]);
			}
			
			// 2n berarti genap
			a.find("tr:nth-child(2n+1)").removeClass("alt"); 		
			a.find("tr:nth-child(2n)").addClass("alt");
			b.find("tr:nth-child(2n+1)").removeClass("alt"); 		
			b.find("tr:nth-child(2n)").addClass("alt");
		}
		
		$("#add").click(function(){
			move($("#list_mhs"), $("#mhs_choosen"));
		});
		
		$("#remove").click(function(ev){
			move($("#mhs_choosen"), $("#list_mhs"));
		});
		
		$("#remove_all").click(function(ev){
			$("#mhs_choosen input[type=checkbox]").prop("checked",true);
			move($("#mhs_choosen"), $("#list_mhs"));
		});
		
		//$("#list_mhs tr td:nth-child(2)").text() ambil text dalam tr
		// $("#mhs_choosen td input:checked").parents("tr").children("td:nth-child(2)")
		
		$("#dist_pembb").submit(function(){
			$("#mhs_choosen input[type=checkbox]").prop("checked",true);
			
		});
		
		$("#list_mhs tr td:nth-child(3), #list_mhs tr td:nth-child(2)").click(function(){
				if($(this).parents("tr").find("input:checked").length == 1){
					$(this).parents("tr").find("input:checked").prop("checked",false);
					$(this).parents("tr").css({"font-weight": "normal"});
				}else {
					$(this).parents("tr").find("input").prop("checked",true);
					$(this).parents("tr").css({"font-weight": "bold"});
				}
			});
		});
		

</script>

<div id="dsn_opt" >
	<form action="<?php echo $action1; ?>" method="post">
		<div>
			<?php echo $dsn_opt12; ?>
		</div>
		<div>
			<input type="submit" data-inline="true" data-theme="e" value="Go" />
		</div>
	</form> 
</div>
<div class="container">
<?php echo $list_mhs; ?>
</div>
<form action="<?php echo $action2; ?>" method="post" id="dist_pembb">
	<div id="button_set">
		<div data-role="controlgroup"  data-corners="false">
			<button type="button" id="add" >&#x25AA;&#x25BA;</button>
			<button type="button" id="remove">&#x25C4;&#x25AA;</button> <!-- karakter intity-->
			<button type="button" id="remove_all">&#x25C4;&#x25C4;</button>
		</div>
		<input  id="simpan" type="submit" value="OK" >
		<?php echo $id_dsn; ?>
	</div>
	<div class="container2">	
		<?php echo $mhs_choosen; ?>
	</div>
</form>
<div id="button"> </div>