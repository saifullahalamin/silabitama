<script language="javascript">
	function validateForm()
	{
		var x=document.forms["ubah_jdl"]["jdl_baru"].value;
		if (x==null || x=="" || x==" ")
		{
		  alert("Masukkan judul baru dengan tema yang sama!");
		  return false;
	  	}
	}
</script>
<form action="<?php echo $action; ?>" method="post" name="ubah_jdl" onsubmit="return validateForm()">
	<div data-role="fieldcontain">
		<?php echo $id_jdl; ?>
	    <label for="jdl_skrg">Judul TA saat ini</label> <?php echo $jdl_skrg; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="jdl_baru">Judul TA baru</label><textarea cols="40" rows="8" name="jdl_baru" placeholder="Masukkan judul baru dengan tema yang sama, jika judul baru anda berbeda tema silahkan ajukan proposal baru."></textarea>
	</div>
	<p>
		<button type="submit" name="submit" data-inline="true">Ubah judul</button>
		<?php echo $kembali; ?>
	</p>
</form>
<div class="data"><?php echo $table; ?></div>