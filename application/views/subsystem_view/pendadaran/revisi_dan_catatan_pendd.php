</div>
<div data-role="fieldcontain">
    <label for="nilai">Penilaian</label> : 
    <ol>
    	<li>Tata tulis (10%)
    		<ul>
    			<li>
    				<label>Bahasa</label> <?php echo $tt_bhs; ?>
    				    						
    				
				</li>
    			<li>
    				<label>Format penulisan</label> <?php echo $tt_fp; ?>
    			</li>
    		</ul>
    	</li>
    	<li>Cara pelaporan (10%)
    		<ul>
    			<li>
    				<label>Tertulis</label> <?php echo $cp_tertulis; ?>
    			<li>
    				<label>Presentasi (lisan)</label> <?php echo $cp_lisan; ?>
    			</li>
    		</ul>
    	</li>
    	<li>
    		<lb>Bobot permasalahan (20%)</lb> <?php echo $b_permasalahan; ?>
    	</li>
    	<li>Penguasaan materi (60%)
    		<ul>
    			<li>
    				<label>Desain proses</label>  <?php echo $pm_dp; ?>
    			<li>
    				<label>Programming</label> <?php echo $pm_prog; ?>
    			</li>
    			<li>
    				<label>User friendly</label> <?php echo $pm_uf; ?>
    			</li>
    		</ul>	
    	</li>
    </ol>
</div>
<?php if($opt == 'det'){?>
<div data-role="fieldcontain">
    <label for="nilai_angka">Nilai angka</label>: <?php echo $nilai_angka; ?>
</div>
<?php } ?>
<div data-role="fieldcontain">
    <label for="stts_rev">Status revisi</label> <?php echo $stts_rev; ?>
</div>
<div data-role="fieldcontain">
    <label for="cttn_pendd">Catatan pendadaran</label> <?php echo $cttn_pendd; ?>
</div>
<div data-role="fieldcontain">
    <label for="cttn_rev">Catatan revisi</label> <?php echo $cttn_rev; ?>
</div>
<div data-role="fieldcontain">
    <label for="bts_wkt">Batas waktu</label> <?php echo $bts_wkt; ?>