<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	<div data-role="fieldcontain">
		<?php echo $id_jdl; ?>
	    <label for="jdl_ta">Judul TA</label> <?php echo $jdl_ta; ?> 
	</div>
	<div data-role="fieldcontain">
	    <label for="abstraksi">Abstraksi judul TA</label> <?php echo $abstraksi; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="jenis">Jenis</label> <?php echo $jenis; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="rek_pembb">Status judul</label> <?php echo $status_jdl; ?>
	</div>
	<?php if($this->session->userdata('user') == 'kaprodi') { ?>
	<div data-role="fieldcontain">
	    <label for="per7an">Persetujuan</label> <?php echo $pers7an; ?>
	</div>
	<p>
    		<?php echo $button; ?>
    		<a href= "<?php echo site_url('user_class/kaprodi/lihat_list_pengajuan_jdl');?>" data-role="button" data-inline="true">Kembali</a>
	</p>
	<?php } ?>
	<?php if($this->session->userdata('user') == 'mahasiswa') { ?>
		<a href= "<?php echo site_url('user_class/mahasiswa/ajukan_jdl');?>" data-role="button" data-inline="true">Kembali</a>
	<?php } ?>
</form>