<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
<?php if($this->session->userdata('user') == 'kaprodi') { // kaprodi --------- ?> 
	<div data-role="fieldcontain">
	    <label for="nim">NIM</label> <?php echo $nim; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="nama">Nama</label> <?php echo $nama; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="prodi">Prodi</label> <?php echo $prodi; ?>
	</div>
<?php } ?>
	<div data-role="fieldcontain">
		<?php echo $id_prop; ?>
		<?php echo $id_jdl; ?>
	    <label for="jdl_ta">Judul TA</label> <?php echo $jdl_ta; ?> 
	</div>
	<div data-role="fieldcontain">
	    <label for="abstraksi">Abstraksi judul TA</label> <?php echo $abstraksi; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="jenis">Jenis</label> <?php echo $jenis; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="stts_prop">Status proposal</label> <?php echo $status_prop; ?>
	</div>
	
<?php if($this->session->userdata('user') == 'kaprodi') { // kaprodi --------- ?> 
	<div data-role="fieldcontain">
	    <label for="rek_pembb">Rekomendasi pembimbing</label> <?php echo $rek_pembb; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="pers7an">Persetujuan proposal</label> <?php echo $pers7an; ?>
	</div>
	<div data-role="fieldcontain">
	    <?php echo $tentukan_pembb; ?>
	</div>
	<p>
    		<?php echo $button; ?>
	</p>
<?php } ?>

<?php if($this->session->userdata('user') == 'mahasiswa') { // mahasiswa ---------  ?>
		<a href= "<?php echo site_url('user_class/mahasiswa/ajukan_prop');?>" data-role="button" data-inline="true">Kembali</a>
<?php } ?>
</form>