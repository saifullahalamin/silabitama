<form action="<?php echo $action; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	<div data-role="fieldcontain">
	    <label for="jdl_ta">Judul TA</label> <?php echo $jdl_ta; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="abstraksi">Abstraksi judul TA</label> <?php echo $abstraksi; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="jenis">Jenis</label> <?php echo $jenis; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="rek_pembb">Rekomendasi dosen pembimbing</label> <?php echo $rek_pembb; ?>
	</div>
	<div data-role="fieldcontain">
	    <label for="file">File proposal</label>
	    <?php echo $file; ?>
	</div>
	<p><?php echo $button; ?></p>
</form>