<?php
// form heading ----------
$form_heading = 'Sign In';
$form_dest = 'sipbta/sign_in_proses';

// form fields ----------
$fields = array(
		form_input(array('name'=> 'username','value' =>'','placeholder'=>'username','data-theme'=>'a')),
		form_password(array('name'=> 'password','value' =>'','placeholder'=>'password','data-theme'=>'a'))
	);

// buttons
$buttons  = '<div class="ui-grid-a"><div class="ui-block-a"></div><div class="ui-block-b">';
$buttons .= form_submit(array('name'=> 'Sign In','value' =>'Sign In','data-icon'=>'check', 'data-theme'=>'b'));
$buttons .= '</div></div>';

// form attribute -----------
$form_att = array('id' => 'Sign In');
$ul_att = array('class' => 'fields');

// form ------------
if($this->session->userdata('is_logged_in'))
{}
else
{
	$login  = '<div data-role="popup" id="loginPop" data-overlay-theme="a" data-theme="a" data-corners="false" data-tolerance="15,15">';
	$login .= '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
	$login .= complete_form($form_heading,$form_dest,$fields,$buttons,$form_att,$ul_att);
	$login .= '</div>';
	echo $login;
}
?>