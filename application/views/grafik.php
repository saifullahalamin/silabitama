<base href="<?= $this->config->item('base_url'); ?>" />
<script type="text/javascript" src="assets/js/swfobject.js"></script>

<script type="text/javascript">
    swfobject.embedSWF(
      "assets/swf/open-flash-chart.swf", "test_chart",
      "<?php echo $chart_width; ?>", "<?php echo $chart_height; ?>",
      "9.0.0", "expressInstall.swf",
      {"data-file":"<?php echo urlencode($data_url); ?>"}
    );
</script>

<div id="test_chart"></div>

